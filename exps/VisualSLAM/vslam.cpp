#include <iostream>
#include <fstream>
#include <assert.h>

#include <helper_timer.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <boost/thread/thread.hpp>
#include <pangolin/pangolin.h>
#include <pangolin/timer.h>

#include "falcon/cuda/cuda_data.h"
#include "falcon/system.h"
#include "falcon/gldraw.h"


using namespace std;
using namespace cv;
using namespace pangolin;
using namespace falcon;

char window_title[] = "Stereo Dense SLAM";

StopWatchInterface *timer = 0;

void CalculateFPS()
{

    char fps[256];
    float ifps = 1.f / (sdkGetAverageTimerValue(&timer) / 1000.f);
    sprintf(fps, "%s: %3.1f fps", window_title, ifps);
//    glutSetWindowTitle(fps);
    sdkResetTimer(&timer);

}

double GetMemoryUsageMB()
{
    size_t avail;
    size_t total;
    cudaMemGetInfo(&avail, &total);

    return (total - avail)/1024.0/1024.0;

}

////////////////////////////////////////////////////////////////////////////
//  2D Handler for Pangolin view
////////////////////////////////////////////////////////////////////////////
struct Handler2D : Handler
{

    Handler2D() : in_view(false), pressed(false){}

    void Mouse(View& view, MouseButton button, int x, int y, bool pressed, int button_state)
    {

        in_view = (x > view.vp.l && x < view.vp.r() && y > view.vp.b && y < view.vp.t()) ? true : false;

        this->button = button;
        this->pressed = pressed;

        last_pos[0] = x;
        last_pos[1] = y;
    }

    void MouseMotion(View& view, int x, int y, int button_state)
    {
        in_view = (x > view.vp.l && x < view.vp.r() && y > view.vp.b && y < view.vp.t()) ? true : false;

        last_pos[0] = x;
        last_pos[1] = y;
    }

    bool IsInsideView() const { return in_view; }
    bool IsPressed() const { return pressed; }

    bool IsLeftButtonClicked()
    {
        bool left_clicked = (button == MouseButtonLeft && pressed);

        button = (MouseButton)0;

        return left_clicked;
    }

    GLint const* GetLastPos() const { return last_pos; }

protected:
    MouseButton button;

    bool pressed;
    bool in_view;
    GLint last_pos[2];
};


void ExportCameraPose(vector<SE3f> const& cam_poses)
{

    ofstream file;
    file.open ("trajectory.txt");
    for(int i = 0; i < cam_poses.size(); ++i)
    {
        Vector6f pose = cam_poses.at(i).log();

        file << pose(0, 0) << " " << pose(1, 0) << " " << pose(2, 0)
             << " " << pose(3, 0) << " " << pose(4, 0)<< " " << pose(5, 0) << endl;
    }

    file.close();
}





int main(int argc, char* argv[])
{

    if(argc < 4)
    {
        cerr << "Usage: ./FalconSLAM <left_video> <right_video> <cam_calib_file>" << endl;
        exit(EXIT_FAILURE);
    }

    /* Camera calibration */
    uint32_t cam_w, cam_h;
    Mat cv_RMAP[2][2];
    Matrix3f K[2];
    SE3f T_ext, T_cl, T_cr;
    float B;
    if(argc == 4)
    {
        FileStorage fs(argv[3], FileStorage::READ); // Read the settings
        if (!fs.isOpened())
        {
            cerr << __PRETTY_FUNCTION__ << ": Could not open the camera calibration file: \"" << argv[3] << "\"" << endl;
            exit(EXIT_FAILURE);
        }
        assert((string)fs["CameraType"] == "stereo");


        cam_w = (int)fs["ImageWidth"];
        cam_h = (int)fs["ImageHeight"];

        Mat cv_K[2], cv_D[2];
        fs["Kl"] >> cv_K[0];
        fs["Kr"] >> cv_K[1];
        assert(cv_K[0].size() == Size(3, 3)  && cv_K[1].size() == Size(3, 3));

        fs["Dl"] >> cv_D[0];
        fs["Dr"] >> cv_D[1];
        assert(cv_D[0].size() == Size(1, 5)  && cv_D[1].size() == Size(1, 5));

        Mat R, t;
        fs["R"] >> R;
        fs["t"] >> t;
        assert(R.type() == CV_64F && t.type() == CV_64F);

//        Mat cv_Tlr = Mat::eye(4, 4, CV_64F);
//        R.copyTo(cv_Tlr.rowRange(0, 3).colRange(0, 3));
//        t.copyTo(cv_Tlr.rowRange(0, 3).colRange(3, 4));

        fs.release();

        /* Stereo rectification */
        Mat cv_RectRot[2], cv_RectProj[2], cv_Q;
        Rect cv_roi[2];
        stereoRectify(cv_K[0],
                      cv_D[0],
                      cv_K[1],
                      cv_D[1],
                      Size(cam_w, cam_h),
                      R,
                      t,
                      cv_RectRot[0],
                      cv_RectRot[1],
                      cv_RectProj[0],
                      cv_RectProj[1],
                      cv_Q,
                      CV_CALIB_ZERO_DISPARITY,
                      0,
                      Size(cam_w, cam_h),
                      &cv_roi[0],
                      &cv_roi[1]);

        /* Get the rectification re-map index */
        for(uint8_t i = 0; i < 2; i++)
        {
            initUndistortRectifyMap(cv_K[i],
                                    cv_D[i],
                                    cv_RectRot[i],
                                    cv_RectProj[i],
                                    Size(cam_w, cam_h),
                                    CV_16SC2,
                                    cv_RMAP[i][0],
                                    cv_RMAP[i][1]);
        }

        /* Using float type in OpenGL is much much more faster!! */
        cv_RectProj[0].rowRange(0, 3).colRange(0, 3).copyTo(cv_K[0]);
        cv_RectProj[1].rowRange(0, 3).colRange(0, 3).copyTo(cv_K[1]);

        cv_K[0].convertTo(cv_K[0], CV_32F);
        cv_K[1].convertTo(cv_K[1], CV_32F);

        K[0] = Map<Matrix<float,3,3,RowMajor> >((float*)cv_K[0].data);
        K[1] = Map<Matrix<float,3,3,RowMajor> >((float*)cv_K[1].data);

        B = cv::norm(t);
        T_ext = SE3f(SE3Group<float>(SO3f(), SE3Group<float>::Point(-B,0,0)));

        T_cl = SE3f::exp(T_ext.log()/2.0);
        T_cr = SE3f::exp(T_ext.inverse().log()/2.0);

    }

    /* Import video */
    VideoCapture video_capturer[2];
    for(uint8_t i = 0; i < 2; i++)
    {
        video_capturer[i].open(argv[i+1]);
        if(!video_capturer[i].isOpened())
        {
            cerr << __PRETTY_FUNCTION__ << ": Can't open video file " << argv[i+1] << endl;
            exit(EXIT_FAILURE);
        }
    }

    /* Check the consistency of left and right video format */
    for(uint8_t i = CV_CAP_PROP_POS_MSEC; i < CV_CAP_PROP_BRIGHTNESS; i++)
        if(video_capturer[0].get(i) != video_capturer[1].get(i))
        {
            cerr << __PRETTY_FUNCTION__ << ": Left and right video files have different format in " << i << endl;
            exit(EXIT_FAILURE);
        }

    uint32_t frame_w = video_capturer[0].get(CV_CAP_PROP_FRAME_WIDTH);
    uint32_t frame_h = video_capturer[0].get(CV_CAP_PROP_FRAME_HEIGHT);

    assert(cam_w == frame_w && cam_h == frame_h);

    /////////////////////////////////////////////////
    //  Pangolin UI Setting
    /////////////////////////////////////////////////
    /* OpenGL, GLEW, Freeglut initialization */    
    int const VIEW_ROW_NUMBER = 2;
    int const VIEW_COL_NUMBER = 3;

    int const UI_WIDTH = 180;

    CreateGlutWindowAndBind(window_title, 2*UI_WIDTH+frame_w*VIEW_COL_NUMBER, frame_h*VIEW_ROW_NUMBER, GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glClearColor(1.0, 1.0, 1.0, 1.0);

    GLDrawer::near = 0.1;
    GLDrawer::far  = 10000;

    GLenum err = glewInit();
    if(GLEW_OK != err)
    {
        std::cerr << "GLEW Error: " << glewGetErrorString(err) << std::endl;
        exit(0);
    }

    /* Views */
    View& l_view = Display("Left Frame View")
            .SetAspect((float)frame_w/(float)frame_h)
            .SetHandler(new Handler2D);

    View& r_view = Display("Right Frame View")
            .SetAspect((float)frame_w/(float)frame_h)
            .SetHandler(new Handler2D);

    View& m_view = Display("Disparity View")
            .SetAspect((float)frame_w/(float)frame_h)
            .SetHandler(new Handler2D);

    View& mest_view = Display("M Estimator Image View")
            .SetAspect((float)frame_w/(float)frame_h)
            .SetHandler(new Handler2D);

    View& warp_view = Display("Warp Image View")
            .SetAspect((float)frame_w/(float)frame_h)
            .SetHandler(new Handler2D);

    /* Pangolin projection and 3D Handler */
    OpenGlRenderState world_cam(
                ProjectionMatrix(frame_w, frame_h, frame_h, frame_h, frame_w/2.0, frame_h/2.0, GLDrawer::near, GLDrawer::far),
                ModelViewLookAt(0.0, -50.0, -60.0, 0.0, 0, 100, AxisNegY));

    View& wview = Display("World View")
            .SetAspect((float)frame_w/(float)frame_h)
            .SetHandler(new Handler3D(world_cam));

    Display("multi")
            .SetBounds(0.0, 1.0, Attach::Pix(2*UI_WIDTH), 1.0)
            .SetLayout(LayoutEqual)
            .AddDisplay(l_view)
            .AddDisplay(r_view)
            .AddDisplay(m_view)
            .AddDisplay(mest_view)
            .AddDisplay(warp_view)
            .AddDisplay(wview);

    /* UI */
    View& sys_panel = CreatePanel("ui_sys")
            .SetBounds(0.0, 1.0, 0.0,  Attach::Pix(UI_WIDTH));

    View& params_panel = CreatePanel("ui_params")
            .SetBounds(0.0, 1.0, Attach::Pix(UI_WIDTH), Attach::Pix(2*UI_WIDTH));

    Var<double> gpu_memory("ui_sys.GPU Memory", GetMemoryUsageMB(), 0, 1024);
    cout << "GPU memory begins with: " << gpu_memory << endl;

    /* SLAM system functions */
    Var<bool> sys_visual("ui_sys.Visualise System", true, true);

    Var<bool> sys_init_bt("ui_sys.Init SLAM System", false, false);
    Var<bool> sys_track_bt("ui_sys.Track", false, false);
    Var<bool> sys_stop_track_bt("ui_sys.Stop Track", false, false);
    bool init_flag = false;
    bool tracking_flag = false;

    Var<int> frame_no("ui_sys.Frame", 0);
    Var<bool> play_button("ui_sys.Play Video", false, false);
    Var<bool> stop_button("ui_sys.Stop Video", false, false);
    bool playing_video_flag = false;

    Var<int> pyr_level("ui_sys.Pyramid Layer", 0, 0, PYR_LEVEL_NUM-1);

    /* World drawer parameters */
    Var<double> v_w_s("ui_sys.World Scaler", 1, 1e-2, 1e2, true);
    Var<double> v_c_s("ui_sys.Camera Scaler", 1e2, 1, 1e5, true);

    Var<bool> export_pose_bt("ui_sys.Export Camera Poses", false, false);

    /* Cost volume parameters */
    Var<int> min_disp("ui_params.Min Disparity", 10, 0, 50);
    Var<int> max_disp("ui_params.Max Disparity", 64, 10, 150);
    Var<int> min_bg_thr("ui_params.Min Bg Threshold", 30, 0, 255);
    Var<int> max_bg_thr("ui_params.Max Bg Threshold", 255, 0, 255);
    Var<int> win_r("ui_params.Window Radius", 7, 0, 20);

    Var<bool> ref_img("ui_params.Reconst Left Camera", true, true);

    Var<bool> ad_button("ui_params.Absolute Distance (AD)", false, false);
    Var<bool> zncc_button("ui_params.ZNCC", false, false);
    DataTermMethod data_term = ZNCC;

    /* Huber-L1 TGV1 parameterrs */
    Var<int> num_itr("ui_params.Number of Iterations", 100, 0, 1000);
    Var<double> alpha("ui_params.Alpha", 10, 0.1, 20.0);
    Var<double> beta("ui_params.Beta", 1.0, 0.1, 5.0);
    Var<double> epsilon("ui_params.Epsilon", 1e-1, 1e-3, 1.0);
    Var<double> lambda("ui_params.Lambda", 1e-3, 1e-4, 10.0, true);
    Var<double> aux_theta("ui_params.Aux Theta", 10.0, 1e-1, 1000, true);
    Var<double> aux_theta_gamma("ui_params.Aux Theta Gamma", 1e-6, 1e-7, 1e-5, true);

    /* OpenGL textures */
    Mat video_frames[2];
    CUDAImage<OpenGL2D,uchar4> cur_imgs[PYR_LEVEL_NUM][2];
    for(int i = 0; i < 2; i++)
    {
        video_capturer[i] >> video_frames[i];
        cvtColor(video_frames[i], video_frames[i], CV_BGR2RGBA, 4);
        remap(video_frames[i], video_frames[i], cv_RMAP[i][0], cv_RMAP[i][1], CV_INTER_LINEAR);

        cur_imgs[0][i].Reinitialise(frame_w, frame_h);
        cur_imgs[0][i].Upload(video_frames[i].data, GL_RGBA, GL_UNSIGNED_BYTE);

        /* Build pyramid */        
        for(int level = 1; level < PYR_LEVEL_NUM; ++level)
        {
            pyrDown(video_frames[i], video_frames[i]);
            cur_imgs[level][i].Reinitialise(video_frames[i].cols, video_frames[i].rows, GL_RGBA8);
            cur_imgs[level][i].Upload(video_frames[i].data, GL_RGBA, GL_UNSIGNED_BYTE);
        }

    }

    SLAMSystem system(cur_imgs,
                      RectifiedStereoCamera(K[0], T_ext),
                      CostVolume::Params(min_disp, max_disp, win_r, ref_img ? LeftRefImage:RightRefImage, ZNCC, min_bg_thr, max_bg_thr, frame_w, frame_h),
                      HuberROF::Params(num_itr, alpha, beta, epsilon, lambda, aux_theta));

    OpenGLCUDAInteropSrc gl_cuda;

    vector<SE3f> cam_poses;

    sdkCreateTimer(&timer);
    while(!ShouldQuit())
    {        

        /* The very beginning of a system iteration */
        sdkStartTimer(&timer);

        /* Drawer variables */
        GLDrawer::w_s = v_w_s;
        GLDrawer::c_s = v_c_s;

        if(HasResized())
            DisplayBase().ActivateScissorAndClear();

        // Render our UI panel when we receive input
        if(HadInput())
        {
            sys_panel.Render();
            params_panel.Render();
        }

        /* Buttons */
        if(Pushed(sys_init_bt))
        {

            Timer timer;
            system.Init();
            cerr << timer.Elapsed_s() << endl;

            init_flag = true;
            tracking_flag = false;
            playing_video_flag = false;
        }

        if(Pushed(sys_track_bt))
        {
            init_flag = false;
            tracking_flag = true;
            playing_video_flag = false;            
        }

        if(Pushed(sys_stop_track_bt))        
            tracking_flag = false;        

        if(Pushed(play_button) && !tracking_flag)
            playing_video_flag = true;

        if(Pushed(stop_button))
            playing_video_flag = false;

        if(Pushed(export_pose_bt))
            ExportCameraPose(cam_poses);

        if(min_disp > max_disp)
            min_disp = max_disp;

        if(max_disp < min_disp)
            max_disp = min_disp;

        if(Pushed(ad_button))
            data_term = AD;

        if(Pushed(zncc_button))
            data_term = ZNCC;

        if(((Handler2D*)l_view.handler)->IsPressed())
        {
            float x = ((Handler2D*)l_view.handler)->GetLastPos()[0] - l_view.vp.l;
            float y = ((Handler2D*)l_view.handler)->GetLastPos()[1] - l_view.vp.b;
            cout << "left: " << "(" << x << "," << y << ")" << endl;
            boost::this_thread::sleep( boost::posix_time::milliseconds(200));
        }

        if(((Handler2D*)r_view.handler)->IsPressed())
        {
            float x = ((Handler2D*)r_view.handler)->GetLastPos()[0] - r_view.vp.l;
            float y = ((Handler2D*)r_view.handler)->GetLastPos()[1] - r_view.vp.b;
            cout << "right: " << "(" << x << "," << y << ")" << endl;
            boost::this_thread::sleep( boost::posix_time::milliseconds(200));
        }

        if(((Handler2D*)m_view.handler)->IsPressed())
        {
            float x = ((Handler2D*)m_view.handler)->GetLastPos()[0] - m_view.vp.l;
            float y = ((Handler2D*)m_view.handler)->GetLastPos()[1] - m_view.vp.b;
            cout << "disparity: " << "(" << x << "," << y << ")" << endl;
            boost::this_thread::sleep( boost::posix_time::milliseconds(200));
        }

        if(((Handler2D*)mest_view.handler)->IsPressed())
        {
            float x = ((Handler2D*)mest_view.handler)->GetLastPos()[0] - mest_view.vp.l;
            float y = ((Handler2D*)mest_view.handler)->GetLastPos()[1] - mest_view.vp.b;
            cout << "M-estimator: " << "(" << x << "," << y << ")" << endl;
            boost::this_thread::sleep( boost::posix_time::milliseconds(200));
        }

        if(((Handler2D*)warp_view.handler)->IsPressed())
        {
            float x = ((Handler2D*)warp_view.handler)->GetLastPos()[0] - warp_view.vp.l;
            float y = ((Handler2D*)warp_view.handler)->GetLastPos()[1] - warp_view.vp.b;
            cout << "Difference: " << "(" << x << "," << y << ")" << endl;
            boost::this_thread::sleep( boost::posix_time::milliseconds(200));
        }


        /* Left video frame view */
        l_view.ActivateScissorAndClear();
        cur_imgs[pyr_level][0].SetNearestNeighbour();
        cur_imgs[pyr_level][0].RenderToViewportFlipY();

        /* Right video frame view */
        r_view.ActivateScissorAndClear();
        cur_imgs[pyr_level][1].SetNearestNeighbour();
        cur_imgs[pyr_level][1].RenderToViewportFlipY();


        /* Play only video */
        if(playing_video_flag)
        {
            for(int i = 0; i < 2; ++i)
            {
                video_capturer[i] >> video_frames[i];
                cvtColor(video_frames[i], video_frames[i], CV_BGR2RGBA, 4);

                remap(video_frames[i], video_frames[i], cv_RMAP[i][0], cv_RMAP[i][1], CV_INTER_LINEAR);
                cur_imgs[0][i].Upload(video_frames[i].data, GL_RGBA, GL_UNSIGNED_BYTE);

                /* Build pyramid */
                for(int level = 1; level < PYR_LEVEL_NUM; ++level)
                {
                    pyrDown(video_frames[i], video_frames[i]);
                    cur_imgs[level][i].Upload(video_frames[i].data, GL_RGBA, GL_UNSIGNED_BYTE);
                }

            }

            frame_no = frame_no + 1;
        }

        /* Tracking */
        if(tracking_flag)
        {

            for(int cam = 0; cam < 2; ++cam)
            {

                video_capturer[cam] >> video_frames[cam];
                cvtColor(video_frames[cam], video_frames[cam], CV_BGR2RGBA, 4);

                remap(video_frames[cam], video_frames[cam], cv_RMAP[cam][0], cv_RMAP[cam][1], CV_INTER_LINEAR);
                cur_imgs[0][cam].Upload(video_frames[cam].data, GL_RGBA, GL_UNSIGNED_BYTE);

                /* Build pyramid */
                for(int level = 1; level < PYR_LEVEL_NUM; ++level)
                {
                    pyrDown(video_frames[cam], video_frames[cam]);
                    cur_imgs[level][cam].Upload(video_frames[cam].data, GL_RGBA, GL_UNSIGNED_BYTE);
                }
            }

            frame_no = frame_no + 1;
            if(system.GetTracker()->Tracking(cur_imgs))
            {
                cam_poses.push_back(system.GetTracker()->GetCameraInWorld());

                warp_view.ActivateScissorAndClear();
                system.GetTracker()->GetWeightImage(gl_cuda.weight_imgs, pyr_level, 0);
                gl_cuda.weight_imgs[pyr_level][0].SetNearestNeighbour();
                gl_cuda.weight_imgs[pyr_level][0].RenderToViewportFlipY();

                mest_view.ActivateScissorAndClear();
                system.GetTracker()->GetErrorImage(gl_cuda.error_imgs, pyr_level, 0);
                gl_cuda.error_imgs[pyr_level][0].SetNearestNeighbour();
                gl_cuda.error_imgs[pyr_level][0].RenderToViewportFlipY();
            }
            else
            {
                system.Init();
                system.GetReconstor()->GetDisparityImage(gl_cuda.disp_imgs);
                system.GetReconstor()->GetPointCloud(cur_imgs[0][!ref_img],
                        gl_cuda.pts,
                        gl_cuda.pts_colour,
                        B,
                        K[0].data(),
                        ref_img ? (system.GetTracker()->GetCameraInWorld()*T_cl).matrix().data() : (system.GetTracker()->GetCameraInWorld()*T_cr).matrix().data());
            }

        }

        /* Visualisation */
        if(sys_visual)
        {            

            /* Disparity view */
            m_view.ActivateScissorAndClear();            
            gl_cuda.disp_imgs.RenderToViewportFlipY();

            /* World view */
            wview.ActivateScissorAndClear(world_cam);
            GLDrawer::DrawWorldCoord();
            for(int i = 0; i < 2; i++)
                GLDrawer::DrawCamera(K[i], i ? system.GetTracker()->GetCameraInWorld()*T_cr : system.GetTracker()->GetCameraInWorld()*T_cl, cur_imgs[pyr_level][i], cam_w, cam_h);

            GLDrawer::DrawDispFrustum(K[ref_img ? LeftRefImage:RightRefImage],
                    ref_img ? system.GetTracker()->GetCameraInWorld()*T_cl : system.GetTracker()->GetCameraInWorld()*T_cr,
                    B,
                    min_disp,
                    max_disp,
                    cam_w,
                    cam_h);

            GLDrawer::RenderPointCloud(gl_cuda.pts, gl_cuda.pts_colour);

        }
        else
        {
            m_view.ActivateScissorAndClear();
            mest_view.ActivateScissorAndClear();
            warp_view.ActivateScissorAndClear();
            wview.ActivateScissorAndClear(world_cam);

            GLDrawer::DrawWorldCoord();
            for(int i = 0; i < 2; i++)
                GLDrawer::DrawCamera(K[i], i ? system.GetTracker()->GetCameraInWorld()*T_cr : system.GetTracker()->GetCameraInWorld()*T_cl, cur_imgs[pyr_level][i], cam_w, cam_h);

            GLDrawer::DrawDispFrustum(K[ref_img ? LeftRefImage:RightRefImage],
                    ref_img ? system.GetTracker()->GetCameraInWorld()*T_cl : system.GetTracker()->GetCameraInWorld()*T_cr,
                    B,
                    min_disp,
                    max_disp,
                    cam_w,
                    cam_h);

        }

        /* Tuning reconstruction */
        if(sys_visual && init_flag)
        {

            if(system.GetReconstor()->Reconstruct(cur_imgs[0],
                                                  CostVolume::Params(min_disp, max_disp, win_r, ref_img ? LeftRefImage:RightRefImage, data_term, min_bg_thr, max_bg_thr, frame_w, frame_h),
                                                  HuberROF::Params(num_itr, alpha, beta, epsilon, lambda, aux_theta)))
            {
                system.GetReconstor()->GetDisparityImage(gl_cuda.disp_imgs);
                system.GetReconstor()->GetPointCloud(cur_imgs[0][!ref_img],
                        gl_cuda.pts,
                        gl_cuda.pts_colour,
                        B,
                        K[0].data(),
                        ref_img ? (system.GetTracker()->GetCameraInWorld()*T_cl).matrix().data() : (system.GetTracker()->GetCameraInWorld()*T_cr).matrix().data());
            }
        }

        // Finish timing before swap buffers to avoid refresh sync
        sdkStopTimer(&timer);
        CalculateFPS();

        // Swap frames and Process Events
        FinishGlutFrame();

        gpu_memory = GetMemoryUsageMB();
    }    

    return 0;
}


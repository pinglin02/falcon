#include <dc1394/types.h>

#include <iostream>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//#include <pangolin/pangolin.h>
//#include <pangolin/video.h>
//#include <pangolin/timer.h>

using namespace std;
//using namespace pangolin;
using namespace cv;

int main(int argc, char* argv[])
{

    VideoCapture cap(argv[1]);
    if(!cap.isOpened())
        return -1;

    cap.set(CV_CAP_PROP_MODE, DC1394_VIDEO_MODE_1024x768_MONO8);
    cap.set(CV_CAP_PROP_FRAME_WIDTH, 1024);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, 768);

    size_t const w = cap.get(CV_CAP_PROP_FRAME_WIDTH);
    size_t const h = cap.get(CV_CAP_PROP_FRAME_HEIGHT);

    cout << w << " " << h << endl;

    while(true)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera

        imshow("video", frame);
        if(waitKey(30) >= 0) break;
    }

    /* Pangolin version

    // Setup Video Source
    VideoInput video(argv[1]);
    const VideoPixelFormat vid_fmt = video.PixFormat();
    const unsigned w = video.Width();
    const unsigned h = video.Height();

    cout << w << endl;
    cout << h << endl;


    // Work out appropriate GL channel and format options
    const GLint glchannels = vid_fmt.channels==1 ? GL_LUMINANCE:GL_RGB;
    const GLenum glformat = vid_fmt.channel_bits[0]==8?GL_UNSIGNED_BYTE:GL_UNSIGNED_SHORT;

    // Create Glut window
    pangolin::CreateWindowAndBind("Main",w, h);

    // Create viewport for video with fixed aspect
    View& vVideo = pangolin::CreateDisplay()
                   .SetBounds(0.0, 1.0, 0.0, 1.0, -(float)w/h);

    // OpenGl Texture for video frame.
    GlTexture texVideo(w,h,glchannels,false,0,GL_LUMINANCE,GL_UNSIGNED_BYTE);

    unsigned char* img = new unsigned char[video.SizeBytes()];

    for(int frame=0; !pangolin::ShouldQuit(); ++frame)
    {
        glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

        Timer t;
        if( video.GrabNext(img,true) )
            texVideo.Upload(img, GL_LUMINANCE, GL_UNSIGNED_BYTE);

        cout << 1.0/t.Elapsed_s() << endl;

        // Activate video viewport and render texture
        vVideo.Activate();
        texVideo.RenderToViewportFlipY();

        // Swap back buffer with front and process window events via GLUT
        pangolin::FinishFrame();
    }

    delete[] img;

    */

    return 0;
}

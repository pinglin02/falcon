#include <iostream>
#include <fstream>
#include <assert.h>
#include <sstream>

#include <helper_timer.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <boost/lexical_cast.hpp>
#include <boost/thread.hpp>

#include <pangolin/pangolin.h>

#include "falcon/cuda/cuda_data.h"
#include "falcon/reconst.h"
#include "falcon/gldraw.h"

using namespace std;
using namespace cv;
using namespace pangolin;
using namespace falcon;

char window_title[] = "Stereo Reconstruction";

StopWatchInterface *timer = 0;

void CalculateFPS()
{

    char fps[256];
    float ifps = 1.f / (sdkGetAverageTimerValue(&timer) / 1000.f);
    sprintf(fps, "%s: %3.1f fps", window_title, ifps);
//    glutSetWindowTitle(fps);
    sdkResetTimer(&timer);

}

double GetMemoryUsageMB()
{
    size_t avail;
    size_t total;
    cudaMemGetInfo(&avail, &total);

    return (total - avail)/1024.0/1024.0;

}

void ExportStereoView(uint32_t const x, uint32_t const y, uint32_t const width, uint32_t const height)
{

    Mat img(Size(width, height), CV_8UC4);
    glReadBuffer(GL_FRONT);
    glReadPixels(x, y, width, height, GL_BGRA, GL_UNSIGNED_BYTE, img.ptr<uint8_t>());

    flip(img, img, 0);

    imwrite("output.png", img);
}

size_t const num_imgs = 53;
string const img_name[2] = {"left_rec", "right_rec"} ;
string const mask_name[2] = {"mask_left_rec", "mask_right_rec"};
string const calib_name = "camera";

Matrix3f K[num_imgs][2];
VectorXf D[num_imgs][2];
Matrix3f R[num_imgs][2];
Vector3f t[num_imgs][2];
Matrix3f H[num_imgs][2];

void PassingCalibrationFile(string const dir)
{

    for(int img_idx = 0; img_idx < num_imgs; ++img_idx)
    {
        string calib_path = dir + "calibs/" + calib_name + boost::lexical_cast<string>((int)img_idx+1) + ".txt";

        ifstream calib_file(calib_path.c_str());
        string line;
        if (calib_file.is_open())
        {
            for(int i = 0; i < 4; ++i)
                getline(calib_file,line);

            string left_cam = line;
            stringstream ss(left_cam);
            float var;

            ss >> var;
            int w = var;

            ss >> var;
            int h = var;

            K[img_idx][0] = Matrix3f::Identity();
            for(int i = 0; i < 3; ++i)
                for(int j = 0; j < 3; ++j)
                    ss >> K[img_idx][0](i, j);

            D[img_idx][0] = VectorXf(8);
            for(int i = 0; i < 8; ++i)
                ss >> D[img_idx][0][i];

            R[img_idx][0] = Matrix3f::Identity();
            for(int i = 0; i < 3; ++i)
                for(int j = 0; j < 3; ++j)
                    ss >> R[img_idx][0](i, j);

            for(int i = 0; i < 3; ++i)
                ss >> t[img_idx][0][i];

            ss.str("");
            ss.clear();

            getline(calib_file,line);
            getline(calib_file,line);

            string right_cam = line;

            ss << right_cam;
            ss >> var >> var;

            K[img_idx][1] = Matrix3f::Identity();
            for(int i = 0; i < 3; ++i)
                for(int j = 0; j < 3; ++j)
                    ss >> K[img_idx][1](i, j);

            D[img_idx][1] = VectorXf(8);
            for(int i = 0; i < 8; ++i)
                ss >> D[img_idx][1][i];

            R[img_idx][1] = Matrix3f::Identity();
            for(int i = 0; i < 3; ++i)
                for(int j = 0; j < 3; ++j)
                    ss >> R[img_idx][1](i, j);

            for(int i = 0; i < 3; ++i)
                ss >> t[img_idx][1][i];

            ss.str("");
            ss.clear();

            getline(calib_file,line);
            getline(calib_file,line);
            getline(calib_file,line);

            getline(calib_file,line);

            string left_homo = line;            

            ss << left_homo;
            H[img_idx][1] = Matrix3f::Identity();
            for(int i = 0; i < 3; ++i)
                for(int j = 0; j < 3; ++j)
                    ss >> H[img_idx][0](i, j);

            ss.str("");
            ss.clear();

            getline(calib_file,line);
            string right_homo = line;

            ss << right_homo;
            H[img_idx][1] = Matrix3f::Identity();
            for(int i = 0; i < 3; ++i)
                for(int j = 0; j < 3; ++j)
                    ss >> H[img_idx][1](i, j);

            calib_file.close();
        }

    }

}

void DisparityExport(CUDAImage<Pitched2D,float> const& real_disp,
                     short const min_disp,
                     int const img_idx,
                     string const dir)
{

    int w = real_disp.GetWidth();
    int h = real_disp.GetHeight();

    int num_pts = w*h;
    float* disp = new float[num_pts];

    checkCudaErrors(cudaMemcpy2D(disp, w*sizeof(float),
                                 real_disp.ptr, real_disp.pitch,
                                 w*sizeof(float), h,
                                 cudaMemcpyDeviceToHost));

    ofstream file;
    file.open((dir + "reconst_disparity" + boost::lexical_cast<string>(img_idx+1) + ".txt").c_str());
    for(int y = 0; y < h; ++y) for(int x = 0; x < w; ++x)
    {
        if(disp[y*w+x] == min_disp)
            file << x << " " << y << " " << 0 << endl;
        else
            file << x << " " << y << " " << disp[y*w+x] << endl;
    }

    file.close();

    delete [] disp;
}



void TriangulationExport(CUDAData<OpenGLBufferType,float3>& pts,
                         size_t const w,
                         size_t const h,
                         int const img_idx,
                         string const dir)
{

    int num_pts = pts.GetNumElemenets();
    float3* pts_w = new float3[num_pts];

    pts.MapCUDARes();
    checkCudaErrors(cudaMemcpy(pts_w,
                               pts.GetPtr(),
                               num_pts*sizeof(float3),
                               cudaMemcpyDeviceToHost));
    pts.UnmapCUDARes();

    ofstream file;
    file.open((dir + boost::lexical_cast<string>(img_idx+1) + ".xyz").c_str());
    for(int y = 0; y < h; ++y) for(int x = 0; x < w; ++x)
    {

        if(pts_w[y*w+x].x == 0 && pts_w[y*w+x].y == 0 && pts_w[y*w+x].z == 0 )
            continue;

        file << x << " " << y << " " << pts_w[y*w+x].x << " " << pts_w[y*w+x].y << " " << pts_w[y*w+x].z << endl;
    }

    file.close();

    delete [] pts_w;
}

void Triangulation(CUDAImage<Pitched2D,float> const& real_disp,
                   CUDAData<OpenGLBufferType,float3>& pts,
                   CUDAData<OpenGLBufferType,uchar4>& pts_colour,
                   CostVolume::Params const cv_params,
                   Mat const img,
                   Mat const mask,
                   int const img_idx,
                   bool const use_distort = false)
{

    int w = real_disp.GetWidth();
    int h = real_disp.GetHeight();

    int num_pts = w*h;
    float* disp = new float[num_pts];

    checkCudaErrors(cudaMemcpy2D(disp, w*sizeof(float),
                                 real_disp.ptr, real_disp.pitch,
                                 w*sizeof(float), h,
                                 cudaMemcpyDeviceToHost));

    float3* pts_w = new float3[num_pts];
    uchar4* colour = new uchar4[num_pts];


    for(int i = 0; i < num_pts; ++i)
    {
        pts_w[i].x = 0;
        pts_w[i].y = 0;
        pts_w[i].z = 0;
    }

    for(int y = 0; y < h; ++y) for(int x = 0; x < w; ++x)
    {

        if(!(x >= cv_params.roi[cv_params.ref_img].x_min && x <= cv_params.roi[cv_params.ref_img].x_max &&
           y >= cv_params.roi[cv_params.ref_img].y_min && y <= cv_params.roi[cv_params.ref_img].y_max))
            continue;

        if(disp[y*w+x] == cv_params.min_disp || mask.at<Vec4b>(y, x)[0] > 0)
            continue;        

        colour[y*w+x].x = img.at<Vec4b>(y, x)[0];
        colour[y*w+x].y = img.at<Vec4b>(y, x)[1];
        colour[y*w+x].z = img.at<Vec4b>(y, x)[2];
        colour[y*w+x].w = 255;

        float rect_pts[2][2];
        rect_pts[0][0] = x;
        rect_pts[0][1] = y;

        rect_pts[1][0] = x - disp[y*w+x];
        rect_pts[1][1] = y;

        /* Inverse homography */
        Vector3f homo_pts[2];
        for(int i = 0; i < 2; ++i)
            homo_pts[i] = H[img_idx][i]*Vector3f(rect_pts[i][0], rect_pts[i][1], 1.0f);

        float unrect_pts[2][2];
        for(int i = 0; i < 2; ++i)
        {
            unrect_pts[i][0] = homo_pts[i][0]/homo_pts[i][2];
            unrect_pts[i][1] = homo_pts[i][1]/homo_pts[i][2];
        }        

        /* Distort points */
        float nm_pts[2][2];
        if(use_distort)
        {
            for(int i = 0; i < 2; ++i)
            {

                /* Transform back to the normalised coordinate */
                float nx = (unrect_pts[i][0] - K[img_idx][i](0, 2)) / K[img_idx][i](0, 0);
                float ny = (unrect_pts[i][1] - K[img_idx][i](1, 2)) / K[img_idx][i](1, 1);

                float r = sqrt(nx*nx + ny*ny);
                float r2 = r*r;
                float r4 = r2*r2;
                float rc = (1.0f + D[img_idx][i][0]*r2 + D[img_idx][i][1]*r4 + D[img_idx][i][4]*r2*r4) /
                        (1.0f + D[img_idx][i][5]*r2 + D[img_idx][i][6]*r4 + D[img_idx][i][7]*r2*r4);

                float tx = D[img_idx][i][2]*(2.0f*nx*ny) + D[img_idx][i][3]*(r2+2.0f*nx*nx);
                float ty = D[img_idx][i][2]*(r2+2.0f*ny*ny) + D[img_idx][i][3]*(2.0f*nx*ny);

                /* Distort image and leave point in normalised coordinate */
                nm_pts[i][0] = (rc*nx+tx);
                nm_pts[i][1] = (rc*ny+ty);
            }
        }
        else
        {
            for(int i = 0; i < 2; ++i)
            {
                /* Transform back to the normalised coordinate */
                nm_pts[i][0] = (unrect_pts[i][0] - K[img_idx][i](0, 2)) / K[img_idx][i](0, 0);
                nm_pts[i][1] = (unrect_pts[i][1] - K[img_idx][i](1, 2)) / K[img_idx][i](1, 1);
            }
        }


        /* Sebastian's triagulation */
//        Vector3f a = -R[img_idx][0].transpose()*t[img_idx][0];
//        Vector3f b = -R[img_idx][1].transpose()*t[img_idx][1];

//        Vector3f u = a - Vector3f(nm_pts[0][0], nm_pts[0][1], 1.0);
//        Vector3f v = b - Vector3f(nm_pts[1][0], nm_pts[1][1], 1.0);

//        float A00 = -u[0]; float A01 = v[0];
//        float A10 = -u[1]; float A11 = v[1];
//        float A20 = -u[2]; float A21 = v[2];
//        float b0 = a[0] - b[0];
//        float b1 = a[1] - b[1];
//        float b2 = a[2] - b[2];

//        float ATA00 = A00 * A00 + A10 * A10 + A20 * A20;
//        float ATA10 = A01 * A00 + A11 * A10 + A21 * A20;
//        float ATA11 = A01 * A01 + A11 * A11 + A21 * A21;
//        float ATb0 = A00 * b0 + A10 * b1 + A20 * b2;
//        float ATb1 = A01 * b0 + A11 * b1 + A21 * b2;

//        float L00 = sqrtf(ATA00);
//        float L10 = ATA10 / L00;
//        float L11 = sqrtf(ATA11 - L10 * L10);

//        float yy0 = ATb0 / L00;
//        float yy1 = (ATb1 - L10 * yy0) / L11;

//        float s = yy1 / L11;
//        float r = (yy0 - L10 * s) / L00;

//        pts_w[y*w+x].x = (r * u[0] + a[0] + s * v[0] + b[0]) * 0.5f;
//        pts_w[y*w+x].y = (r * u[1] + a[1] + s * v[1] + b[1]) * 0.5f;
//        pts_w[y*w+x].z = (r * u[2] + a[2] + s * v[2] + b[2]) * 0.5f;

        // Dan's triangulation
        float u[3];
        u[0] = R[img_idx][1](0, 0)*nm_pts[0][0] + R[img_idx][1](0, 1)*nm_pts[0][1] + R[img_idx][1](0, 2);
        u[1] = R[img_idx][1](1, 0)*nm_pts[0][0] + R[img_idx][1](1, 1)*nm_pts[0][1] + R[img_idx][1](1, 2);
        u[2] = R[img_idx][1](2, 0)*nm_pts[0][0] + R[img_idx][1](2, 1)*nm_pts[0][1] + R[img_idx][1](2, 2);

        float sq_norm[2];
        for(int i = 0; i < 2; ++i)
            sq_norm[i] = nm_pts[i][0]*nm_pts[i][0] + nm_pts[i][1]*nm_pts[i][1] + 1.0;

        float DD = sq_norm[0]*sq_norm[1] - (u[0]*nm_pts[1][0]+u[1]*nm_pts[1][1]+u[2])*(u[0]*nm_pts[1][0]+u[1]*nm_pts[1][1]+u[2]);

        float  duT = u[0]*t[img_idx][1][0]+u[1]*t[img_idx][1][1]+u[2]*t[img_idx][1][2];
        float  dnmrT = nm_pts[1][0]*t[img_idx][1][0]+nm_pts[1][1]*t[img_idx][1][1]+t[img_idx][1][2];
        float  dunmr = nm_pts[1][0]*u[0]+nm_pts[1][1]*u[1]+u[2];

        float NN1 = dunmr*dnmrT - sq_norm[1]*duT;
        float NN2 = sq_norm[0]*dnmrT - duT*dunmr;

        //
        float Zl = NN1/DD;
        float Zr = NN2/DD;

        float X1[3];
        X1[0] = nm_pts[0][0]*Zl;
        X1[1] = nm_pts[0][1]*Zl;
        X1[2] = Zl;


        float X2[3];
        X2[0] = R[img_idx][1](0, 0)*(nm_pts[1][0]*Zr - t[img_idx][1][0]) + R[img_idx][1](1, 0)*(nm_pts[1][1]*Zr - t[img_idx][1][1]) + R[img_idx][1](2, 0)*(Zr - t[img_idx][1][2]);
        X2[1] = R[img_idx][1](0, 1)*(nm_pts[1][0]*Zr - t[img_idx][1][0]) + R[img_idx][1](1, 1)*(nm_pts[1][1]*Zr - t[img_idx][1][1]) + R[img_idx][1](2, 1)*(Zr - t[img_idx][1][2]);
        X2[2] = R[img_idx][1](0, 2)*(nm_pts[1][0]*Zr - t[img_idx][1][0]) + R[img_idx][1](1, 2)*(nm_pts[1][1]*Zr - t[img_idx][1][1]) + R[img_idx][1](2, 2)*(Zr - t[img_idx][1][2]);

        pts_w[y*w+x].x = (X1[0]+X2[0])*.5f;
        pts_w[y*w+x].y = (X1[1]+X2[1])*.5f;
        pts_w[y*w+x].z = (X1[2]+X2[2])*.5f;

    }

    pts.MapCUDARes();
    checkCudaErrors(cudaMemcpy(pts.GetPtr(),
                               pts_w,
                               num_pts*sizeof(float3),
                               cudaMemcpyHostToDevice));

    pts.UnmapCUDARes();


    pts_colour.MapCUDARes();
    checkCudaErrors(cudaMemcpy(pts_colour.GetPtr(),
                               colour,
                               num_pts*sizeof(uchar4),
                               cudaMemcpyHostToDevice));

    pts_colour.UnmapCUDARes();

    delete disp;
    delete pts_w;
    delete colour;

}

////////////////////////////////////////////////////////////////////////////
//  2D Handler for Pangolin view
////////////////////////////////////////////////////////////////////////////
struct Handler2D : Handler
{

    Handler2D() : in_view(false), pressed(false){}

    void Mouse(View& view, MouseButton button, int x, int y, bool pressed, int button_state)
    {

        in_view = (x > view.vp.l && x < view.vp.r() && y > view.vp.b && y < view.vp.t()) ? true : false;

        this->button = button;
        this->pressed = pressed;

        last_pos[0] = x;
        last_pos[1] = y;
    }

    void MouseMotion(View& view, int x, int y, int button_state)
    {
        in_view = (x > view.vp.l && x < view.vp.r() && y > view.vp.b && y < view.vp.t()) ? true : false;

        last_pos[0] = x;
        last_pos[1] = y;
    }

    bool IsInsideView() const { return in_view; }
    bool IsPressed() const { return pressed; }

    bool IsLeftButtonClicked()
    {
        bool left_clicked = (button == MouseButtonLeft && pressed);

        button = (MouseButton)0;

        return left_clicked;
    }

    GLint const* GetLastPos() const { return last_pos; }

protected:
    MouseButton button;

    bool pressed;
    bool in_view;
    GLint last_pos[2];
};


int main(int argc, char* argv[])
{

    if(argc < 2)
    {
        cerr << "Usage: ./TMI <data_dir>" << endl;
        exit(EXIT_FAILURE);
    }

    string const dir = string(argv[1]);

    PassingCalibrationFile(dir);

    /* Load images */
    Mat imgs[2], masks[2];
    for(int i = 0; i < 2; ++i)
    {
        cvtColor(imread(dir + "imgs/" + img_name[i]+"1.bmp"), imgs[i], CV_BGR2RGBA, 4);
        cvtColor(imread(dir + "imgs/" + mask_name[i]+"1.bmp"), masks[i], CV_BGR2RGBA, 4);
    }

    uint32_t img_w = imgs[0].size().width;
    uint32_t img_h = imgs[0].size().height;

    float B = 5.0;

    /* Rectification images with lines */
    Mat line_imgs[2];
    for(int i = 0; i < 2; ++i)
    {
        imgs[i].copyTo(line_imgs[i]);
        for(int j=0; j < line_imgs[i].rows; j += 16)
            line(line_imgs[i], Point(0, j), Point(line_imgs[i].cols, j), Scalar(0, 255, 0, 255), 1, 8);
    }

    /////////////////////////////////////////////////
    //  Pangolin UI Setting
    /////////////////////////////////////////////////
    /* OpenGL, GLEW, Freeglut initialization */
    int const UI_WIDTH = 180;
    CreateGlutWindowAndBind(window_title, UI_WIDTH+3*(img_w/2.5), 2*(img_h/2.5), GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glClearColor(1.0, 1.0, 1.0, 1.0);

    GLDrawer::near = 0.1;
    GLDrawer::far  = 10000;

    GLenum err = glewInit();
    if(GLEW_OK != err)
    {
        std::cerr << "GLEW Error: " << glewGetErrorString(err) << std::endl;
        exit(0);
    }

    /* Views */
    View& lview = Display("Left Image View")
            .SetAspect((float)img_w/(float)img_h)
            .SetHandler(new Handler2D);

    View& rview = Display("Right Image View")
            .SetAspect((float)img_w/(float)img_h)
            .SetHandler(new Handler2D);

    View& mask_lview = Display("Left Mask View")
            .SetAspect((float)img_w/(float)img_h)
            .SetHandler(new Handler2D);

    View& mask_rview = Display("Right Mask View")
            .SetAspect((float)img_w/(float)img_h)
            .SetHandler(new Handler2D);

    View& mview = Display("Disparity View")
            .SetAspect((float)img_w/(float)img_h)
            .SetHandler(new Handler2D);

    OpenGlRenderState world_cam(
                ProjectionMatrix(img_w, img_h, img_h, img_h, img_w/2.0, img_h/2.0, GLDrawer::near, GLDrawer::far),
                ModelViewLookAt(5.0, -5.0, -5.0, 0.0, 0.0, 0.0, AxisNegY));

    View& wview = Display("World View")
            .SetAspect((float)img_w/(float)img_h)
            .SetHandler(new Handler3D(world_cam));

    Display("multi")
            .SetBounds(0.0, 1.0, Attach::Pix(UI_WIDTH), 1.0)
            .SetLayout(LayoutEqual)
            .AddDisplay(lview)
            .AddDisplay(rview)
            .AddDisplay(mview)
            .AddDisplay(mask_lview)
            .AddDisplay(mask_rview)
            .AddDisplay(wview);

    /* UI */
    View& d_panel = CreatePanel("ui")
        .SetBounds(0.0, 1.0, 0.0, Attach::Pix(UI_WIDTH));

    Var<double> gpu_memory("ui.GPU Memory", GetMemoryUsageMB(), 0, 1024);    

    Var<int> img_idx("ui.Image No", 0, 0, num_imgs-1);
    int cur_img_idx = 0;
    Var<bool> show_rect("ui.Show Rect Image", false, true);

    /* Cost volume parameters */
    /* HD */
    Var<int> min_disp("ui.Min Disparity", 40, 0, 200);
    Var<int> max_disp("ui.Max Disparity", 120, 0, 200);

    /* PAL */
//    Var<int> min_disp("ui.Min Disparity", 10, 0, 50);
//    Var<int> max_disp("ui.Max Disparity", 80, 0, 200);


    Var<int> min_bg_thr("ui.Min Bg Threshold", 1, 0, 255);
    Var<int> max_bg_thr("ui.Max Bg Threshold", 255, 0, 255);

    Var<int> win_r("ui.Window Radius", 10, 0, 20);

    Var<bool> ref_img("ui.Reconst Left Camera", true, true);

    Var<bool> ad_button("ui.Absolute Distance (AD)", false, false);
    Var<bool> zncc_button("ui.ZNCC", false, false);
    DataTermMethod data_term = ZNCC;

    /* Huber-L1 TGV1 parameterrs */
    Var<int> num_itr("ui.Number of Iterations", 400, 0, 500);
    Var<double> alpha("ui.Alpha", 0.1, 0.01, 20.0, true);
    Var<double> beta("ui.Beta", 1.0, 0.1, 5.0);
    Var<double> epsilon("ui.Epsilon", 1e-1, 1e-3, 1.0);
    Var<double> lambda("ui.Lambda", 1e-3, 1e-4, 10.0, true);
    Var<double> aux_theta("ui.Aux Theta", 10.0, 1e-1, 1000, true);
    Var<double> aux_theta_gamma("ui.Aux Theta Gamma", 1e-6, 1e-7, 1e-5, true);

    /* World drawer parameters */
    Var<double> v_w_s("ui_sys.World Scaler", 1, 1e-2, 1e2, true);
    Var<double> v_c_s("ui_sys.Camera Scaler", 1e2, 1, 1e5, true);

    /* Export view buttons */
    Var<bool> export_stereo_view_button("ui.Export stereo view", false, false);

    /* OpenGl textures */
    CUDAImage<OpenGL2D,uchar4> cuda_imgs[2];
    CUDAImage<OpenGL2D,uchar4> cuda_masks[2];
    CUDAImage<OpenGL2D,uchar4> cuda_line_imgs[2];
    for(uint32_t i = 0; i < 2; i++)
    {
        cuda_imgs[i].Reinitialise(img_w, img_h);
        cuda_imgs[i].Upload(imgs[i].data, GL_RGBA, GL_UNSIGNED_BYTE);

        cuda_masks[i].Reinitialise(img_w, img_h);
        cuda_masks[i].Upload(masks[i].data, GL_RGBA, GL_UNSIGNED_BYTE);

        cuda_line_imgs[i].Reinitialise(img_w, img_h);
        cuda_line_imgs[i].Upload(line_imgs[i].data, GL_RGBA, GL_UNSIGNED_BYTE);
    }

    CUDAImage<OpenGL2D,uchar4> cuda_disp(img_w, img_h);

    double gpu_mem_begin = GetMemoryUsageMB();
    cout << "GPU memory begins: " << gpu_mem_begin << endl;

    StereoReconstructor stereo_reconst(CostVolume::Params(min_disp, max_disp, win_r, ref_img ? LeftRefImage:RightRefImage, ZNCC, min_bg_thr, max_bg_thr, img_w, img_h),
                                       HuberROF::Params(num_itr, alpha, beta, epsilon, lambda, aux_theta));

    CUDAImage<Pitched2D,float> real_disp;

    stereo_reconst.Reconstruct(cuda_imgs, ref_img ? LeftRefImage : RightRefImage);

    double gpu_mem_end = GetMemoryUsageMB();
    cout << "GPU memory ends: " << gpu_mem_end << endl;
    cout << "GPU memory usage: " << gpu_mem_end - gpu_mem_begin << endl;

    /* Point cloud data */
    CUDAData<OpenGLBufferType,float3> pts(OpenGLArrayBuffer, img_w*img_h, sizeof(float3));
    CUDAData<OpenGLBufferType,uchar4> pts_colour(OpenGLArrayBuffer, img_w*img_h, sizeof(uchar4));

    /* TMI export */
//    for(int img_idx = 0; img_idx < num_imgs; ++img_idx)
//    {
//        for(int i = 0; i < 2; ++i)
//        {
//            cvtColor(imread(dir + "imgs/" + img_name[i] + boost::lexical_cast<string>((int)img_idx+1) + ".bmp"), imgs[i], CV_BGR2RGBA, 4);
//            cvtColor(imread(dir + "imgs/" + mask_name[i] + boost::lexical_cast<string>((int)img_idx+1) + ".bmp"), masks[i], CV_BGR2RGBA, 4);

//            imgs[i].copyTo(line_imgs[i]);
//            for(int j=0; j < line_imgs[i].rows; j += 16)
//                line(line_imgs[i], Point(0, j), Point(line_imgs[i].cols, j), Scalar(0, 255, 0, 255), 1, 8);

//            cuda_imgs[i].Upload(imgs[i].data, GL_RGBA, GL_UNSIGNED_BYTE);
//            cuda_masks[i].Upload(masks[i].data, GL_RGBA, GL_UNSIGNED_BYTE);
//            cuda_line_imgs[i].Upload(line_imgs[i].data, GL_RGBA, GL_UNSIGNED_BYTE);
//        }

//        stereo_reconst.Reconstruct(cuda_imgs, LeftRefImage);
//        stereo_reconst.GetRealDisparity(real_disp);

//        Triangulation(real_disp,
//                      pts,
//                      pts_colour,
//                      CostVolume::Params(min_disp, max_disp, win_r, ref_img ? LeftRefImage : RightRefImage, data_term, min_bg_thr, max_bg_thr, img_w, img_h),
//                      imgs[0], masks[0], img_idx);

//        /* Export triangulated 3D point cloud */
//        TriangulationExport(pts, img_w, img_h, img_idx, dir);

//        /* Export disparity map */
////        DisparityExport(real_disp, min_disp, img_idx, dir);

//        cout << img_idx << endl;
//    }

    stereo_reconst.GetDisparityImage(cuda_disp);
    stereo_reconst.GetRealDisparity(real_disp);
    stereo_reconst.GetPointCloud(cuda_imgs[0], pts, pts_colour, B, K[img_idx][0].data(), SE3f().matrix().data());

//    Triangulation(real_disp,
//                  pts,
//                  pts_colour,
//                  CostVolume::Params(min_disp, max_disp, win_r, ref_img ? LeftRefImage : RightRefImage, data_term, min_bg_thr, max_bg_thr, img_w, img_h),
//                  imgs[0], masks[0], img_idx);

    sdkCreateTimer(&timer);
    while(!ShouldQuit())
    {
        /* The very beginning of a system iteration */
        sdkStartTimer(&timer);

        /* Drawer variables */
        GLDrawer::w_s = v_w_s;
        GLDrawer::c_s = v_c_s;

        SE3f T_ext(R[img_idx][1], t[img_idx][1]);

        if(HasResized())
            DisplayBase().ActivateScissorAndClear();

        // Render our UI panel when we receive input
        if(HadInput())
            d_panel.Render();

        if(min_disp > max_disp)
            min_disp = max_disp;

        if(max_disp < min_disp)
            max_disp = min_disp;

        if(Pushed(ad_button))
            data_term = AD;

        if(Pushed(zncc_button))
            data_term = ZNCC;

        if(((Handler2D*)lview.handler)->IsPressed())
        {
            float x = ((Handler2D*)lview.handler)->GetLastPos()[0] - lview.vp.l;
            float y = ((Handler2D*)lview.handler)->GetLastPos()[1] - lview.vp.b;
            cout << "left: " << "(" << x << "," << y << ")" << endl;
            boost::this_thread::sleep(boost::posix_time::milliseconds(200));
        }

        if(((Handler2D*)rview.handler)->IsPressed())
        {
            float x = ((Handler2D*)rview.handler)->GetLastPos()[0] - rview.vp.l;
            float y = ((Handler2D*)rview.handler)->GetLastPos()[1] - rview.vp.b;
            cout << "right: " << "(" << x << "," << y << ")" << endl;
            boost::this_thread::sleep(boost::posix_time::milliseconds(200));
        }

        if(((Handler2D*)mview.handler)->IsPressed())
        {
            float x = ((Handler2D*)mview.handler)->GetLastPos()[0] - mview.vp.l;
            float y = ((Handler2D*)mview.handler)->GetLastPos()[1] - mview.vp.b;
            cout << "disparity: " << "(" << x << "," << y << ")" << endl;
            boost::this_thread::sleep(boost::posix_time::milliseconds(200));
        }

        if(((Handler2D*)mask_lview.handler)->IsPressed())
        {
            float x = ((Handler2D*)mask_lview.handler)->GetLastPos()[0] - lview.vp.l;
            float y = ((Handler2D*)mask_lview.handler)->GetLastPos()[1] - lview.vp.b;
            cout << "left: " << "(" << x << "," << y << ")" << endl;
            boost::this_thread::sleep(boost::posix_time::milliseconds(200));
        }

        if(((Handler2D*)mask_rview.handler)->IsPressed())
        {
            float x = ((Handler2D*)mask_rview.handler)->GetLastPos()[0] - rview.vp.l;
            float y = ((Handler2D*)mask_rview.handler)->GetLastPos()[1] - rview.vp.b;
            cout << "right: " << "(" << x << "," << y << ")" << endl;
            boost::this_thread::sleep(boost::posix_time::milliseconds(200));
        }


        /* Switching images */
        if(cur_img_idx != img_idx)
        {
            for(int i = 0; i < 2; ++i)
            {
                cvtColor(imread(dir + "imgs/" + img_name[i] + boost::lexical_cast<string>((int)img_idx+1) + ".bmp"), imgs[i], CV_BGR2RGBA, 4);
                cvtColor(imread(dir + "imgs/" + mask_name[i] + boost::lexical_cast<string>((int)img_idx+1) + ".bmp"), masks[i], CV_BGR2RGBA, 4);

                imgs[i].copyTo(line_imgs[i]);
                for(int j=0; j < line_imgs[i].rows; j += 16)
                    line(line_imgs[i], Point(0, j), Point(line_imgs[i].cols, j), Scalar(0, 255, 0, 255), 1, 8);

                cuda_imgs[i].Upload(imgs[i].data, GL_RGBA, GL_UNSIGNED_BYTE);
                cuda_masks[i].Upload(masks[i].data, GL_RGBA, GL_UNSIGNED_BYTE);
                cuda_line_imgs[i].Upload(line_imgs[i].data, GL_RGBA, GL_UNSIGNED_BYTE);
            }

            stereo_reconst.Reconstruct(cuda_imgs, ref_img ? LeftRefImage : RightRefImage);
            stereo_reconst.GetDisparityImage(cuda_disp);

            stereo_reconst.GetRealDisparity(real_disp);
            stereo_reconst.GetPointCloud(cuda_imgs[0], pts, pts_colour, B, K[img_idx][0].data(), SE3f().matrix().data());

//            Triangulation(real_disp,
//                          pts,
//                          pts_colour,
//                          CostVolume::Params(min_disp, max_disp, win_r, ref_img ? LeftRefImage : RightRefImage, data_term, min_bg_thr, max_bg_thr, img_w, img_h),
//                          imgs[0], masks[0], img_idx);

            cur_img_idx = img_idx;
            boost::this_thread::sleep(boost::posix_time::milliseconds(2000));
        }

        /* Main system */        
        if(stereo_reconst.Reconstruct(cuda_imgs,
                                      CostVolume::Params(min_disp, max_disp, win_r, ref_img ? LeftRefImage : RightRefImage, data_term, min_bg_thr, max_bg_thr, img_w, img_h),
                                      HuberROF::Params(num_itr, alpha, beta, epsilon, lambda, aux_theta)))
        {
            stereo_reconst.GetDisparityImage(cuda_disp);

            stereo_reconst.GetRealDisparity(real_disp);
            stereo_reconst.GetPointCloud(cuda_imgs[0], pts, pts_colour, B, K[img_idx][0].data(), SE3f().matrix().data());

//            Triangulation(real_disp,
//                          pts,
//                          pts_colour,
//                          CostVolume::Params(min_disp, max_disp, win_r, ref_img ? LeftRefImage : RightRefImage, data_term, min_bg_thr, max_bg_thr, img_w, img_h),
//                          imgs[0], masks[0], img_idx);

        }

        /* Left video frame view */
        lview.ActivateScissorAndClear();
        if(show_rect)
            cuda_line_imgs[0].RenderToViewportFlipY();
        else
            cuda_imgs[0].RenderToViewportFlipY();

        /* Right video frame view */
        rview.ActivateScissorAndClear();
        if(show_rect)
            cuda_line_imgs[1].RenderToViewportFlipY();
        else
            cuda_imgs[1].RenderToViewportFlipY();

        /* Disparity view */
        mview.ActivateScissorAndClear();
        cuda_disp.RenderToViewportFlipY();

        mask_lview.ActivateScissorAndClear();
        cuda_masks[0].RenderToViewportFlipY();

        mask_rview.ActivateScissorAndClear();
        cuda_masks[1].RenderToViewportFlipY();

        wview.ActivateScissorAndClear(world_cam);
        GLDrawer::DrawWorldCoord();

        for(int i = 0; i < 2; ++i)
            GLDrawer::DrawCamera(i ? K[img_idx][1]:K[img_idx][0], i ? T_ext.inverse() : SE3f(), cuda_imgs[i], img_w, img_h);

        GLDrawer::DrawDispFrustum(ref_img ? K[img_idx][0] : K[img_idx][1], ref_img ? SE3f() : T_ext.inverse(),
                                  B, min_disp, max_disp, img_w, img_h);

        GLDrawer::RenderPointCloud(pts, pts_colour);

        /* Export views */
        if(Pushed(export_stereo_view_button))
            ExportStereoView(lview.vp.l, lview.vp.b, 2*lview.vp.w, lview.vp.h);

        // Finish timing before swap buffers to avoid refresh sync
        sdkStopTimer(&timer);
        CalculateFPS();

        // Swap frames and Process Events
        FinishGlutFrame();

        gpu_memory = GetMemoryUsageMB();
    }

    return 0;
}


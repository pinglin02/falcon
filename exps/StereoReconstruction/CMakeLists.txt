###############################################################################
# OpenCV: http://opencv.willowgarage.com/wiki/
FIND_PACKAGE(OpenCV REQUIRED)
INCLUDE_DIRECTORIES(${OpenCV_INCLUDE_DIR})
LINK_DIRECTORIES(${OpenCV_LIBRARY_DIRS})
LINK_LIBRARIES(${OpenCV_LIBS})

FIND_PACKAGE(Pangolin REQUIRED)
INCLUDE_DIRECTORIES(${Pangolin_INCLUDE_DIRS})
LINK_DIRECTORIES(${Pangolin_LIBRARY_DIRS})
LINK_LIBRARIES(${Pangolin_LIBRARIES})

FIND_PACKAGE(Falcon REQUIRED)
INCLUDE_DIRECTORIES(${Falcon_INCLUDE_DIRS})
LINK_DIRECTORIES(${Falcon_LIBRARY_DIRS})
LINK_LIBRARIES(${Falcon_LIBRARIES})

FIND_PACKAGE(CUDA REQUIRED)

CUDA_ADD_EXECUTABLE(StereoVideoReconstruction stereo_disp_video.cpp)
CUDA_ADD_EXECUTABLE(StereoRectImgReconst stereo_disp_img.cpp)

#include <iostream>
#include <assert.h>

#include <helper_timer.h>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <boost/thread/thread.hpp>
#include <pangolin/pangolin.h>

#include "falcon/cuda/cuda_data.h"
#include "falcon/reconst.h"
#include "falcon/gldraw.h"

using namespace std;
using namespace cv;
using namespace pangolin;
using namespace falcon;

char window_title[] = "Stereo Reconstruction";

StopWatchInterface *timer = 0;

void CalculateFPS()
{

    char fps[256];
    float ifps = 1.f / (sdkGetAverageTimerValue(&timer) / 1000.f);
    sprintf(fps, "%s: %3.1f fps", window_title, ifps);
//    glutSetWindowTitle(fps);
    sdkResetTimer(&timer);

}

double GetMemoryUsageMB()
{
    size_t avail;
    size_t total;
    cudaMemGetInfo(&avail, &total);

    return (total - avail)/1024.0/1024.0;

}

int main(int argc, char* argv[])
{

    if(argc < 4)
    {
        cerr << "Usage: ./StereoImgsReconstruction <left_video> <right_video> <cam_calib_file>" << endl;
        exit(EXIT_FAILURE);
    }

    /* Camera calibration */
    uint32_t cam_w, cam_h;
    Mat cv_RMAP[2][2];
    Matrix3f K[2];
    SE3f T_ext;
    float B;
    if(argc == 4)
    {
        FileStorage fs(argv[3], FileStorage::READ); // Read the settings
        if (!fs.isOpened())
        {
            cerr << __PRETTY_FUNCTION__ << ": Could not open the camera calibration file: \"" << argv[3] << "\"" << endl;
            exit(EXIT_FAILURE);
        }
        assert((string)fs["CameraType"] == "stereo");


        cam_w = (int)fs["ImageWidth"];
        cam_h = (int)fs["ImageHeight"];

        Mat cv_K[2], cv_D[2];
        fs["Kl"] >> cv_K[0];
        fs["Kr"] >> cv_K[1];
        assert(cv_K[0].size() == Size(3, 3)  && cv_K[1].size() == Size(3, 3));

        fs["Dl"] >> cv_D[0];
        fs["Dr"] >> cv_D[1];
        assert(cv_D[0].size() == Size(1, 5)  && cv_D[1].size() == Size(1, 5));

        Mat R, t;
        fs["R"] >> R;
        fs["t"] >> t;
        assert(R.type() == CV_64F && t.type() == CV_64F);

        Mat cv_Tlr = Mat::eye(4, 4, CV_64F);
        R.copyTo(cv_Tlr.rowRange(0, 3).colRange(0, 3));
        t.copyTo(cv_Tlr.rowRange(0, 3).colRange(3, 4));

        fs.release();

        /* Stereo rectification */
        Mat cv_RectRot[2], cv_RectProj[2], cv_Q;
        Rect cv_roi[2];
        stereoRectify(cv_K[0],
                      cv_D[0],
                      cv_K[1],
                      cv_D[1],
                      Size(cam_w, cam_h),
                      R,
                      t,
                      cv_RectRot[0],
                      cv_RectRot[1],
                      cv_RectProj[0],
                      cv_RectProj[1],
                      cv_Q,
                      CV_CALIB_ZERO_DISPARITY,
                      0,
                      Size(cam_w, cam_h),
                      &cv_roi[0],
                      &cv_roi[1]);

        /* Get the rectification re-map index */
        for(uint8_t i = 0; i < 2; i++)
        {
            initUndistortRectifyMap(cv_K[i],
                                    cv_D[i],
                                    cv_RectRot[i],
                                    cv_RectProj[i],
                                    Size(cam_w, cam_h),
                                    CV_16SC2,
                                    cv_RMAP[i][0],
                                    cv_RMAP[i][1]);
        }

//        cout << "Kl = " << cv_K[0] << endl;
//        cout << "Kr = " << cv_K[1] << endl;

//        cout << "new_Kl = " << cv_RectProj[0] << endl;
//        cout << "new_Kr = " << cv_RectProj[1] << endl;

//        cout << "Q = " << cv_Q << endl;

//        cout << "B1 = " << (float)(st_cam.RectProj[1].at<double>(0, 3)/st_cam.RectProj[0].at<double>(0, 0)) << endl;
//        cout << "B2 = " << (float)(st_cam.RectProj[1].at<double>(0, 3)/st_cam.K[0].at<double>(0, 0)) << endl;
//        cout << "B3 = " << cv::norm(t) << endl;

//        cout << st_cam.Text << endl;

        /* Using float type in OpenGL is much much more faster!! */
        cv_RectProj[0].rowRange(0, 3).colRange(0, 3).copyTo(cv_K[0]);
        cv_RectProj[1].rowRange(0, 3).colRange(0, 3).copyTo(cv_K[1]);

        cv_K[0].convertTo(cv_K[0], CV_32F);
        cv_K[1].convertTo(cv_K[1], CV_32F);

        K[0] = Map<Matrix<float,3,3,RowMajor> >((float*)cv_K[0].data);
        K[1] = Map<Matrix<float,3,3,RowMajor> >((float*)cv_K[1].data);

        B = cv::norm(t);
        T_ext = SE3f(SE3Group<float>(SO3f(), SE3Group<float>::Point(-B,0,0)));

    }

    /* Import video */
    VideoCapture video_capturer[2];
    for(uint8_t i = 0; i < 2; i++)
    {
        video_capturer[i].open(argv[i+1]);
        if(!video_capturer[i].isOpened())
        {
            cerr << __PRETTY_FUNCTION__ << ": Can't open video file " << argv[i+1] << endl;
            exit(EXIT_FAILURE);
        }
    }

    /* Check the consistency of left and right video format */
    for(uint8_t i = CV_CAP_PROP_POS_MSEC; i < CV_CAP_PROP_BRIGHTNESS; i++)
        if(video_capturer[0].get(i) != video_capturer[1].get(i))
        {
            cerr << __PRETTY_FUNCTION__ << ": Left and right video files have different format in " << i << endl;
            exit(EXIT_FAILURE);
        }

    uint32_t frame_w = video_capturer[0].get(CV_CAP_PROP_FRAME_WIDTH);
    uint32_t frame_h = video_capturer[0].get(CV_CAP_PROP_FRAME_HEIGHT);

    assert(cam_w == frame_w && cam_h == frame_h);

    /////////////////////////////////////////////////
    //  Pangolin UI Setting
    /////////////////////////////////////////////////
    /* OpenGL, GLEW, Freeglut initialization */
    int const UI_WIDTH = 180;
    CreateGlutWindowAndBind(window_title, UI_WIDTH+2*frame_w, 2*frame_h, GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glClearColor(1.0, 1.0, 1.0, 1.0);

    GLDrawer::near = 0.1;
    GLDrawer::far  = 10000;

    GLenum err = glewInit();
    if(GLEW_OK != err)
    {
        std::cerr << "GLEW Error: " << glewGetErrorString(err) << std::endl;
        exit(0);
    }

    /* Views */
    View& lview = Display("Left Frame View")
            .SetAspect((float)frame_w/(float)frame_h);
//            .SetHandler(new Handler2D);

    View& rview = Display("Right Frame View")
            .SetAspect((float)frame_w/(float)frame_h);
//            .SetHandler(new Handler2D);

    View& mview = Display("Disparity View")
            .SetAspect((float)frame_w/(float)frame_h);
//            .SetHandler(new Handler2D);

    /* Pangolin projection and 3D Handler */
    OpenGlRenderState world_cam(
                ProjectionMatrix(frame_w, frame_h, frame_h, frame_h, frame_w/2.0, frame_h/2.0, GLDrawer::near, GLDrawer::far),
                ModelViewLookAt(5.0, -5.0, -5.0, 0.0, 0.0, 0.0, AxisNegY));

    View& wview = Display("World View")
            .SetAspect((float)frame_w/(float)frame_h)
            .SetHandler(new Handler3D(world_cam));

    Display("multi")
            .SetBounds(0.0, 1.0, Attach::Pix(UI_WIDTH), 1.0)
            .SetLayout(LayoutEqual)
            .AddDisplay(lview)
            .AddDisplay(rview)
            .AddDisplay(mview)
            .AddDisplay(wview);

    /* UI */
    View& d_panel = CreatePanel("ui")
        .SetBounds(0.0, 1.0, 0.0, Attach::Pix(UI_WIDTH));

    Var<double> gpu_memory("ui.GPU Memory", GetMemoryUsageMB(), 0, 1024);
    cout << "Aviable GPU memory " << GetMemoryUsageMB() << endl;

    Var<int> frame_no("ui.Frame", 0);
    Var<bool> play_button("ui.Play Video", false, false);
    Var<bool> stop_button("ui.Stop Video", false, false);
    bool playing_flag = false;

    /* Cost volume parameters */
    Var<int> min_disp("ui.Min Disparity", 1, 0, 50);
    Var<int> max_disp("ui.Max Disparity", 31, 10, 150);
    Var<int> min_bg_thr("ui.Min Bg Threshold", 0, 0, 255);
    Var<int> max_bg_thr("ui.Max Bg Threshold", 255, 0, 255);
    Var<int> win_r("ui.Window Radius", 4, 0, 20);

    Var<bool> ref_img("ui.Reconst Left Camera", true, true);

    Var<bool> ad_button("ui.Absolute Distance (AD)", false, false);
    Var<bool> zncc_button("ui.ZNCC", false, false);
    DataTermMethod data_term = ZNCC;

    /* Huber-Huber parameterrs */
    Var<int> num_itr("ui.Number of Iterations", 200, 0, 500);
    Var<double> alpha("ui.Alpha", 10, 1, 20.0);
    Var<double> beta("ui.Beta", 1.0, 0.1, 1.0);
    Var<double> epsilon("ui.Epsilon", 1e-3, 1e-6, 1.0, true);
    Var<double> lambda("ui.Lambda", 5, 0.01, 10.0);
    Var<double> aux_theta("ui.Aux Theta", 0.1, 0, 1.0);

    /* World drawer parameters */
    Var<double> v_w_s("ui.World Scaler", 1, 1e-2, 1e2, true);
    Var<double> v_c_s("ui.Camera Scaler", 1e2, 1, 1e5, true);

    /* OpenGL textures */
    Mat video_frames[2];
    CUDAImage<OpenGL2D,uchar4> cuda_imgs[2];
    CUDAImage<OpenGL2D,uchar4> cuda_disp(frame_w, frame_h);
    for(uint8_t i = 0; i < 2; i++)
    {
        video_capturer[i] >> video_frames[i];
        cvtColor(video_frames[i], video_frames[i], CV_BGR2RGBA, 4);

        remap(video_frames[i], video_frames[i], cv_RMAP[i][0], cv_RMAP[i][1], CV_INTER_LINEAR);

        cuda_imgs[i].Reinitialise(frame_w, frame_h);
        cuda_imgs[i].Upload(video_frames[i].data);
    }    

    double gpu_mem_begin = GetMemoryUsageMB();
    cout << "GPU memory begins: " << gpu_mem_begin << endl;
    StereoReconstructor stereo_reconst(CostVolume::Params(min_disp, max_disp, win_r, ref_img ? LeftRefImage:RightRefImage, ZNCC, min_bg_thr, max_bg_thr, frame_w, frame_h),
                                       HuberROF::Params(num_itr, alpha, beta, epsilon, lambda, aux_theta));

    stereo_reconst.Reconstruct(cuda_imgs, ref_img ? LeftRefImage : RightRefImage);

    double gpu_mem_end = GetMemoryUsageMB();
    cout << "GPU memory ends: " << gpu_mem_end << endl;
    cout << "GPU memory usage: " << gpu_mem_end - gpu_mem_begin << endl;

    /* Point cloud data */
    CUDAData<OpenGLBufferType,float3> pts;
    CUDAData<OpenGLBufferType,uchar4> pts_colour;

    stereo_reconst.GetDisparityImage(cuda_disp);
    stereo_reconst.GetPointCloud(cuda_imgs[!ref_img], pts, pts_colour, B, K[0].data(), ref_img ? SE3f().matrix().data() : T_ext.inverse().matrix().data());

    sdkCreateTimer(&timer);
    while(!ShouldQuit())
    {
        /* The very beginning of a system iteration */
        sdkStartTimer(&timer);

        /* Drawer variables */
        GLDrawer::w_s = v_w_s;
        GLDrawer::c_s = v_c_s;

        if(HasResized())
            DisplayBase().ActivateScissorAndClear();

        // Render our UI panel when we receive input
        if(HadInput())
            d_panel.Render();

        if(Pushed(play_button))
            playing_flag = true;

        if(Pushed(stop_button))
            playing_flag = false;

        if(min_disp > max_disp)
            min_disp = max_disp;

        if(max_disp < min_disp)
            max_disp = min_disp;

        if(Pushed(ad_button))
            data_term = AD;

        if(Pushed(zncc_button))
            data_term = ZNCC;

//        if(((Handler2D*)lview.handler)->IsPressed())
//        {
//            float x = ((Handler2D*)lview.handler)->GetLastPos()[0] - lview.vp.l;
//            float y = ((Handler2D*)lview.handler)->GetLastPos()[1] - lview.vp.b;
//            cout << "left: " << "(" << x << "," << y << ")" << endl;
//            boost::this_thread::sleep( boost::posix_time::milliseconds(200));
//        }

//        if(((Handler2D*)rview.handler)->IsPressed())
//        {
//            float x = ((Handler2D*)rview.handler)->GetLastPos()[0] - rview.vp.l;
//            float y = ((Handler2D*)rview.handler)->GetLastPos()[1] - rview.vp.b;
//            cout << "right: " << "(" << x << "," << y << ")" << endl;
//            boost::this_thread::sleep( boost::posix_time::milliseconds(200));
//        }

//        if(((Handler2D*)mview.handler)->IsPressed())
//        {
//            float x = ((Handler2D*)mview.handler)->GetLastPos()[0] - mview.vp.l;
//            float y = ((Handler2D*)mview.handler)->GetLastPos()[1] - mview.vp.b;
//            cout << "disparity: " << "(" << x << "," << y << ")" << endl;
//            boost::this_thread::sleep( boost::posix_time::milliseconds(200));
//        }

        /* Main system */
        if(playing_flag)
        {
            for(uint8_t i = 0; i < 2; i++)
            {
                video_capturer[i] >> video_frames[i];
                cvtColor(video_frames[i], video_frames[i], CV_BGR2RGBA, 4);




                remap(video_frames[i], video_frames[i], cv_RMAP[i][0], cv_RMAP[i][1], CV_INTER_LINEAR);
                cuda_imgs[i].Upload(video_frames[i].data);
            }          

            frame_no = frame_no + 1;

            stereo_reconst.Reconstruct(cuda_imgs, ref_img ? LeftRefImage : RightRefImage);

        }

        if(stereo_reconst.Reconstruct(cuda_imgs,
                                      CostVolume::Params(min_disp, max_disp, win_r, ref_img ? LeftRefImage : RightRefImage, data_term, min_bg_thr, max_bg_thr, frame_w, frame_h),
                                      HuberROF::Params(num_itr, alpha, beta, epsilon, lambda, aux_theta)) || playing_flag)
        {

            stereo_reconst.GetDisparityImage(cuda_disp);
            stereo_reconst.GetPointCloud(cuda_imgs[!ref_img], pts, pts_colour, B, K[0].data(), ref_img ? SE3f().matrix().data() : T_ext.inverse().matrix().data());

        }


        /* Left video frame view */
        lview.ActivateScissorAndClear();
        cuda_imgs[0].RenderToViewportFlipY();

        /* Right video frame view */
        rview.ActivateScissorAndClear();
        cuda_imgs[1].RenderToViewportFlipY();

        /* Disparity view */
        mview.ActivateScissorAndClear();
        cuda_disp.RenderToViewportFlipY();

        /* World view */
        wview.ActivateScissorAndClear(world_cam);

        /* GLDrawer in world view */
        GLDrawer::DrawWorldCoord();

        for(int i = 0; i < 2; i++)
            GLDrawer::DrawCamera(K[i], i ? T_ext.inverse() : SE3f(), cuda_imgs[i], cam_w, cam_h);

        GLDrawer::DrawDispFrustum(K[ref_img ? LeftRefImage:RightRefImage],
                ref_img ? SE3f() : T_ext.inverse(),
                B, min_disp, max_disp, cam_w, cam_h);

        GLDrawer::RenderPointCloud(pts, pts_colour);

        // Finish timing before swap buffers to avoid refresh sync
        sdkStopTimer(&timer);
        CalculateFPS();

        // Swap frames and Process Events
        FinishGlutFrame();

        gpu_memory = GetMemoryUsageMB();
    }

    return 0;
}

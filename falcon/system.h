#ifndef FALCON_SLAMSYSTEM_H
#define FALCON_SLAMSYSTEM_H

#include <vector>
#include <Eigen/Core>

#include "falcon.h"

#include "cuda/cuda_const.h"

#include "keyframe.h"
#include "pt_cloud.h"

#include "reconst.h"
#include "tracker.h"

namespace falcon {

struct RectifiedStereoCamera
{
    RectifiedStereoCamera() {}

    RectifiedStereoCamera(Matrix3f const& K, SE3f const& T_ext)
    {
        this->K = K;
        this->T_ext = T_ext;

        B = abs(T_ext.translation()(0, 0));
    }

    /* Assume stereo is rectified and intrinsic matrix the same */
    Matrix3f K;
    SE3f T_ext;
    float B;
};

struct SystemParams
{

    SystemParams()
        : frame_w(0), frame_h(0) {}

    SystemParams(size_t const frame_w, size_t const frame_h, RectifiedStereoCamera const st_cam)
        : frame_w(frame_w), frame_h(frame_h), st_cam(st_cam)
    {}

    size_t frame_w, frame_h;    
    RectifiedStereoCamera st_cam;
};

class SLAMSystem
{

public:
    SLAMSystem(CUDAImage<OpenGL2D, uchar4> st_frames[PYR_LEVEL_NUM][2],
               RectifiedStereoCamera const st_cam,
               CostVolume::Params const& cv_params,
               HuberROF::Params const& hbl1_tgv1_params);
    ~SLAMSystem();

    void Init();

    void ResetKeyframes();

    vector<StereoKeyframe*> GetKeyframes() { return keyframes; }    

    bool AddKeyframe();

    bool Tracking(CUDAImage<OpenGL2D, uchar4> st_frames[PYR_LEVEL_NUM][2]);

    StereoReconstructor* GetReconstor() { return &reconstor; }
    Tracker* GetTracker() { return &tracker; }

private:
    SLAMSystem();
    SLAMSystem(SLAMSystem const&);
    SLAMSystem& operator=(SLAMSystem const&);

    StereoReconstructor reconstor;
    Tracker tracker;

    /* Pointers to OpenGL texture sources */
    CUDAImage<OpenGL2D,uchar4>* st_cur_frames[PYR_LEVEL_NUM][2];

    /* Will do optimisation on keyframs in the near future... */
    vector<StereoKeyframe*> keyframes;

    SystemParams params;

};

} // namespace falcon {

#endif // FALCON_SLAMSYSTEM_H

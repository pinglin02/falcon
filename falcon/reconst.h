#ifndef FALCON_RECONST_H
#define FALCON_RECONST_H

#include <iostream>

#include "cuda/cuda_reconst.h"

namespace falcon {

using namespace std;

class StereoReconstructor
{

public:
    StereoReconstructor(CostVolume::Params const& cv_params,
                        HuberROF::Params const& hbl1_tgv1_params);
    ~StereoReconstructor();

    /* Static reconstruction for initialisation */
    void Reconstruct(CUDAImage<OpenGL2D,uchar4> st_frames[2],
                     RefImage const ref_img);

    /* Dynamic reconstruction for adjusting the reconstruction */
    bool Reconstruct(CUDAImage<OpenGL2D,uchar4> st_frames[2],
                     CostVolume::Params const& disp_cv_params,
                     HuberROF::Params const& hbl1_tgb1_params);

    void GetRealDisparity(CUDAImage<Pitched2D,float>& real_disp);

    void GetDisparityImage(CUDAImage<OpenGL2D,uchar4>& uchar4_disp);
    void GetPointCloud(CUDAImage<OpenGL2D,uchar4>& frame,
                       CUDAData<OpenGLBufferType,float3>& pts,
                       CUDAData<OpenGLBufferType,uchar4>& pts_colour,
                       float const baseline,
                       float const K[9],
                       float const T[16]);

    CostVolume::Params GetCostVolumeParams() const { return cv_data.GetParams(); }

    size_t GetValidPtsNum() const { return cv_data.GetParams().roi_w*cv_data.GetParams().roi_h; }

private:    
    StereoReconstructor();
    StereoReconstructor(StereoReconstructor const&);
    StereoReconstructor& operator=(StereoReconstructor const&);

    void CostVolumeFunctions();
    void HuberL1TGV1Functions();    

    CostVolume::Data cv_data;
    HuberROF::Data opt_data;

};

/* CUDA API */
extern "C" void BindTexturesCUDA(CUDAImage<OpenGL2D,uchar4> st_frames[2],
                                 RefImage const ref_img);

extern "C" void UnbindTexturesCUDA();

extern "C" void CopyNormDisp2RealDispCUDA(CUDAImage<Pitched2D,float>& real_disp,
                                          CostVolume::Data const& cv_data,
                                          HuberROF::Data const& opt_data);

extern "C" void CopyNormDisp2OpenGLTextureCUDA(CUDAImage<OpenGL2D,uchar4>& gl_img,
                                               CostVolume::Data const& cv_data,
                                               HuberROF::Data const& opt_data);

extern "C" void DispMap2PtsCloudCUDA(CUDAImage<OpenGL2D,uchar4>& frame,
                                     CUDAData<OpenGLBufferType,float3>& pts,
                                     CUDAData<OpenGLBufferType,uchar4>& pts_colour,
                                     CostVolume::Data const& cv_data,
                                     HuberROF::Data const& opt_data,
                                     float const baseline,
                                     float const K[9],
                                     float const T[16]);


} // namespace falcon {

#endif // FALCON_RECONST_H

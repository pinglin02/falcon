#ifndef FALCON_CUDA_DATA_H
#define FALCON_CUDA_DATA_H

#include <assert.h>
#include <string>
#include <typeinfo>

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

#include "cuda_const.h"

namespace falcon {

using namespace std;

struct CUDADataType {};

/* Base class template */
template<typename T, typename D>
struct CUDAData
{
};

template<typename D>
struct CUDAData<CUDADataType,D>
{

    typedef CUDADataType data_type;
    typedef D data_format;

    uint32_t num_elements;
    D* ptr;

    CUDAData() : num_elements(0), ptr(NULL)
    {
    }

    CUDAData(size_t const num_elements)
        :num_elements(num_elements)
    {
        checkCudaErrors(cudaMalloc(&ptr, num_elements*sizeof(D)));
    }

    CUDAData(CUDAData const& rhs)
    {

        ptr = rhs.ptr;
        num_elements = rhs.num_elements;
    }

    CUDAData& operator=(CUDAData rhs)
    {

        swap(ptr, rhs.ptr);
        swap(num_elements, rhs.num_elements);

        return *this;
    }

    ~CUDAData()
    {
        checkCudaErrors(cudaFree(ptr));
    }

    size_t GetNumElemenets() const { return num_elements; }
    size_t GetBytes() const { return num_elements*sizeof(D); }

    string GetImageType() const { return typeid(data_type).name(); }
    string GetDataFormat() const { return typeid(data_format).name(); }

};

/* For OPENGL_BUFFER_DATA */
enum OpenGLBufferType { NULLBuffer = 0,
                        OpenGLArrayBuffer = GL_ARRAY_BUFFER,
                        OpenGLElementArrayBuffer = GL_ELEMENT_ARRAY_BUFFER,
                        OpenGLPixelPackBuffer = GL_PIXEL_PACK_BUFFER,
                        OpenGLPixelUnpackBuffer = GL_PIXEL_UNPACK_BUFFER };

template<typename D>
struct CUDAData<OpenGLBufferType,D>
{

    typedef CUDADataType data_type;
    typedef D data_format;

    GLuint bid;
    OpenGLBufferType buf_type;
    cudaGraphicsResource* res;
    D* ptr;

    size_t num_elements;
    size_t size_bytes;
    bool is_mapped;    

    /* NULL data */
    CUDAData() : num_elements(0), size_bytes(0), bid(0), buf_type(NULLBuffer), ptr(NULL), res(NULL) {}

    /* Bind with OpenGL buffer immediately */
    CUDAData(OpenGLBufferType buf_type, size_t const num_elements, size_t const size_bytes, size_t cudause = cudaGraphicsMapFlagsNone, GLenum gluse = GL_DYNAMIC_DRAW)
        :num_elements(num_elements), size_bytes(size_bytes), buf_type(buf_type), is_mapped(false)
    {        

        glGenBuffers(1, &bid);
        BindOpenGLBuffer();
        glBufferData(buf_type, num_elements*size_bytes, 0, gluse);
        UnbindOpenGLBuffer();

        cudaGraphicsGLRegisterBuffer(&res, bid, cudause);

    }

    ~CUDAData()
    {        
        glDeleteBuffers(1, &bid);
        if(res)
            checkCudaErrors(cudaGraphicsUnregisterResource(res));
    }

    /* Could be bound from outside */
    void BindOpenGLBuffer() const
    {
        glBindBuffer(buf_type, bid);
    }

    void UnbindOpenGLBuffer() const
    {
        glBindBuffer(buf_type, 0);
    }

    void Reinitialise(OpenGLBufferType buf_type, size_t const num_elements, size_t const size_bytes, size_t cudause = cudaGraphicsMapFlagsNone, GLenum gluse = GL_DYNAMIC_DRAW)
    {

        if(bid)
        {
            checkCudaErrors(cudaGraphicsUnregisterResource(res));
            glDeleteBuffers(1, &bid);
        }

        this->num_elements = num_elements;
        this->size_bytes = size_bytes;
        this->buf_type = buf_type;
        this->is_mapped = false;

        glGenBuffers(1, &bid);
        glBindBuffer(buf_type, bid);
        glBufferData(buf_type, num_elements*size_bytes, 0, gluse);
        glBindBuffer(buf_type, 0);

        cudaGraphicsGLRegisterBuffer(&res, bid, cudause);
    }

    void UploadOpenGLBuffer(void const *const data, size_t const size_bytes, size_t const offset)
    {
        BindOpenGLBuffer();
        glBufferSubData(buf_type, offset, size_bytes, data);
        UnbindOpenGLBuffer();

        this->size_bytes = size_bytes;
    }

    void MapCUDARes()
    {        
        cudaGraphicsMapResources(1, &res, 0);
        cudaGraphicsResourceGetMappedPointer((void**)&ptr, &size_bytes, res);
        is_mapped = true;
    }

    void UnmapCUDARes()
    {
        cudaGraphicsUnmapResources(1, &res, 0);
        is_mapped = false;
    }

    D* GetPtr()
    {
        assert(is_mapped);
        return ptr;
    }

    size_t GetNumElemenets() const { return num_elements; }
    size_t GetBytes() const {assert(size_bytes == num_elements*sizeof(D)); return size_bytes; }

private:
    CUDAData(CUDAData const& rhs);
    CUDAData& operator=(CUDAData rhs);

};

} // namespace falcon {

#endif // FALCON_CUDA_DATA_H

#ifndef FALCON_CUDA_RECONST_H
#define FALCON_CUDA_RECONST_H

#include "cuda_const.h"
#include "cuda_data.h"
#include "cuda_img.h"

namespace falcon {

/* Cost-volume for left or right camera */
enum RefImage {LeftRefImage = 0, RightRefImage};

/* Data term method */
enum DataTermMethod {AD, ZNCC};

////////////////////////////////////////////////////////////////////////////
//  Stereo reconstruction using disparity (rectification required)
////////////////////////////////////////////////////////////////////////////
namespace CostVolume {

struct Params
{

    /* Must have an empty constructor for CUDA device constant memory */
    Params(){}

    Params(uint32_t const min_disp,
           uint32_t const max_disp,
           uint32_t const win_r,
           RefImage const ref_img,
           DataTermMethod const method,
           uint8_t min_bg_thr,
           uint8_t max_bg_thr,
           uint32_t const frame_w,
           uint32_t const frame_h);

    bool operator!=(Params const& rhs) const;

    short min_disp;
    short max_disp;
    short num_disp;

    short win_r;

    short frame_w, frame_h;

    ROI roi[2];
    short roi_w, roi_h;

    RefImage ref_img;
    DataTermMethod method;

    uint8_t min_bg_thr;
    uint8_t max_bg_thr;

};

struct Data
{


    Data(Params const params);
    ~Data();

    void UpdateParams(Params const params);

    Params GetParams() const { return params; }

    void SetRefImage(RefImage const ref_img);

    /* CUDA 3D array memory */
    cudaArray* cost_volume_array;

    /* CUDA 2D pitched memory */
    CUDAImage<Pitched2D,float> win_disp, min_disp_cost, max_disp_cost;

private:
    Params params;

    Data();
    Data(Data const&);
    Data const& operator=(Data const&);
};

void BuildCostVolume(cudaArray *const cost_volume_array,
                     CostVolume::Params const cv_params);

void WinnerTakeAll(CUDAImage<Pitched2D,float> const& win_disp,
                   CUDAImage<Pitched2D,float> const& min_disp_cost,
                   CUDAImage<Pitched2D,float> const& max_disp_cost,
                   CostVolume::Params const cv_params);

} // namespace CostVolume {

namespace HuberROF {

struct Primal { float u; };
struct PrimalStep { float tau; };

struct Auxiliary { float a; };

struct Dual { float p[2]; };
struct DualStep { float sigma[2]; };

struct DiffuseTensor { float D[4]; };

struct Params
{

    /* Must have an empty constructor for CUDA device constant memory */
    Params(){}

    Params(int num_itr,
           float const alpha,
           float const beta,
           float const epsilon,
           float const lambda,
           float const theta);

    bool operator!=(Params const& rhs) const;

    short num_itr;

    float alpha;
    float beta;
    float epsilon;
    float lambda;
    float theta;

};

struct Data
{

    Data(Params const params, size_t const w, size_t const h);
    ~Data();

    void UpdateParams(Params const params);
    void Resize(size_t const w, size_t const h);

    Params GetParams() const { return params; }

    CUDAImage<Pitched2D,Primal> primal, old_primal, head_primal;
    CUDAImage<Pitched2D,Dual> dual;
    CUDAImage<Pitched2D,Auxiliary> aux;
    CUDAImage<Pitched2D,DiffuseTensor> diffuse_tensor;

    CUDAImage<Pitched2D,PrimalStep> primal_step;
    CUDAImage<Pitched2D,DualStep> dual_step;

    CUDAImage<Pitched2D,float> error_img;

    size_t w, h;

private:
    Params params;

    Data();
    Data(Data const&);
    Data const& operator=(Data const&);

};

void CalculateDiffuseTensor(CUDAImage<Pitched2D,DiffuseTensor> const& diffuse_tensor,
                            CostVolume::Params const cv_params,
                            HuberROF::Params const opt_params);

void Preconditioning(CUDAImage<Pitched2D,DiffuseTensor> const& diffuse_tensor,
                     CUDAImage<Pitched2D,PrimalStep> const& primal_step,
                     CUDAImage<Pitched2D,DualStep> const& dual_step,
                     CostVolume::Params const cv_params,
                     HuberROF::Params const opt_params);

void ItrHuberROF(CUDAImage<Pitched2D,float> const& win_disp,
                    CUDAImage<Pitched2D,float> const& min_disp_cost,
                    CUDAImage<Pitched2D,float> const& max_disp_cost,
                    CUDAImage<Pitched2D,Primal>& head_primal,
                    CUDAImage<Pitched2D,Primal>& primal,
                    CUDAImage<Pitched2D,Primal>& old_primal,
                    CUDAImage<Pitched2D,Dual>& dual,
                    CUDAImage<Pitched2D,Auxiliary>& aux,
                    CUDAImage<Pitched2D,DiffuseTensor> const& diffuse_tensor,
                    CUDAImage<Pitched2D,PrimalStep> const& primal_step,
                    CUDAImage<Pitched2D,DualStep> const& dual_step,
                    CostVolume::Params const cv_params,
                    HuberROF::Params const opt_params);

} // namespace HuberL1TGV1 {

} // namespace falcon {

#endif // FALCON_CUDA_RECONST_H

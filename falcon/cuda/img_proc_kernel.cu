#include "cuda_const.h"
#include "../img_proc.h"

namespace falcon{

namespace CUDAImgProc{


__constant__ uchar4 const_text_colour;

__global__ void DrawTextPtsOn3DPtsOccluding_Kernel(float3 *const pts,
                                                   float3 *const text_pts,
                                                   uchar4 *const text_pts_colour,
                                                   cudaTextureObject_t const mask_tex,
                                                   cudaTextureObject_t const weight_img_tex,
                                                   ROI const roi,
                                                   short const w,
                                                   short const h,
                                                   bool const no_weight)
{

    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x < w && y < h)
    {
        uchar1 const mask = tex2D<uchar1>(mask_tex, x, y);
        uchar4 const weight_img = tex2D<uchar4>(weight_img_tex, x+roi.x_min, y+roi.y_min);

        uchar4 const weight_img_xup = tex2D<uchar4>(weight_img_tex, x+roi.x_min+1, y+roi.y_min);
        uchar4 const weight_img_xdown = tex2D<uchar4>(weight_img_tex, x+roi.x_min-1, y+roi.y_min);

        uchar4 const weight_img_yup = tex2D<uchar4>(weight_img_tex, x+roi.x_min, y+roi.y_min+1);
        uchar4 const weight_img_ydown = tex2D<uchar4>(weight_img_tex, x+roi.x_min, y+roi.y_min-1);

        if(mask.x == 255 &&
                ((weight_img.x > 210 && weight_img.y > 210 && weight_img.z > 210) ||
                 (weight_img_xup.x > 210 && weight_img_xup.y > 210 && weight_img_xup.z > 210) ||
                 (weight_img_xdown.x > 210 && weight_img_xdown.y > 210 && weight_img_xdown.z > 210) ||
                 (weight_img_yup.x > 210 && weight_img_yup.y > 210 && weight_img_yup.z > 210) ||
                 (weight_img_ydown.x > 210 && weight_img_ydown.y > 210 && weight_img_ydown.z > 210) ||
                 no_weight))
        {
            text_pts[y*w+x] = pts[y*w+x];

            if(text_pts_colour[y*w+x].x == 0 && text_pts_colour[y*w+x].y == 0 && text_pts_colour[y*w+x].z == 0)
                text_pts_colour[y*w+x] = const_text_colour;
        }
        else
        {
            text_pts[y*w+x] = (float3){0, 0, 1e32};
            text_pts_colour[y*w+x] = (uchar4){0, 0, 0, 0};
        }
    }

}

extern "C" void DrawTextPtsOn3DPtsOccluding(CUDAData<OpenGLBufferType,float3>& pts,
                                            CUDAData<OpenGLBufferType,float3>& text_pts,
                                            CUDAData<OpenGLBufferType,uchar4>& text_pts_colour,
                                            CUDAImage<Pitched2D,uchar1>& mask,
                                            CUDAImage<OpenGL2D,uchar4>& weight_img,
                                            uchar4 const text_colour,
                                            ROI const roi,
                                            short const w,
                                            short const h,
                                            short const frame_w,
                                            short const frame_h)
{   

    if(pts.GetNumElemenets() == 0)  // nothing to draw
        return;

    if(text_pts.GetNumElemenets() != pts.GetNumElemenets())
    {
        text_pts.Reinitialise(OpenGLArrayBuffer, pts.GetNumElemenets(), sizeof(float3));
        text_pts_colour.Reinitialise(OpenGLArrayBuffer, pts.GetNumElemenets(), sizeof(uchar4));
    }

    bool no_weight = true;
    if(weight_img.res)
    {
        no_weight = false;
        weight_img.CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeElementType);
    }

    checkCudaErrors(cudaMemcpyToSymbol(const_text_colour, &text_colour, sizeof(uchar4)));

    mask.CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeElementType);


    pts.MapCUDARes();
    text_pts.MapCUDARes();
    text_pts_colour.MapCUDARes();

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((w+dimBlock.x-1)/dimBlock.x, (h+dimBlock.y-1)/dimBlock.y);

    DrawTextPtsOn3DPtsOccluding_Kernel<<<dimGrid, dimBlock>>>(pts.GetPtr(),
                                                              text_pts.GetPtr(),
                                                              text_pts_colour.GetPtr(),
                                                              mask.cudaTexes,
                                                              weight_img.cudaTexes,
                                                              roi,
                                                              w, h,
                                                              no_weight);

    pts.UnmapCUDARes();
    text_pts.UnmapCUDARes();
    text_pts_colour.UnmapCUDARes();

    mask.DestroyCUDATextureObject();

    if(weight_img.res)
        weight_img.DestroyCUDATextureObject();

}


__global__ void DrawTextPtsOn3DPts_Kernel(float3 *const pts,
                                          float3 *const text_pts,
                                          uchar4 *const text_pts_colour,
                                          cudaTextureObject_t const mask_tex,
                                          short const w,
                                          short const h)
{

    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x < w && y < h)
    {
        uchar1 const mask = tex2D<uchar1>(mask_tex, x, y);

        if(mask.x == 255)
        {
            text_pts[y*w+x] = pts[y*w+x];

            if(text_pts_colour[y*w+x].x == 0 && text_pts_colour[y*w+x].y == 0 && text_pts_colour[y*w+x].z == 0)
                text_pts_colour[y*w+x] = const_text_colour;
        }
        else
        {
            text_pts[y*w+x] = (float3){0, 0, 1e32};
            text_pts_colour[y*w+x] = (uchar4){0, 0, 0, 0};
        }
    }

}

extern "C" void DrawTextPtsOn3DPts(CUDAData<OpenGLBufferType,float3>& pts,
                                   CUDAData<OpenGLBufferType,float3>& text_pts,
                                   CUDAData<OpenGLBufferType,uchar4>& text_pts_colour,
                                   CUDAImage<Pitched2D,uchar1>& mask,
                                   uchar4 const text_colour,
                                   short const w,
                                   short const h)
{

    if(pts.GetNumElemenets() == 0)  // nothing to draw
        return;

    if(text_pts.GetNumElemenets() != pts.GetNumElemenets())
    {
        text_pts.Reinitialise(OpenGLArrayBuffer, pts.GetNumElemenets(), sizeof(float3));
        text_pts_colour.Reinitialise(OpenGLArrayBuffer, pts.GetNumElemenets(), sizeof(uchar4));
    }

    checkCudaErrors(cudaMemcpyToSymbol(const_text_colour, &text_colour, sizeof(uchar4)));

    mask.CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeElementType);


    pts.MapCUDARes();
    text_pts.MapCUDARes();
    text_pts_colour.MapCUDARes();

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((w+dimBlock.x-1)/dimBlock.x, (h+dimBlock.y-1)/dimBlock.y);

    DrawTextPtsOn3DPts_Kernel<<<dimGrid, dimBlock>>>(pts.GetPtr(),
                                                     text_pts.GetPtr(),
                                                     text_pts_colour.GetPtr(),
                                                     mask.cudaTexes,
                                                     w, h);

    pts.UnmapCUDARes();
    text_pts.UnmapCUDARes();
    text_pts_colour.UnmapCUDARes();

    mask.DestroyCUDATextureObject();

}






} // namespace CUDAImgProc{

} // namespace falcon{

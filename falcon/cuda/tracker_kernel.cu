#include "cuda_const.h"
#include "cuda_math.h"
#include "cuda_img.h"

namespace falcon {

/* 3x3 Matrix */
__constant__ float const_K[9];

/* 4x4 Matrix */
__constant__ float const_T_rl[16];
__constant__ float const_T_lr[16];
__constant__ float const_T_cl[16];
__constant__ float const_T_cr[16];
__constant__ float const_T_head[16];

/* 16*6 Matrix */
__constant__ float const_J_SE3[96];

/* 6x6 Matrix */
__constant__ float const_J_ladj[36];
__constant__ float const_J_radj[36];

surface<void, cudaSurfaceType2D> img_surface;

extern "C" void SetConstTheadCUDA(float const T_head[16])
{
    checkCudaErrors(cudaMemcpyToSymbol(const_T_head, T_head, 16*sizeof(float)));
}

extern "C" void SetConstMatrixCUDA(float const K[9],
                                   float const T_rl[16],
                                   float const T_lr[16],
                                   float const T_cl[16],
                                   float const T_cr[16],
                                   float const J_ladj[36],
                                   float const J_radj[36],
                                   float const J_SE3[96])
{

    checkCudaErrors(cudaMemcpyToSymbol(const_K, K, 9*sizeof(float)));

    checkCudaErrors(cudaMemcpyToSymbol(const_T_rl, T_rl, 16*sizeof(float)));
    checkCudaErrors(cudaMemcpyToSymbol(const_T_lr, T_lr, 16*sizeof(float)));
    checkCudaErrors(cudaMemcpyToSymbol(const_T_cl, T_cl, 16*sizeof(float)));
    checkCudaErrors(cudaMemcpyToSymbol(const_T_cr, T_cr, 16*sizeof(float)));

    checkCudaErrors(cudaMemcpyToSymbol(const_J_ladj, J_ladj, 36*sizeof(float)));
    checkCudaErrors(cudaMemcpyToSymbol(const_J_radj, J_radj, 36*sizeof(float)));

    checkCudaErrors(cudaMemcpyToSymbol(const_J_SE3, J_SE3, 96*sizeof(float)));

}

/**
 *  Jacobian matrix for J_Iref.
 *
 *  Remark: Note that reference image is with ROI size
 *
 */
__global__ void Cal_J_Iref_Kernel(cudaPitchedPtr const J_Iref,
                                  cudaTextureObject_t const ref_img_tex,
                                  cudaTextureObject_t const ref_disp_tex,
                                  bool const is_left,
                                  short const level,
                                  short const frame_w,
                                  short const frame_h)
{

    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x < frame_w && y < frame_h)
    {

        float point[3] = {x, y, 1.0f};

        float dI_Iref[2];
        dI_Iref[0] = (FloatRGBA2FloatGray(tex2D<float4>(ref_img_tex, x+1, y)) -
                      FloatRGBA2FloatGray(tex2D<float4>(ref_img_tex, x-1, y))) / 2.0f;
        dI_Iref[1] = (FloatRGBA2FloatGray(tex2D<float4>(ref_img_tex, x, y+1)) -
                      FloatRGBA2FloatGray(tex2D<float4>(ref_img_tex, x, y-1))) / 2.0f;

        size_t pyr0_x = x*pow(2.0f, float(level));
        size_t pyr0_y = y*pow(2.0f, float(level));

        float line[3] = {-1.0f,
                         -1.0f,
                         is_left ? point[0] - tex2D<float>(ref_disp_tex, pyr0_x, pyr0_y)/pow(2.0f, float(level)) + point[1] :
                                   point[0] + tex2D<float>(ref_disp_tex, pyr0_x, pyr0_y)/pow(2.0f, float(level)) + point[1]};

        float const fu = const_K[0] / pow(2.0f, float(level));
        float const fv = const_K[4] / pow(2.0f, float(level));
        float const u0 = const_K[6] / pow(2.0f, float(level));
        float const v0 = const_K[7] / pow(2.0f, float(level));

        float K[9], inv_K[9];
        SetZero<3>(K);
        K[0] = fu;
        K[4] = fv;
        K[6] = u0;
        K[7] = v0;
        K[8] = 1.0f;

        SetZero<3>(inv_K);
        inv_K[0] = 1.0f/fu;
        inv_K[4] = 1.0f/fv;
        inv_K[6] = -u0/fu;
        inv_K[7] = -v0/fv;
        inv_K[8] = 1.0f;

        float A[12], A_R[9], A_t[3];
        LieSE3from4x4(A, is_left ? const_T_rl : const_T_lr);
        LieGetRotation(A_R, A);
        LieGetTranslation(A_t, A);

        float B[12], B_R[9], B_t[3];
        LieSetIdentity(B);
        LieGetRotation(B_R, B);
        LieGetTranslation(B_t, B);

        float a_r[9], a_t[3], b_r[9], b_t[3], tmp1[9], tmp2[9];
        MatMul<3,3,3>(tmp1, A_R, inv_K);
        MatMul<3,3,3>(a_r, K, tmp1);
        MatMul<3,3,1>(a_t, K, A_t);

        MatMul<3,3,3>(tmp1, B_R, inv_K);
        MatMul<3,3,3>(b_r, K, tmp1);
        MatMul<3,3,1>(b_t, K, B_t);

        float Tsr0[9], Tsr1[9], Tsr2[9];
        MatMul<3,1,3>(tmp1, a_r, b_t);
        MatMul<3,1,3>(tmp2, a_t, b_r);
        MatSub<3,3>(Tsr0, tmp1, tmp2);

        MatMul<3,1,3>(tmp1, &a_r[3], b_t);
        MatMul<3,1,3>(tmp2, a_t, &b_r[3]);
        MatSub<3,3>(Tsr1, tmp1, tmp2);

        MatMul<3,1,3>(tmp1, &a_r[6], b_t);
        MatMul<3,1,3>(tmp2, a_t, &b_r[6]);
        MatSub<3,3>(Tsr2, tmp1, tmp2);

        /* Remark: Tensor matrix is highly sparse, which can be hacked to be more compact. will do later... */
        float h0[3], h1[3], h2[3];
        MatMul<1,3,3>(h0, line, Tsr0);
        MatMul<1,3,3>(h1, line, Tsr1);
        MatMul<1,3,3>(h2, line, Tsr2);

        float warp_point[3];
        warp_point[0] = h0[0]*point[0] + h1[0]*point[1] + h2[0]*point[2];
        warp_point[1] = h0[1]*point[0] + h1[1]*point[1] + h2[1]*point[2];
        warp_point[2] = h0[2]*point[0] + h1[2]*point[1] + h2[2]*point[2];

        Matrixf1x3* J_Iref_row = (Matrixf1x3*)((char*)J_Iref.ptr + y*J_Iref.pitch);

        J_Iref_row[x].m[0] = dI_Iref[0] / warp_point[2];
        J_Iref_row[x].m[1] = dI_Iref[1] / warp_point[2];
        J_Iref_row[x].m[2] = -dI_Iref[0] * (warp_point[0] / (warp_point[2]*warp_point[2]))
                             -dI_Iref[1] * (warp_point[1] / (warp_point[2]*warp_point[2]));

    }

}

extern "C" void CalJIrefCUDA(CUDAImage<Pitched2D,Matrixf1x3> J_Iref[PYR_LEVEL_NUM][2],
                             CUDAImage<OpenGL2D,uchar4> *const ref_imgs[PYR_LEVEL_NUM][2],
                             CUDAImage<Pitched2D,float> *const ref_disps[2])
{

    for(int level = 0; level < PYR_LEVEL_NUM; ++level) for(int cam = 0; cam < 2; ++cam)
    {

        ref_imgs[level][cam]->CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeNormalizedFloat, false);
        ref_disps[cam]->CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeElementType, false);

        size_t const frame_w = ref_imgs[level][cam]->GetWidth();
        size_t const frame_h = ref_imgs[level][cam]->GetHeight();

        dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
        dim3 dimGrid = dim3((frame_w+dimBlock.x-1)/dimBlock.x, (frame_h+dimBlock.y-1)/dimBlock.y);
        Cal_J_Iref_Kernel<<<dimGrid, dimBlock>>>(J_Iref[level][cam].GetCUDAPitchedPtr(),
                                                 ref_imgs[level][cam]->cudaTexes,
                                                 ref_disps[cam]->cudaTexes,
                                                 cam == 0 ? true : false,
                                                 level, frame_w, frame_h);

        ref_disps[cam]->DestroyCUDATextureObject();
        ref_imgs[level][cam]->DestroyCUDATextureObject();
    }

}

/**
 *  Jacobian matrix for J_w.
 *
 *  Remark: This matrix is very heavy...
 */
__global__ void Cal_J_w_Kernel(cudaPitchedPtr const J_w,
                               cudaTextureObject_t const ref_disp_tex,
                               bool const is_left,
                               short const level,
                               short const frame_w,
                               short const frame_h)
{

    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x < frame_w && y < frame_h)
    {

        float point[3] = {x, y, 1.0f};

        size_t pyr0_x = x*pow(2.0f, float(level));
        size_t pyr0_y = y*pow(2.0f, float(level));

        float line[3] = {-1.0f,
                         -1.0f,
                         is_left ? point[0] - tex2D<float>(ref_disp_tex, pyr0_x, pyr0_y)/pow(2.0f, float(level)) + point[1] :
                                   point[0] + tex2D<float>(ref_disp_tex, pyr0_x, pyr0_y)/pow(2.0f, float(level)) + point[1]};

        float const fu = const_K[0] / pow(2.0f, float(level));
        float const fv = const_K[4] / pow(2.0f, float(level));
        float const u0 = const_K[6] / pow(2.0f, float(level));
        float const v0 = const_K[7] / pow(2.0f, float(level));

        float K[9], inv_K[9];
        SetZero<3>(K);
        K[0] = fu;
        K[4] = fv;
        K[6] = u0;
        K[7] = v0;
        K[8] = 1.0f;

        SetZero<3>(inv_K);
        inv_K[0] = 1.0f/fu;
        inv_K[4] = 1.0f/fv;
        inv_K[6] = -u0/fu;
        inv_K[7] = -v0/fv;
        inv_K[8] = 1.0f;

        float T[12], T_R[9], T_t[3];
        LieSE3from4x4(T, is_left ? const_T_rl : const_T_lr);
        LieGetRotation(T_R, T);
        LieGetTranslation(T_t, T);

        float trans_inv_K[9];
        MatTranspose<3>(trans_inv_K, inv_K);

        float Tsr0[9], Tsr1[9], Tsr2[9];
        float h0[3], h1[3], h2[3];

        float dr[9], J[3], tmp[9];
        int idx_R[] = {0 ,1, 2, 4, 5, 6, 8, 9, 10};

        Matrixf3x16* J_w_row = (Matrixf3x16*)((char*)J_w.ptr + y*J_w.pitch);
        memset(J_w_row[x].m, 0 , sizeof(Matrixf3x16));

        for(int r = 0; r < 3; ++r) for(int q = 0; q < 3; ++q)
        {
            MatMul<3,1,3>(dr, K+q*3, trans_inv_K+r*3);

            MatMul<3,1,3>(tmp, T_t, dr);
            MatMul<3,3,3>(Tsr0, K, tmp, -1.0f);

            MatMul<3,1,3>(tmp, T_t, dr+3);
            MatMul<3,3,3>(Tsr1, K, tmp, -1.0f);

            MatMul<3,1,3>(tmp, T_t, dr+6);
            MatMul<3,3,3>(Tsr2, K, tmp, -1.0f);

            MatMul<1,3,3>(h0, line, Tsr0);
            MatMul<1,3,3>(h1, line, Tsr1);
            MatMul<1,3,3>(h2, line, Tsr2);

            J[0] = h0[0]*point[0] + h1[0]*point[1] + h2[0]*point[2];
            J[1] = h0[1]*point[0] + h1[1]*point[1] + h2[1]*point[2];
            J[2] = h0[2]*point[0] + h1[2]*point[1] + h2[2]*point[2];

            J_w_row[x].m[3*idx_R[r*3+q]]   = J[0];
            J_w_row[x].m[3*idx_R[r*3+q]+1] = J[1];
            J_w_row[x].m[3*idx_R[r*3+q]+2] = J[2];

        }

        float dt[3], rot[9];
        int idx_t[] = {12, 13, 14};
        for(int o = 0; o < 3; ++o)
        {

            dt[0] = K[o*3];
            dt[1] = K[o*3+1];
            dt[2] = K[o*3+2];

            MatMul<3,3,3>(tmp, T_R, inv_K);
            MatMul<3,3,3>(rot, K, tmp);

            MatMul<3,1,3>(Tsr0, rot, dt);
            MatMul<3,1,3>(Tsr1, rot+3, dt);
            MatMul<3,1,3>(Tsr2, rot+6, dt);

            MatMul<1,3,3>(h0, line, Tsr0);
            MatMul<1,3,3>(h1, line, Tsr1);
            MatMul<1,3,3>(h2, line, Tsr2);

            J[0] = h0[0]*point[0] + h1[0]*point[1] + h2[0]*point[2];
            J[1] = h0[1]*point[0] + h1[1]*point[1] + h2[1]*point[2];
            J[2] = h0[2]*point[0] + h1[2]*point[1] + h2[2]*point[2];

            J_w_row[x].m[3*idx_t[o]]   = J[0];
            J_w_row[x].m[3*idx_t[o]+1] = J[1];
            J_w_row[x].m[3*idx_t[o]+2] = J[2];
        }

    }

}

extern "C" void CalJwCUDA(CUDAImage<Pitched2D,Matrixf3x16> J_w[PYR_LEVEL_NUM][2],
                          CUDAImage<Pitched2D,float> *const ref_disps[2])
{

    for(int i = 0; i < 2; ++i)
        ref_disps[i]->CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeElementType, false);

    size_t frame_w = ref_disps[0]->GetWidth();
    size_t frame_h = ref_disps[0]->GetHeight();
    for(int level = 0; level < PYR_LEVEL_NUM; ++level)
    {
        dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
        dim3 dimGrid = dim3((frame_w+dimBlock.x-1)/dimBlock.x, (frame_h+dimBlock.y-1)/dimBlock.y);

        for(int i = 0; i < 2; ++i)
            Cal_J_w_Kernel<<<dimGrid, dimBlock>>>(J_w[level][i].GetCUDAPitchedPtr(),
                                                  ref_disps[i]->cudaTexes,
                                                  i == 0 ? true : false,
                                                  level, frame_w, frame_h);

        frame_w = (frame_w+1)/2;
        frame_h = (frame_h+1)/2;
    }

    for(int i = 0; i < 2; ++i)
        ref_disps[i]->DestroyCUDATextureObject();

}



/**
 *  Warpping current image by the predicted transformation
 *
 *  Remark: Note that the current image texture is with the entire image size instead of the ROI size.
 *
 */
__global__ void WarpCurImageAndError_Kernel(cudaPitchedPtr const J_cur,
                                            cudaPitchedPtr const I_warp,
                                            cudaPitchedPtr const I_errors,
                                            cudaTextureObject_t const ref_img_tex,
                                            cudaTextureObject_t const ref_disp_tex,
                                            cudaTextureObject_t const cur_img_tex,
                                            bool const is_left,
                                            short const level,
                                            short const frame_w,
                                            short const frame_h)
{

    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x < frame_w && y < frame_h)
    {

        float disp = tex2D<float>(ref_disp_tex, x*pow(2.0f, float(level)), y*pow(2.0f, float(level)))/pow(2.0f, float(level));

        float point[3] = {x, y, 1.0f};
        float line[3] = {-1.0f,
                         -1.0f,
                         is_left ? point[0] - disp + point[1] :
                                   point[0] + disp + point[1]};

        float const fu = const_K[0] / pow(2.0f, float(level));
        float const fv = const_K[4] / pow(2.0f, float(level));
        float const u0 = const_K[6] / pow(2.0f, float(level));
        float const v0 = const_K[7] / pow(2.0f, float(level));

        float K[9], inv_K[9];
        SetZero<3>(K);
        K[0] = fu;
        K[4] = fv;
        K[6] = u0;
        K[7] = v0;
        K[8] = 1.0f;

        SetZero<3>(inv_K);
        inv_K[0] = 1.0f/fu;
        inv_K[4] = 1.0f/fv;
        inv_K[6] = -u0/fu;
        inv_K[7] = -v0/fv;
        inv_K[8] = 1.0f;

        float A[12], A_R[9], A_t[3];
        LieSE3from4x4(A, is_left ? const_T_rl : const_T_lr);
        LieGetRotation(A_R, A);
        LieGetTranslation(A_t, A);

        float B[12], B_R[9], B_t[3];

        float T_live[12], T_tmp[12];
        LieSE3from4x4(T_live, const_T_head);        

        if(is_left)
        {
            float T_cl[12], inv_T_cl[12];

            LieSE3from4x4(T_cl, const_T_cl);
            LieInverseSE3(inv_T_cl, T_cl);

            LieMulSE3(T_tmp, T_live, T_cl);
            LieMulSE3(B, inv_T_cl, T_tmp);
        }
        else
        {
            float T_cr[12], inv_T_cr[12];

            LieSE3from4x4(T_cr, const_T_cr);
            LieInverseSE3(inv_T_cr, T_cr);

            LieMulSE3(T_tmp, T_live, T_cr);
            LieMulSE3(B, inv_T_cr, T_tmp);

        }

        LieGetRotation(B_R, B);
        LieGetTranslation(B_t, B);

        float a_r[9], a_t[3], b_r[9], b_t[3], tmp1[9], tmp2[9];
        MatMul<3,3,3>(tmp1, A_R, inv_K);
        MatMul<3,3,3>(a_r, K, tmp1);
        MatMul<3,3,1>(a_t, K, A_t);

        MatMul<3,3,3>(tmp1, B_R, inv_K);
        MatMul<3,3,3>(b_r, K, tmp1);
        MatMul<3,3,1>(b_t, K, B_t);

        float Tsr0[9], Tsr1[9], Tsr2[9];

        MatMul<3,1,3>(tmp1, a_r, b_t);
        MatMul<3,1,3>(tmp2, a_t, b_r);
        MatSub<3,3>(Tsr0, tmp1, tmp2);

        MatMul<3,1,3>(tmp1, a_r+3, b_t);
        MatMul<3,1,3>(tmp2, a_t, b_r+3);
        MatSub<3,3>(Tsr1, tmp1, tmp2);

        MatMul<3,1,3>(tmp1, a_r+6, b_t);
        MatMul<3,1,3>(tmp2, a_t, b_r+6);
        MatSub<3,3>(Tsr2, tmp1, tmp2);

        float h0[3], h1[3], h2[3];
        MatMul<1,3,3>(h0, line, Tsr0);
        MatMul<1,3,3>(h1, line, Tsr1);
        MatMul<1,3,3>(h2, line, Tsr2);

        float warp_pts[3];
        warp_pts[0] = h0[0]*point[0] + h1[0]*point[1] + h2[0]*point[2];
        warp_pts[1] = h0[1]*point[0] + h1[1]*point[1] + h2[1]*point[2];
        warp_pts[2] = h0[2]*point[0] + h1[2]*point[1] + h2[2]*point[2];

        float proj_x = warp_pts[0]/warp_pts[2];
        float proj_y = warp_pts[1]/warp_pts[2];

        float* I_warp_row = (float*)((char*)I_warp.ptr + y*I_warp.pitch);
        float* I_errors_row = (float*)((char*)I_errors.ptr + y*I_errors.pitch);
        Matrixf2x3* J_cur_row = (Matrixf2x3*)((char*)J_cur.ptr + y*J_cur.pitch);

        if(disp > 0 && proj_x >= 0 && proj_x < frame_w && proj_y >= 0 && proj_y < frame_h)
        {
            float warp_I = FloatRGBA2FloatGray(tex2D<float4>(cur_img_tex, proj_x, proj_y));
            float ref_I = FloatRGBA2FloatGray(tex2D<float4>(ref_img_tex, x, y));

            I_warp_row[x] = warp_I;
            I_errors_row[x] = ref_I - warp_I;

            J_cur_row[x].m[0] = 1.0f / warp_pts[2];
            J_cur_row[x].m[1] = 0.0f;
            J_cur_row[x].m[2] = 0.0f;
            J_cur_row[x].m[3] = 1.0f / warp_pts[2];
            J_cur_row[x].m[4] = -(warp_pts[0] / (warp_pts[2]*warp_pts[2]));
            J_cur_row[x].m[5] = -(warp_pts[1] / (warp_pts[2]*warp_pts[2]));

        }
        else
        {
            I_warp_row[x] = 0.0f;
            I_errors_row[x] = 1.0;

            J_cur_row[x].m[0] = 0.0f;
            J_cur_row[x].m[1] = 0.0f;
            J_cur_row[x].m[2] = 0.0f;
            J_cur_row[x].m[3] = 0.0f;
            J_cur_row[x].m[4] = 0.0f;
            J_cur_row[x].m[5] = 0.0f;
        }

    }

}

extern "C" void WarpCurImageCUDA(CUDAImage<Pitched2D,Matrixf2x3> J_cur[PYR_LEVEL_NUM][2],
                                 CUDAImage<Pitched2D,float> I_warp[PYR_LEVEL_NUM][2],
                                 CUDAImage<Pitched2D,float> I_errors[PYR_LEVEL_NUM][2],
                                 CUDAImage<OpenGL2D,uchar4> st_cur_frames[PYR_LEVEL_NUM][2],
                                 CUDAImage<OpenGL2D,uchar4>* ref_imgs[PYR_LEVEL_NUM][2],
                                 CUDAImage<Pitched2D,float>* ref_disps[2],
                                 size_t const level,
                                 bool const cam)
{

    ref_imgs[level][cam]->CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeNormalizedFloat, false);
    ref_disps[cam]->CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeElementType, false);
    st_cur_frames[level][cam].CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModeLinear, cudaReadModeNormalizedFloat, false);

    size_t const frame_w = I_warp[level][cam].GetWidth();
    size_t const frame_h = I_warp[level][cam].GetHeight();

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((frame_w+dimBlock.x-1)/dimBlock.x,
                        (frame_h+dimBlock.y-1)/dimBlock.y);

    WarpCurImageAndError_Kernel<<<dimGrid, dimBlock>>>(J_cur[level][cam].GetCUDAPitchedPtr(),
                                                       I_warp[level][cam].GetCUDAPitchedPtr(),
                                                       I_errors[level][cam].GetCUDAPitchedPtr(),
                                                       ref_imgs[level][cam]->cudaTexes,
                                                       ref_disps[cam]->cudaTexes,
                                                       st_cur_frames[level][cam].cudaTexes,
                                                       cam == 0 ? true : false,
                                                       level, frame_w, frame_h);

    st_cur_frames[level][cam].DestroyCUDATextureObject();
    ref_imgs[level][cam]->DestroyCUDATextureObject();
    ref_disps[cam]->DestroyCUDATextureObject();

}

/**
 *  Gauss-Newton with calculated Tukey weights, image errors.
 *
 *  Remark: Note that the current image texture is with the entire image size instead of the ROI size.
 *
 */
__global__ void GaussNewton_Kernel(float* dev_block_se3_gradients,
                                   float* dev_block_se3_hessians,
                                   float* dev_block_errors,
                                   cudaPitchedPtr const J_Iref,
                                   cudaPitchedPtr const J_cur,
                                   cudaPitchedPtr const J_w,
                                   cudaTextureObject_t const warp_img_tex,
                                   cudaTextureObject_t const error_img_tex,
                                   cudaTextureObject_t const weight_img_tex,
                                   bool const is_left,
                                   short const frame_w,
                                   short const frame_h)
{

    __shared__ float shared_se3_gradients[SHARE_THREAD_NUM][SHARE_THREAD_NUM][6];
    __shared__ float shared_se3_hessians[SHARE_THREAD_NUM][SHARE_THREAD_NUM][36];
    __shared__ float shared_errors[SHARE_THREAD_NUM][SHARE_THREAD_NUM];

    size_t const block_idx = blockIdx.x + blockIdx.y * gridDim.x;

    short const x = blockIdx.x * blockDim.x + threadIdx.x;
    short const y = blockIdx.y * blockDim.y + threadIdx.y;

    if(x < frame_w && y < frame_h)
    {

        float d_Icur[2];
        d_Icur[0] = (tex2D<float>(warp_img_tex, x+1, y) -
                     tex2D<float>(warp_img_tex, x-1, y)) / 2.0f;
        d_Icur[1] = (tex2D<float>(warp_img_tex, x, y+1) -
                     tex2D<float>(warp_img_tex, x, y-1)) / 2.0f;

        float J_Icur[3];
        Matrixf2x3* J_cur_row = (Matrixf2x3*)((char*)J_cur.ptr + y*J_cur.pitch);
        MatMul<1,2,3>(J_Icur, d_Icur, J_cur_row[x].m);

        Matrixf1x3* J_Iref_row = (Matrixf1x3*)((char*)J_Iref.ptr + y*J_Iref.pitch);
        Matrixf3x16* J_w_row = (Matrixf3x16*)((char*)J_w.ptr + y*J_w.pitch);

        float J_IrefPlusJ_Icur[3];
        MatAdd<3,1>(J_IrefPlusJ_Icur, J_Iref_row[x].m, J_Icur);
        MatMul<3,1>(J_IrefPlusJ_Icur, J_IrefPlusJ_Icur, 0.5f);

        float J_I_w[16];
        MatMul<1,3,16>(J_I_w, J_IrefPlusJ_Icur, J_w_row[x].m);

        float J_SE3xJ_adj[16*6];
        if(is_left)
            MatMul<16,6,6>(J_SE3xJ_adj, const_J_SE3, const_J_ladj);
        else
            MatMul<16,6,6>(J_SE3xJ_adj, const_J_SE3, const_J_radj);

        float J[6];
        MatMul<1,16,6>(J, J_I_w, J_SE3xJ_adj);

        float error = tex2D<float>(error_img_tex, x, y);

        float weight = tex2D<float>(weight_img_tex, x, y);;
        error *= weight;

        float Hessian[36];
        MatMul<6,1,6>(Hessian, J, J, weight);

        float gradient[6];
        MatMul<1,6>(gradient, J, error);

        for(int i = 0; i < 6; ++i)
            shared_se3_gradients[threadIdx.x][threadIdx.y][i] = gradient[i];

        for(int i = 0; i < 36; ++i)
            shared_se3_hessians[threadIdx.x][threadIdx.y][i] = Hessian[i];

        shared_errors[threadIdx.x][threadIdx.y] = error*error;


    }
    else
    {
        memset(&shared_se3_gradients[threadIdx.x][threadIdx.y], 0, sizeof(float)*6);
        memset(&shared_se3_hessians[threadIdx.x][threadIdx.y], 0, sizeof(float)*36);
        shared_errors[threadIdx.x][threadIdx.y] = 0.0f;
    }

    __syncthreads();

    /* Two-dimensional reduction */
    for(uint32_t s = blockDim.x >> 1; s > 0; s >>= 1)
    {
        if(threadIdx.x < s)
        {
            for(int i = 0; i < 6; ++i)
                shared_se3_gradients[threadIdx.x][threadIdx.y][i] += shared_se3_gradients[threadIdx.x+s][threadIdx.y][i];

            for(int i = 0; i < 36; ++i)
                shared_se3_hessians[threadIdx.x][threadIdx.y][i] += shared_se3_hessians[threadIdx.x+s][threadIdx.y][i];

                shared_errors[threadIdx.x][threadIdx.y] += shared_errors[threadIdx.x+s][threadIdx.y];
        }
        __syncthreads();
    }

    for(uint32_t s = blockDim.y >> 1; s > 0; s >>= 1)
    {
        if (threadIdx.y < s)
        {
            for(int i = 0; i < 6; ++i)
                shared_se3_gradients[threadIdx.x][threadIdx.y][i] += shared_se3_gradients[threadIdx.x][threadIdx.y+s][i];

            for(int i = 0; i < 36; ++i)
                shared_se3_hessians[threadIdx.x][threadIdx.y][i] += shared_se3_hessians[threadIdx.x][threadIdx.y+s][i];

            shared_errors[threadIdx.x][threadIdx.y] += shared_errors[threadIdx.x][threadIdx.y+s];
        }
        __syncthreads();
    }

    if(threadIdx.x == 0 && threadIdx.y == 0)
    {

        for(int i = 0; i < 6; ++i)
            dev_block_se3_gradients[block_idx*6+i] = shared_se3_gradients[threadIdx.x][threadIdx.y][i];

        for(int i = 0; i < 36; ++i)
            dev_block_se3_hessians[block_idx*36+i] = shared_se3_hessians[threadIdx.x][threadIdx.y][i];

        dev_block_errors[block_idx] = shared_errors[threadIdx.x][threadIdx.y];
    }

}

extern "C" void GaussNewtonCUDA(float* dev_block_se3_gradients[PYR_LEVEL_NUM],
                                float* dev_block_se3_hessians[PYR_LEVEL_NUM],
                                float* dev_block_errors[PYR_LEVEL_NUM],
                                float* block_se3_gradients[PYR_LEVEL_NUM],
                                float* block_se3_hessians[PYR_LEVEL_NUM],
                                float* block_errors[PYR_LEVEL_NUM],
                                size_t const num_blocks[PYR_LEVEL_NUM],
                                CUDAImage<Pitched2D,Matrixf1x3> J_Iref[PYR_LEVEL_NUM][2],
                                CUDAImage<Pitched2D,Matrixf2x3> J_cur[PYR_LEVEL_NUM][2],
                                CUDAImage<Pitched2D,Matrixf3x16> J_w[PYR_LEVEL_NUM][2],
                                CUDAImage<Pitched2D,float> I_warp[PYR_LEVEL_NUM][2],
                                CUDAImage<Pitched2D,float> I_errors[PYR_LEVEL_NUM][2],
                                CUDAImage<Pitched2D,float> I_weights[PYR_LEVEL_NUM][2],
                                size_t const level,
                                bool const cam)
{

    I_warp[level][cam].CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeElementType, false);
    I_errors[level][cam].CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeElementType, false);
    I_weights[level][cam].CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeElementType, false);

    size_t const frame_w = I_warp[level][cam].GetWidth();
    size_t const frame_h = I_warp[level][cam].GetHeight();

    dim3 dimBlock = dim3(SHARE_THREAD_NUM, SHARE_THREAD_NUM);
    dim3 dimGrid = dim3((frame_w+dimBlock.x-1)/dimBlock.x,
                        (frame_h+dimBlock.y-1)/dimBlock.y);
    GaussNewton_Kernel<<<dimGrid, dimBlock>>>(dev_block_se3_gradients[level],
                                              dev_block_se3_hessians[level],
                                              dev_block_errors[level],
                                              J_Iref[level][cam].GetCUDAPitchedPtr(),
                                              J_cur[level][cam].GetCUDAPitchedPtr(),
                                              J_w[level][cam].GetCUDAPitchedPtr(),
                                              I_warp[level][cam].cudaTexes,
                                              I_errors[level][cam].cudaTexes,
                                              I_weights[level][cam].cudaTexes,
                                              cam == 0 ? true : false,
                                              frame_w,
                                              frame_h);

    checkCudaErrors(cudaMemcpy(block_se3_gradients[level], dev_block_se3_gradients[level], sizeof(float)*num_blocks[level]*6, cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(block_se3_hessians[level], dev_block_se3_hessians[level], sizeof(float)*num_blocks[level]*36, cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(block_errors[level], dev_block_errors[level], sizeof(float)*num_blocks[level], cudaMemcpyDeviceToHost));

    I_weights[level][cam].DestroyCUDATextureObject();
    I_errors[level][cam].DestroyCUDATextureObject();
    I_warp[level][cam].DestroyCUDATextureObject();

}


/*
 *  Copy error image to OpenGL RGBA texture
 *
 *  Remark: The warped image is normalised to [0, 1]
 */
__global__ void GetErrorImageCUDAKernel(cudaTextureObject_t const error_img_tex,
                                        short const frame_w,
                                        short const frame_h)
{
    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x < frame_w && y < frame_h)
    {

        float grayf = tex2D<float>(error_img_tex, x, y);
        int8_t gray = 255*abs(grayf);
        uchar4 data = make_uchar4(gray, gray, gray, 255);
        surf2Dwrite(data, img_surface, x*4, y);

    }

}

extern "C" void GetErrorImageCUDA(CUDAImage<OpenGL2D,uchar4> gl_img[PYR_LEVEL_NUM][2],
                                  CUDAImage<Pitched2D,float> I_errors[PYR_LEVEL_NUM][2],
                                  size_t const level,
                                  bool const cam)
{

    size_t const frame_w = I_errors[level][cam].GetWidth();
    size_t const frame_h = I_errors[level][cam].GetHeight();

    I_errors[level][cam].CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeElementType, false);

    gl_img[level][cam].MapCUDARes();

    checkCudaErrors(cudaBindSurfaceToArray(img_surface, gl_img[level][cam].GetArrayPtr()));

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((frame_w+dimBlock.x-1)/dimBlock.x, (frame_h+dimBlock.y-1)/dimBlock.y);
    GetErrorImageCUDAKernel<<<dimGrid, dimBlock>>>(I_errors[level][cam].cudaTexes,
                                                   frame_w,
                                                   frame_h);

    gl_img[level][cam].UnmapCUDARes();
    I_errors[level][cam].DestroyCUDATextureObject();

}


/*
 *  Copy warped image to OpenGL RGBA texture
 *
 *  Remark: The warped image is normalised to [0, 1]
 */
__global__ void GetWarpedImageCUDAKernel(cudaTextureObject_t const warp_img_tex,
                                         short const frame_w,
                                         short const frame_h)
{
    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x < frame_w && y < frame_h)
    {

        float grayf = tex2D<float>(warp_img_tex, x, y);
        int8_t gray = grayf*255;
        uchar4 data = make_uchar4(gray, gray, gray, 255);
        surf2Dwrite(data, img_surface, x*4, y);

    }

}

extern "C" void GetWarpedImageCUDA(CUDAImage<OpenGL2D,uchar4> gl_img[PYR_LEVEL_NUM][2],
                                   CUDAImage<Pitched2D,float> I_warp[PYR_LEVEL_NUM][2],
                                   size_t const level,
                                   bool const cam)
{

    short const frame_w = gl_img[level][cam].GetWidth();
    short const frame_h = gl_img[level][cam].GetHeight();

    I_warp[level][cam].CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeElementType, false);
    gl_img[level][cam].MapCUDARes();

    checkCudaErrors(cudaBindSurfaceToArray(img_surface, gl_img[level][cam].GetArrayPtr()));

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((frame_w+dimBlock.x-1)/dimBlock.x, (frame_h+dimBlock.y-1)/dimBlock.y);
    GetWarpedImageCUDAKernel<<<dimGrid, dimBlock>>>(I_warp[level][cam].cudaTexes,
                                                    frame_w,
                                                    frame_h);

    gl_img[level][cam].UnmapCUDARes();
    I_warp[level][cam].DestroyCUDATextureObject();

}

/*
 *  Copy weight image to OpenGL RGBA texture
 *
 *  Remark: The image wieght is in [0, 1]
 */
__global__ void GetWightImageCUDAKernel(cudaTextureObject_t const weight_img_tex,
                                        short const frame_w,
                                        short const frame_h)
{
    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    if(x < frame_w && y < frame_h)
    {

        float weight = tex2D<float>(weight_img_tex, x, y);

        if(weight < 0.01f)
        {
            uchar4 data = make_uchar4(0, 0, 150, 255);
            surf2Dwrite(data, img_surface, x*4, y);

        }
        else
        {
            int8_t gray = weight*255;
            uchar4 data = make_uchar4(gray, gray, gray, 255);
            surf2Dwrite(data, img_surface, x*4, y);
        }
    }

}

extern "C" void GetWeightImageCUDA(CUDAImage<OpenGL2D,uchar4> gl_img[PYR_LEVEL_NUM][2],
                                   CUDAImage<Pitched2D,float> I_weights[PYR_LEVEL_NUM][2],
                                   size_t const level,
                                   bool const cam)
{

    short const frame_w = gl_img[level][cam].GetWidth();
    short const frame_h = gl_img[level][cam].GetHeight();

    I_weights[level][cam].CreateCUDATextureObject(cudaAddressModeBorder, cudaFilterModePoint, cudaReadModeElementType, false);
    gl_img[level][cam].MapCUDARes();

    checkCudaErrors(cudaBindSurfaceToArray(img_surface, gl_img[level][cam].GetArrayPtr()));

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((frame_w+dimBlock.x-1)/dimBlock.x, (frame_h+dimBlock.y-1)/dimBlock.y);
    GetWightImageCUDAKernel<<<dimGrid, dimBlock>>>(I_weights[level][cam].cudaTexes,
                                                   frame_w,
                                                   frame_h);

    gl_img[level][cam].UnmapCUDARes();
    I_weights[level][cam].DestroyCUDATextureObject();

}

//__global__ void TextureTestKernel(cudaPitchedPtr test_img,
//                                  cudaTextureObject_t const texObj,
//                                  size_t const w,
//                                  size_t const h)
//{

//    size_t const x = blockDim.x * blockIdx.x + threadIdx.x;
//    size_t const y = blockDim.y * blockIdx.y + threadIdx.y;

//    if(x < w && y < h)
//    {
//        float* test_img_row = (float*)((char*)test_img.ptr + y*test_img.pitch);
//        test_img_row[x] = FloatRGBA2FloatGray(tex2D<float4>(texObj, x+0.5f, y+0.5f));
//    }

//}

//void Tracker::TextureTest(CUDAImage<Pitched2D,float>& test_img,
//                          cudaTextureObject_t const texObj)
//{

//    size_t const w = test_img.GetWidth();
//    size_t const h = test_img.GetHeight();

//    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
//    dim3 dimGrid = dim3((w+dimBlock.x-1)/dimBlock.x, (h+dimBlock.y-1)/dimBlock.y);
//    TextureTestKernel<<<dimGrid, dimBlock>>>(test_img.GetCUDAPitchedPtr(),
//                                             texObj,
//                                             w,
//                                             h);

//}



} // namespace falcon {

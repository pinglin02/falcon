#include <stdio.h>

#include "cuda_math.h"

#include "cuda_reconst.h"

namespace falcon {

/* CUDA texutres */
texture<uchar4, cudaTextureType2D, cudaReadModeElementType> ref_img_tex;
texture<uchar4, cudaTextureType2D, cudaReadModeElementType> target_img_tex;

texture<float, cudaTextureType2D, cudaReadModeElementType> win_disp_tex;

texture<float, cudaTextureType2D, cudaReadModeElementType> min_disp_cost_tex;
texture<float, cudaTextureType2D, cudaReadModeElementType> max_disp_cost_tex;

texture<float, cudaTextureType2D, cudaReadModeElementType> aux_tex;

texture<float, cudaTextureType2D, cudaReadModeElementType> head_primal_tex;
texture<float, cudaTextureType2D, cudaReadModeElementType> old_primal_tex;

texture<float, cudaTextureType2D, cudaReadModeElementType> primal_tex;
texture<float, cudaTextureType2D, cudaReadModeElementType> primal_step_tex;

texture<float2, cudaTextureType2D, cudaReadModeElementType> dual_tex;
texture<float2, cudaTextureType2D, cudaReadModeElementType> dual_step_tex;

texture<float4, cudaTextureType2D, cudaReadModeElementType> diffuse_tensor_tex;

/* Write OpenGL texture by using CUDA surface */
surface<void, cudaSurfaceType3D> cost_volume_surface;
surface<void, cudaSurfaceType2D> disp_surface;

/* Constant variables */
float const cost_max = 1.0;

/* Constant structures */
//__constant__ CostVolume::Params const_dev_cv_params;
//__constant__ HuberL1TGV1::Params const_dev_hbl1_tgv1_params;

/* Camera intrinsic matrix */
__constant__ float const_dev_K[9];
__constant__ float const_dev_T[16];

namespace CostVolume {

/*
 *  Cost-Volume Construction
 *
 *  Remark: The cost value is normalised to [0,1]
 */

/* Absolute Differences (AD) */
__global__ void ADKernel(CostVolume::Params const cv_params)
{
    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;
    short const z = blockDim.z * blockIdx.z + threadIdx.z;

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    short const min_disp = cv_params.min_disp;

    RefImage const ref_img  = cv_params.ref_img;

    uint8_t const min_bg_thr = cv_params.min_bg_thr;
    uint8_t const max_bg_thr = cv_params.max_bg_thr;

    if(x < roi_w && y < roi_h && z < cv_params.num_disp)
    {
        surf3Dwrite(cost_max, cost_volume_surface, x*sizeof(float), y, z);

        uint32_t const fx = x + cv_params.roi[cv_params.ref_img].x_min;
        uint32_t const fy = y + cv_params.roi[cv_params.ref_img].y_min;

        uint8_t const ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx, fy));

        if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
        {

            uint32_t disp = z+min_disp;
            switch (ref_img)
            {
            case LeftRefImage:
            {
                float cost = abs(RGBA2FloatGray(tex2D(ref_img_tex, fx, fy)) - RGBA2FloatGray(tex2D(target_img_tex, fx-disp, fy)));
                surf3Dwrite(cost, cost_volume_surface, x*sizeof(float), y, z);
            }
                break;
            case RightRefImage:
            {
                    float cost = abs(RGBA2FloatGray(tex2D(ref_img_tex, fx, fy)) - RGBA2FloatGray(tex2D(target_img_tex, fx+disp, fy)));
                    surf3Dwrite(cost, cost_volume_surface, x*sizeof(float), y, z);
            }
                break;
            default:
                break;
            }
        }
    }
}

/* Zero norm_grad_imgalised Cross-Correlation (ZNCC) */
__global__ void ZNCCKernel(CostVolume::Params const cv_params)
{
    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;
    short const z = blockDim.z * blockIdx.z + threadIdx.z;

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    short const min_disp = cv_params.min_disp;
    short const win_r = cv_params.win_r;

    RefImage const ref_img  = cv_params.ref_img;

    uint8_t const min_bg_thr = cv_params.min_bg_thr;
    uint8_t const max_bg_thr = cv_params.max_bg_thr;

    if (x < roi_w && y < roi_h && z < cv_params.num_disp)
    {
        surf3Dwrite(cost_max, cost_volume_surface, x*sizeof(float), y, z);

        uint32_t const fx = x + cv_params.roi[cv_params.ref_img].x_min;
        uint32_t const fy = y + cv_params.roi[cv_params.ref_img].y_min;

        uint8_t const ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx, fy));

        if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
        {            
            switch (ref_img)
            {
            case LeftRefImage:
            {
                uint32_t disp = z+min_disp;
                float sum_ref = 0, sum_target = 0, sum_ref_target = 0, sum_sq_ref = 0, sum_sq_target = 0;

                for(int win_x = fx-win_r; win_x <= fx+win_r; win_x++)
                    for(int win_y = fy-win_r; win_y <= fy+win_r; win_y++)
                    {
                        float ref_gray = RGBA2FloatGray(tex2D(ref_img_tex, win_x, win_y));
                        float target_gray = RGBA2FloatGray(tex2D(target_img_tex, win_x-disp, win_y));

                        sum_ref += ref_gray;
                        sum_target += target_gray;

                        sum_ref_target += (ref_gray*target_gray);

                        sum_sq_ref += (ref_gray*ref_gray);
                        sum_sq_target += (target_gray*target_gray);
                    }

                float N = (2*win_r+1)*(2*win_r+1);

                float numerator = N*sum_ref_target - sum_ref*sum_target;
                float denominator = (N*sum_sq_target - sum_target*sum_target)*(N*sum_sq_ref - sum_ref*sum_ref);

                if(denominator > 0)
                {
                    float cost = 1.0 - (numerator*rsqrtf(denominator)+1.0)/2.0;
                    surf3Dwrite(cost, cost_volume_surface, x*sizeof(float), y, z);
                }

            }
                break;
            case RightRefImage:                
            {
                    uint32_t disp = z+min_disp;
                    float sum_ref = 0, sum_target = 0, sum_ref_target = 0, sum_sq_ref = 0, sum_sq_target = 0;

                    for(int win_x = fx-win_r; win_x <= fx+win_r; win_x++)
                        for(int win_y = fy-win_r; win_y <= fy+win_r; win_y++)
                        {
                              float ref_gray = RGBA2FloatGray(tex2D(ref_img_tex, win_x, win_y));
                            float target_gray = RGBA2FloatGray(tex2D(target_img_tex, win_x+disp, win_y));

                            sum_ref += ref_gray;
                            sum_target += target_gray;

                            sum_ref_target += ref_gray*target_gray;

                            sum_sq_ref += ref_gray*ref_gray;
                            sum_sq_target += target_gray*target_gray;
                        }

                    float N = (2*win_r+1)*(2*win_r+1);

                    float numerator = N*sum_ref_target - sum_ref*sum_target;
                    float denominator = (N*sum_sq_target - sum_target*sum_target)*(N*sum_sq_ref - sum_ref*sum_ref);

                    if(denominator > 0)
                    {
                        float cost = 1.0 - (numerator*rsqrtf(denominator)+1.0)/2.0;
                        surf3Dwrite(cost, cost_volume_surface, x*sizeof(float), y, z);
                    }

                }
                break;
            default:
                break;
            }

        }
    }

}

void BuildCostVolume(cudaArray *const cost_volume_array,
                     CostVolume::Params const cv_params)
{

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    checkCudaErrors(cudaBindSurfaceToArray(cost_volume_surface, cost_volume_array));

    dim3 dimBlock(THREAD_NUM_3D_BLOCK, THREAD_NUM_3D_BLOCK, THREAD_NUM_3D_BLOCK);
    dim3 dimGrid((roi_w+dimBlock.x-1)/dimBlock.x,
                 (roi_h+dimBlock.y-1)/dimBlock.y,
                 (cv_params.num_disp+dimBlock.z-1)/dimBlock.z);

    /* AD */
    if(cv_params.method == AD)
        ADKernel<<<dimGrid, dimBlock>>>(cv_params);

    /* ZNCC */
    if(cv_params.method == ZNCC)
        ZNCCKernel<<<dimGrid, dimBlock>>>(cv_params);

}

/*
 *  Winner-take-all kernel
 *
 *  Remark: The cost value is normalised to [0,1]
 */

__global__ void WinnerTakeAllKernel(cudaPitchedPtr const win_disp,
                                    cudaPitchedPtr const min_disp_cost,
                                    cudaPitchedPtr const max_disp_cost,
                                    CostVolume::Params const cv_params)
{
    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    short const num_disp = cv_params.num_disp;

    if(x < roi_w && y < roi_h)
    {

        float min_cost = cost_max;
        short min_cost_idx = 0;
        for(int z = 0; z < num_disp; ++z)
        {

            float cost = surf3Dread<float>(cost_volume_surface, x*sizeof(float), y, z);
            if(cost < min_cost)
            {
                min_cost = cost;
                min_cost_idx = z;
            }
        }

        float* win_disp_row = (float*)((char*)win_disp.ptr + y*win_disp.pitch);
        win_disp_row[x] = float(min_cost_idx)/float(num_disp-1);

        float* min_disp_cost_row = (float*)((char*)min_disp_cost.ptr + y*min_disp_cost.pitch);
        min_disp_cost_row[x] = min_cost;

        /* Search for max cost value */
        float max_cost = -1;
        for(uint32_t z = 0; z < num_disp; z++)
        {
            float cost = surf3Dread<float>(cost_volume_surface, x*sizeof(float), y, z);
            if(cost > max_cost)
                max_cost = cost;
        }

        float* max_disp_cost_row = (float*)((char*)max_disp_cost.ptr + y*max_disp_cost.pitch);
        max_disp_cost_row[x] = max_cost;
    }

}

void WinnerTakeAll(CUDAImage<Pitched2D,float> const& win_disp,
                   CUDAImage<Pitched2D,float> const& min_disp_cost,
                   CUDAImage<Pitched2D,float> const& max_disp_cost,
                   CostVolume::Params const cv_params)
{

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((roi_w+dimBlock.x-1)/dimBlock.x, (roi_h+dimBlock.y-1)/dimBlock.y);
    WinnerTakeAllKernel<<<dimGrid, dimBlock>>>(win_disp.GetCUDAPitchedPtr(),
                                               min_disp_cost.GetCUDAPitchedPtr(),
                                               max_disp_cost.GetCUDAPitchedPtr(),
                                               cv_params);
}

} // namespace CostVolume {


namespace HuberROF {

/*
 *  Diffusion tensor kernel
 *
 *  Remark: Diffusion tensor is constant so that can be pre-computed and stored/accessed by texture for efficiency.
 *
 */

__global__ void CalculateDiffuseTensorKernel(cudaPitchedPtr const diffuse_tensor,
                                             CostVolume::Params const cv_params,
                                             HuberROF::Params const opt_params)
{
    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    float const alpha   = opt_params.alpha;
    float const beta    = opt_params.beta;

    if(x < roi_w && y < roi_h)
    {

        uint32_t const fx = x + cv_params.roi[cv_params.ref_img].x_min;
        uint32_t const fy = y + cv_params.roi[cv_params.ref_img].y_min;

        HuberROF::DiffuseTensor* diff_tensor_row = (HuberROF::DiffuseTensor*)((char*)diffuse_tensor.ptr + y*diffuse_tensor.pitch);

        float nabla_img[2];
        nabla_img[0] = RGBA2FloatGray(tex2D(ref_img_tex, fx+1, fy)) - RGBA2FloatGray(tex2D(ref_img_tex, fx, fy));
        nabla_img[1] = RGBA2FloatGray(tex2D(ref_img_tex, fx, fy+1)) - RGBA2FloatGray(tex2D(ref_img_tex, fx, fy));

        float norm_nabla_img = sqrt(nabla_img[0]*nabla_img[0] + nabla_img[1]*nabla_img[1]);

        float edge_weight = exp(-alpha*pow(norm_nabla_img, beta));

        if(norm_nabla_img > 0)
        {

            float n[2], p_n[2];

            n[0] = nabla_img[0]/norm_nabla_img;
            n[1] = nabla_img[1]/norm_nabla_img;
            p_n[0] = -n[1];
            p_n[1] = n[0];

            /* Diffision tensor D indexed as [0 2; 1 3] */
            diff_tensor_row[x].D[0] = edge_weight*(n[0]*n[0]) + p_n[0]*p_n[0];
            diff_tensor_row[x].D[1] = edge_weight*(n[1]*n[0]) + p_n[1]*p_n[0];
            diff_tensor_row[x].D[2] = edge_weight*(n[0]*n[1]) + p_n[0]*p_n[1];
            diff_tensor_row[x].D[3] = edge_weight*(n[1]*n[1]) + p_n[1]*p_n[1];
        }
        else
        {
            diff_tensor_row[x].D[0] = edge_weight;
            diff_tensor_row[x].D[1] = 0;
            diff_tensor_row[x].D[2] = 0;
            diff_tensor_row[x].D[3] = edge_weight;
        }

    }

}

void CalculateDiffuseTensor(CUDAImage<Pitched2D,DiffuseTensor> const& diffuse_tensor,
                            CostVolume::Params const cv_params,
                            HuberROF::Params const opt_params)
{

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    checkCudaErrors(cudaMemset2D(diffuse_tensor.ptr, diffuse_tensor.pitch, 0.0, roi_w*sizeof(HuberROF::DiffuseTensor), roi_h));

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((roi_w+dimBlock.x-1)/dimBlock.x, (roi_h+dimBlock.y-1)/dimBlock.y);
    CalculateDiffuseTensorKernel<<<dimGrid, dimBlock>>>(diffuse_tensor.GetCUDAPitchedPtr(),
                                                        cv_params,
                                                        opt_params);
}


/*
 *  Preconditioning kernel
 *
 *  Remark: The step sigma and tau are hard-coded into an image array, so as for the diffusion tensor.
 *
 */

__global__ void PreconditioningKernel(cudaPitchedPtr const priaml_step,
                                      cudaPitchedPtr const dual_step,
                                      CostVolume::Params const cv_params,
                                      HuberROF::Params const opt_params)
{
    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    if(x < roi_w && y < roi_h)
    {

        HuberROF::PrimalStep* primal_step_row = (HuberROF::PrimalStep*)((char*)priaml_step.ptr + y*priaml_step.pitch);
        HuberROF::DualStep* dual_step_row = (HuberROF::DualStep*)((char*)dual_step.ptr + y*dual_step.pitch);

        float D[4], Dx[4], Dy[4];

        D[0] = tex2D(diffuse_tensor_tex, x, y).x;
        D[1] = tex2D(diffuse_tensor_tex, x, y).y;
        D[2] = tex2D(diffuse_tensor_tex, x, y).z;
        D[3] = tex2D(diffuse_tensor_tex, x, y).w;

        Dx[0] = tex2D(diffuse_tensor_tex, x-1, y).x;
        Dx[1] = tex2D(diffuse_tensor_tex, x-1, y).y;
        Dx[2] = tex2D(diffuse_tensor_tex, x-1, y).z;
        Dx[3] = tex2D(diffuse_tensor_tex, x-1, y).w;

        Dy[0] = tex2D(diffuse_tensor_tex, x, y-1).x;
        Dy[1] = tex2D(diffuse_tensor_tex, x, y-1).y;
        Dy[2] = tex2D(diffuse_tensor_tex, x, y-1).z;
        Dy[3] = tex2D(diffuse_tensor_tex, x, y-1).w;

        /* D*nabla sum along columns for dual */
        if(x < roi_w)
            dual_step_row[x].sigma[0] = 2*abs(D[0]) + 2*abs(D[2]);

        if(y < roi_h)
            dual_step_row[x].sigma[1] = 2*abs(D[1]) + 2*abs(D[3]);

        dual_step_row[x].sigma[0] = dual_step_row[x].sigma[0] == 0 ? 0:1.0/dual_step_row[x].sigma[0];
        dual_step_row[x].sigma[1] = dual_step_row[x].sigma[1] == 0 ? 0:1.0/dual_step_row[x].sigma[1];

        /* D*nabla sum along rows for primal */
        primal_step_row[x].tau += abs(D[0]) + abs(D[2]);
        primal_step_row[x].tau += abs(D[1]) + abs(D[3]);

        if(x > 0 && y > 0)
            primal_step_row[x].tau += abs(Dx[0]) + abs(Dy[3]);

        if(x == 0 && y > 0)
            primal_step_row[x].tau += abs(Dy[3]);

        if(y == 0 && x > 0)
            primal_step_row[x].tau += abs(Dx[0]);

        primal_step_row[x].tau = primal_step_row[x].tau == 0 ? 0 : 1.0/primal_step_row[x].tau;

    }

}


void Preconditioning(CUDAImage<Pitched2D,DiffuseTensor> const& diffuse_tensor,
                     CUDAImage<Pitched2D,PrimalStep> const& primal_step,
                     CUDAImage<Pitched2D,DualStep> const& dual_step,
                     CostVolume::Params const cv_params,
                     HuberROF::Params const opt_params)
{

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    checkCudaErrors(cudaMemset2D(primal_step.ptr, primal_step.pitch, 0.0, roi_w*sizeof(HuberROF::PrimalStep), roi_h));
    checkCudaErrors(cudaMemset2D(dual_step.ptr, dual_step.pitch, 0.0, roi_w*sizeof(HuberROF::DualStep), roi_h));

    checkCudaErrors(cudaBindTexture2D(0, diffuse_tensor_tex, diffuse_tensor.ptr,
                                      cudaCreateChannelDesc<float4>(),
                                      roi_w, roi_h, diffuse_tensor.pitch));

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((roi_w+dimBlock.x-1)/dimBlock.x, (roi_h+dimBlock.y-1)/dimBlock.y);
    PreconditioningKernel<<<dimGrid, dimBlock>>>(primal_step.GetCUDAPitchedPtr(),
                                                 dual_step.GetCUDAPitchedPtr(),
                                                 cv_params,
                                                 opt_params);

    checkCudaErrors(cudaUnbindTexture(diffuse_tensor_tex));

}

/*
 *  Huber-L2 preconditioning dual updating kernel
 *
 *  Remark: Diffusion tensor in float4 texture: [x z; y w] = [0 2; 1 3].
 *
 */
__global__ void HuberROFDualKernel(cudaPitchedPtr const dual,
                                  float const epsilon,
                                  CostVolume::Params const cv_params)
{

    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    uint8_t const min_bg_thr = cv_params.min_bg_thr;
    uint8_t const max_bg_thr = cv_params.max_bg_thr;

    if(x < roi_w && y < roi_h)
    {

        uint32_t const fx = x + cv_params.roi[cv_params.ref_img].x_min;
        uint32_t const fy = y + cv_params.roi[cv_params.ref_img].y_min;

        uint8_t const ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx, fy));

        if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
        {

            HuberROF::Dual* dual_row = (HuberROF::Dual*)((char*)dual.ptr + y*dual.pitch);

            float D_nabla_primal[2], sigma[2], D[4];

            D[0] = tex2D(diffuse_tensor_tex, x, y).x;
            D[1] = tex2D(diffuse_tensor_tex, x, y).y;
            D[2] = tex2D(diffuse_tensor_tex, x, y).z;
            D[3] = tex2D(diffuse_tensor_tex, x, y).w;

            if(x < roi_w)
                D_nabla_primal[0] = -(D[0]+D[2])*tex2D(head_primal_tex, x, y) +
                        D[0]*tex2D(head_primal_tex, x+1, y) + D[2]*tex2D(head_primal_tex, x, y+1);

            if(y < roi_h)
                D_nabla_primal[1] = -(D[1]+D[3])*tex2D(head_primal_tex, x, y) +
                        D[1]*tex2D(head_primal_tex, x+1, y) + D[3]*tex2D(head_primal_tex, x, y+1);

//            if(x == roi_w-1)
//                D_nabla_primal[1] = -(D[1]+D[3])*tex2D(head_primal_tex, x, y) + D[3]*tex2D(head_primal_tex, x, y+1);

//            if(y == roi_h-1)
//                D_nabla_primal[0] = -(D[0]+D[2])*tex2D(head_primal_tex, x, y) + D[0]*tex2D(head_primal_tex, x+1, y);

            sigma[0] = tex2D(dual_step_tex, x, y).x;
            sigma[1] = tex2D(dual_step_tex, x, y).y;

            for(uint8_t i = 0; i < 2; i++)
                dual_row[x].p[i] = (dual_row[x].p[i] + sigma[i]*D_nabla_primal[i]) / (1.0f + sigma[i]*epsilon);

            // constrain \epsilon < ||p|| <= 1.0
            float p_norm = sqrt(dual_row[x].p[0]*dual_row[x].p[0] + dual_row[x].p[1]*dual_row[x].p[1]);
            if(p_norm > epsilon)
            {
                float reprojection = fmaxf(1.0f, p_norm);

                for(uint8_t i = 0; i < 2; i++)
                    dual_row[x].p[i] /= reprojection;
            }
        }
    }
}

/*
 *  Huber-L2 preconditioning primal updating kernel
 *
 *  Remark: Diffusion tensor in float4 texture: [x z; y w].
 *
 */
__global__ void HuberROFPrimalKernel(cudaPitchedPtr const primal,
                                    float const aux_theta,
                                    CostVolume::Params const cv_params)
{

    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    uint8_t const min_bg_thr = cv_params.min_bg_thr;
    uint8_t const max_bg_thr = cv_params.max_bg_thr;

    if(x < roi_w && y < roi_h)
    {

        uint32_t const fx = x + cv_params.roi[cv_params.ref_img].x_min;
        uint32_t const fy = y + cv_params.roi[cv_params.ref_img].y_min;

        uint8_t const ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx, fy));

        if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
        {

            HuberROF::Primal* primal_row = (HuberROF::Primal*)((char*)primal.ptr + y*primal.pitch);

            float div_D_dual = 0.0, D[4], Dx[4], Dy[4];

            D[0] = tex2D(diffuse_tensor_tex, x, y).x;
            D[1] = tex2D(diffuse_tensor_tex, x, y).y;
            D[2] = tex2D(diffuse_tensor_tex, x, y).z;
            D[3] = tex2D(diffuse_tensor_tex, x, y).w;

            Dx[0] = tex2D(diffuse_tensor_tex, x-1, y).x;
            Dx[1] = tex2D(diffuse_tensor_tex, x-1, y).y;
            Dx[2] = tex2D(diffuse_tensor_tex, x-1, y).z;
            Dx[3] = tex2D(diffuse_tensor_tex, x-1, y).w;

            Dy[0] = tex2D(diffuse_tensor_tex, x, y-1).x;
            Dy[1] = tex2D(diffuse_tensor_tex, x, y-1).y;
            Dy[2] = tex2D(diffuse_tensor_tex, x, y-1).z;
            Dy[3] = tex2D(diffuse_tensor_tex, x, y-1).w;

            if(x < roi_w)
                div_D_dual += (D[0]+D[2])*tex2D(dual_tex, x, y).x;

            if(y < roi_h)
                div_D_dual += (D[1]+D[3])*tex2D(dual_tex, x, y).y;

            if(x > 0)
                div_D_dual += -Dx[0]*tex2D(dual_tex, x-1, y).x;

            if(y > 0)
                div_D_dual += -Dy[3]*tex2D(dual_tex, x, y-1).y;

            float tau = tex2D(primal_step_tex, x, y);
            primal_row[x].u = (tex2D(old_primal_tex, x, y) + tau*div_D_dual + tau*aux_theta*tex2D(aux_tex, x, y)) / (1.0f + tau*aux_theta);

//            if(primal_row[x].u > 1.0)
//                primal_row[x].u = 1.0;

//            if(primal_row[x].u < 0.0)
//                primal_row[x].u = 0.0;

        }
    }
}

/*
 *  Huber-L2 head primal updating kernel
 *
 *  Remark: Diffusion tensor in float4 texture: [x z; y w].
 *
 */
__global__ void HuberROFHeadPrimalKernel(cudaPitchedPtr const head_primal,
                                        CostVolume::Params const cv_params)
{

    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    uint8_t const min_bg_thr = cv_params.min_bg_thr;
    uint8_t const max_bg_thr = cv_params.max_bg_thr;

    if(x < roi_w && y < roi_h)
    {

        uint32_t const fx = x + cv_params.roi[cv_params.ref_img].x_min;
        uint32_t const fy = y + cv_params.roi[cv_params.ref_img].y_min;

        uint8_t ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx, fy));

        HuberROF::Primal* head_primal_row = (HuberROF::Primal*)((char*)head_primal.ptr + y*head_primal.pitch);

        if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
        {
            head_primal_row[x].u = 2.0*tex2D(primal_tex, x, y) - tex2D(old_primal_tex, x, y);
        }
        else
        {
            ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx-1, fy));
            if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
                head_primal_row[x].u = 2.0*tex2D(primal_tex, x-1, y) - tex2D(old_primal_tex, x-1, y);

            ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx-1, fy-1));
            if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
                head_primal_row[x].u = 2.0*tex2D(primal_tex, x-1, y-1) - tex2D(old_primal_tex, x-1, y-1);

            ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx, fy-1));
            if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
                head_primal_row[x].u = 2.0*tex2D(primal_tex, x, y-1) - tex2D(old_primal_tex, x, y-1);

            ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx+1, fy-1));
            if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
                head_primal_row[x].u = 2.0*tex2D(primal_tex, x+1, y-1) - tex2D(old_primal_tex, x+1, y-1);

            ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx+1, fy));
            if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
                head_primal_row[x].u = 2.0*tex2D(primal_tex, x+1, y) - tex2D(old_primal_tex, x+1, y);

            ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx+1, fy+1));
            if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
                head_primal_row[x].u = 2.0*tex2D(primal_tex, x+1, y+1) - tex2D(old_primal_tex, x+1, y+1);

            ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx, fy+1));
            if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
                head_primal_row[x].u = 2.0*tex2D(primal_tex, x, y+1) - tex2D(old_primal_tex, x, y+1);

            ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx-1, fy+1));
            if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
                head_primal_row[x].u = 2.0*tex2D(primal_tex, x-1, y+1) - tex2D(old_primal_tex, x-1, y+1);

        }
    }

}

/*
 *  Cost-volume pixel-wise searching kernel
 *
 *  Remark:
 *
 */

__global__ void CostVolumePixelWiseSearch(cudaPitchedPtr const aux,
                                          float const aux_theta,
                                          float const lambda,
                                          CostVolume::Params const cv_params)
{

    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    short const num_disp  = cv_params.num_disp;

    uint8_t const min_bg_thr = cv_params.min_bg_thr;
    uint8_t const max_bg_thr = cv_params.max_bg_thr;

    if(x < roi_w && y < roi_h)
    {

        uint32_t const fx = x + cv_params.roi[cv_params.ref_img].x_min;
        uint32_t const fy = y + cv_params.roi[cv_params.ref_img].y_min;

        uint8_t const ref_gray = RGBA2Gray(tex2D(ref_img_tex, fx, fy));

        if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
        {

            HuberROF::Auxiliary* aux_row = (HuberROF::Auxiliary*)((char*)aux.ptr + y*aux.pitch);

//            float max_min_cost_diff = tex2D(max_disp_cost_tex, x, y) - tex2D(min_disp_cost_tex, x, y);

//            int lower_bound = round((tex2D(primal_tex, x, y) - sqrt(2.0*aux_theta*lambda*max_min_cost_diff))*(num_disp-1));
//            int upper_bound = round((tex2D(primal_tex, x, y) + sqrt(2.0*aux_theta*lambda*max_min_cost_diff))*(num_disp-1));

//            lower_bound = lower_bound < 0 ? 0 : lower_bound;
//            upper_bound = upper_bound > num_disp-1 ? num_disp-1 : upper_bound;

            float Eaux_min = 100, Eaux;
//            for(uint32_t z = lower_bound; z <= upper_bound; z++)
            for(uint32_t z = 0; z < num_disp; z++)
            {

                float aux_noramlised = (float)z/float(num_disp-1);

                Eaux = 0.5f*aux_theta*pow((tex2D(primal_tex, x, y) - aux_noramlised),2) + lambda*surf3Dread<float>(cost_volume_surface, x*sizeof(float), y, z);
                if(Eaux < Eaux_min)
                {
                    Eaux_min = Eaux;
                    aux_row[x].a = aux_noramlised;
                }
            }

            /* Sub-sampling using Newton step */
/*
            uint32_t z = aux_row[x].a*(num_disp-1);

            float nabla_cost;
            if(z == 0)
                nabla_cost = surf3Dread<float>(cost_volume_surface, x*sizeof(float), y, z+1) -
                        surf3Dread<float>(cost_volume_surface, x*sizeof(float), y, z);
            else if(z == num_disp-1)
                nabla_cost = surf3Dread<float>(cost_volume_surface, x*sizeof(float), y, z) -
                        surf3Dread<float>(cost_volume_surface, x*sizeof(float), y, z-1);
            else
                nabla_cost = 0.5*(surf3Dread<float>(cost_volume_surface, x*sizeof(float), y, z+1) -
                                  surf3Dread<float>(cost_volume_surface, x*sizeof(float), y, z-1));

            aux_row[x].a = tex2D(primal_tex, x, y) + aux_theta*lambda*nabla_cost;

            aux_row[x].a = aux_row[x].a > 1.0 ? 1.0 : aux_row[x].a;
            aux_row[x].a = aux_row[x].a < 0   ?   0 : aux_row[x].a;
*/

        }
    }
}

void ItrHuberROF(CUDAImage<Pitched2D,float> const& win_disp,
                 CUDAImage<Pitched2D,float> const& min_disp_cost,
                 CUDAImage<Pitched2D,float> const& max_disp_cost,
                 CUDAImage<Pitched2D,Primal>& head_primal,
                 CUDAImage<Pitched2D,Primal>& primal,
                 CUDAImage<Pitched2D,Primal>& old_primal,
                 CUDAImage<Pitched2D,Dual>& dual,
                 CUDAImage<Pitched2D,Auxiliary>& aux,
                 CUDAImage<Pitched2D,DiffuseTensor> const& diffuse_tensor,
                 CUDAImage<Pitched2D,PrimalStep> const& primal_step,
                 CUDAImage<Pitched2D,DualStep> const& dual_step,
                 CostVolume::Params const cv_params,
                 HuberROF::Params const opt_params)
{

    short roi_w = cv_params.roi_w;
    short roi_h = cv_params.roi_h;

    checkCudaErrors(cudaBindTexture2D(0, win_disp_tex, win_disp.ptr, cudaCreateChannelDesc<float>(), roi_w, roi_h, win_disp.pitch));
    checkCudaErrors(cudaBindTexture2D(0, min_disp_cost_tex, min_disp_cost.ptr, cudaCreateChannelDesc<float>(), roi_w, roi_h, min_disp_cost.pitch));
    checkCudaErrors(cudaBindTexture2D(0, max_disp_cost_tex, max_disp_cost.ptr, cudaCreateChannelDesc<float>(), roi_w, roi_h, max_disp_cost.pitch));
    checkCudaErrors(cudaBindTexture2D(0, head_primal_tex, head_primal.ptr, cudaCreateChannelDesc<float>(), roi_w, roi_h, head_primal.pitch));
    checkCudaErrors(cudaBindTexture2D(0, primal_tex, primal.ptr, cudaCreateChannelDesc<float>(), roi_w, roi_h, primal.pitch));
    checkCudaErrors(cudaBindTexture2D(0, old_primal_tex, old_primal.ptr, cudaCreateChannelDesc<float>(), roi_w, roi_h, old_primal.pitch));
    checkCudaErrors(cudaBindTexture2D(0, dual_tex, dual.ptr, cudaCreateChannelDesc<float2>(), roi_w, roi_h, dual.pitch));
    checkCudaErrors(cudaBindTexture2D(0, aux_tex, aux.ptr, cudaCreateChannelDesc<float>(), roi_w, roi_h, aux.pitch));
    checkCudaErrors(cudaBindTexture2D(0, primal_step_tex, primal_step.ptr, cudaCreateChannelDesc<float>(), roi_w, roi_h, primal_step.pitch));
    checkCudaErrors(cudaBindTexture2D(0, dual_step_tex, dual_step.ptr, cudaCreateChannelDesc<float2>(), roi_w, roi_h, dual_step.pitch));
    checkCudaErrors(cudaBindTexture2D(0, diffuse_tensor_tex, diffuse_tensor.ptr, cudaCreateChannelDesc<float4>(), roi_w, roi_h, diffuse_tensor.pitch));

    /* Init primal variables */
    primal.CopyFrom(win_disp);
    head_primal.CopyFrom(win_disp);

    /* Init dual variables */
    dual.SetValue(0);

    /* Init auxiliary variable */
    aux.CopyFrom(win_disp);

    float _aux_theta = 0;

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((roi_w+dimBlock.x-1)/dimBlock.x, (roi_h+dimBlock.y-1)/dimBlock.y);

//    float theta_w = float(cv_params.max_disp)/float(opt_params.lambda);

    for(uint32_t itr_n = 0; itr_n < opt_params.num_itr; itr_n++)
    {

        checkCudaErrors(cudaMemcpy2D(old_primal.ptr, old_primal.pitch, primal.ptr, primal.pitch, primal.w*sizeof(Primal), primal.h, cudaMemcpyDeviceToDevice));

        for(int in_n = 0; in_n < 1; ++in_n)
        {
            /* Dual update */
            HuberROFDualKernel<<<dimGrid, dimBlock>>>(dual.GetCUDAPitchedPtr(), opt_params.epsilon, cv_params);

            /* Primal update */
            HuberROFPrimalKernel<<<dimGrid, dimBlock>>>(primal.GetCUDAPitchedPtr(), _aux_theta, cv_params);

            /* Head primal update */
            HuberROFHeadPrimalKernel<<<dimGrid, dimBlock>>>(head_primal.GetCUDAPitchedPtr(), cv_params);
        }

        /* Pixel-wise line search in cost-volume */
        CostVolumePixelWiseSearch<<<dimGrid, dimBlock>>>(aux.GetCUDAPitchedPtr(), _aux_theta, opt_params.lambda, cv_params);

        /* Smoothstep function */
        float x = (float)itr_n/(float)opt_params.num_itr;
//        _aux_theta = x*x*x*(x*(x*6.0f - 15.0f) + 10.0f)*float(opt_params.lambda);
        _aux_theta = (3.0f*x*x - 2.0f*x*x*x)*2.0*float(opt_params.lambda);
//        printf("%f\n", _aux_theta*float(opt_params.lambda));

    }

    checkCudaErrors(cudaUnbindTexture(win_disp_tex));
    checkCudaErrors(cudaUnbindTexture(min_disp_cost_tex));
    checkCudaErrors(cudaUnbindTexture(max_disp_cost_tex));
    checkCudaErrors(cudaUnbindTexture(head_primal_tex));
    checkCudaErrors(cudaUnbindTexture(primal_tex));
    checkCudaErrors(cudaUnbindTexture(old_primal_tex));
    checkCudaErrors(cudaUnbindTexture(dual_tex));
    checkCudaErrors(cudaUnbindTexture(aux_tex));
    checkCudaErrors(cudaUnbindTexture(primal_step_tex));
    checkCudaErrors(cudaUnbindTexture(dual_step_tex));
    checkCudaErrors(cudaUnbindTexture(diffuse_tensor_tex));


}

} // namespace HuberL1TGV1 {


/*
 *  Bind image textures
 *
 *  Remark: Reference image can be left or right camera image, so the target image
 */
extern "C" void BindTexturesCUDA(CUDAImage<OpenGL2D,uchar4> st_frames[2],
                                 RefImage const ref_img)
{

    st_frames[0].MapCUDARes();
    st_frames[1].MapCUDARes();

    /* Bind to read-only textures */
    if(ref_img == LeftRefImage)
    {
        checkCudaErrors(cudaBindTextureToArray(ref_img_tex, st_frames[0].GetArrayPtr()));
        checkCudaErrors(cudaBindTextureToArray(target_img_tex, st_frames[1].GetArrayPtr()));
    }
    else if(ref_img == RightRefImage)
    {
        checkCudaErrors(cudaBindTextureToArray(ref_img_tex, st_frames[1].GetArrayPtr()));
        checkCudaErrors(cudaBindTextureToArray(target_img_tex, st_frames[0].GetArrayPtr()));
    }

    st_frames[0].UnmapCUDARes();
    st_frames[1].UnmapCUDARes();

}

/*
 *  Unbind image textures
 *
 *  Remark: Reference image can be left or right camera image, so the target image
 */
extern "C" void UnbindTexturesCUDA()
{
    checkCudaErrors(cudaUnbindTexture(ref_img_tex));
    checkCudaErrors(cudaUnbindTexture(target_img_tex));
}

/*
 *  Convert normalised disparity [0, 1] to real disparity [min_disp, max_disp]
 *
 */
__global__ void CopyNormDisp2RealDispKernel(cudaPitchedPtr const real_disp,
                                            CostVolume::Params const cv_params)
{
    uint32_t const x = blockDim.x * blockIdx.x + threadIdx.x;
    uint32_t const y = blockDim.y * blockIdx.y + threadIdx.y;

    size_t const min_disp = cv_params.min_disp;
    size_t const num_disp = cv_params.num_disp;

    if (x < cv_params.frame_w && y < cv_params.frame_h)
    {
        float* real_disp_row = (float*)((char*)real_disp.ptr + y*real_disp.pitch);

        if(x >= cv_params.roi[cv_params.ref_img].x_min && x <= cv_params.roi[cv_params.ref_img].x_max &&
           y >= cv_params.roi[cv_params.ref_img].y_min && y <= cv_params.roi[cv_params.ref_img].y_max)
        {

            short const roi_x = x - cv_params.roi[cv_params.ref_img].x_min;
            short const roi_y = y - cv_params.roi[cv_params.ref_img].y_min;

            real_disp_row[x] = min_disp + tex2D(primal_tex, roi_x, roi_y)*(num_disp-1);

        }
        else
            real_disp_row[x] = 0;
    }
}

extern "C" void CopyNormDisp2RealDispCUDA(CUDAImage<Pitched2D,float>& real_disp,
                                          CostVolume::Data const& cv_data,
                                          HuberROF::Data const& opt_data)
{

    checkCudaErrors(cudaBindTexture2D(0, primal_tex, opt_data.primal.ptr,
                                      cudaCreateChannelDesc<float>(),
                                      opt_data.primal.w, opt_data.primal.h, opt_data.primal.pitch));

    size_t const frame_w = real_disp.GetWidth();
    size_t const frame_h = real_disp.GetHeight();

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((frame_w+dimBlock.x-1)/dimBlock.x,
                        (frame_h+dimBlock.y-1)/dimBlock.y);

    CopyNormDisp2RealDispKernel<<<dimGrid, dimBlock>>>(real_disp.GetCUDAPitchedPtr(),
                                                       cv_data.GetParams());

    checkCudaErrors(cudaUnbindTexture(primal_tex));

}

/*
 *  Copy disparity to OpenGL RGBA texture
 *
 *  Remark: The disparity value is normalised to [0,1]
 */
__global__ void CopyNormDisp2OpenGLTextureKernel(CostVolume::Params const cv_params)
{
    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    if (x < cv_params.frame_w && y < cv_params.frame_h)
    {

        if(x >= cv_params.roi[cv_params.ref_img].x_min && x <= cv_params.roi[cv_params.ref_img].x_max &&
           y >= cv_params.roi[cv_params.ref_img].y_min && y <= cv_params.roi[cv_params.ref_img].y_max)
        {

            short const roi_x = x - cv_params.roi[cv_params.ref_img].x_min;
            short const roi_y = y - cv_params.roi[cv_params.ref_img].y_min;

            float disp = tex2D(primal_tex, roi_x, roi_y);
            uint8_t disp_gray = disp*255;

            uchar4 data = make_uchar4(disp_gray, disp_gray, disp_gray, 255);
            surf2Dwrite(data, disp_surface, x*4, y);
        }
        else
        {
            uchar4 data = make_uchar4(0, 0, 0, 255);
            surf2Dwrite(data, disp_surface, x*4, y);
        }

    }
}

extern "C" void CopyNormDisp2OpenGLTextureCUDA(CUDAImage<OpenGL2D,uchar4>& gl_img,
                                               CostVolume::Data const& cv_data,
                                               HuberROF::Data const& opt_data)
{

    checkCudaErrors(cudaBindTexture2D(0, primal_tex, opt_data.primal.ptr,
                                      cudaCreateChannelDesc<float>(),
                                      opt_data.primal.w, opt_data.primal.h, opt_data.primal.pitch));

    gl_img.MapCUDARes();

    checkCudaErrors(cudaBindSurfaceToArray(disp_surface, gl_img.GetArrayPtr()));

    short const frame_w = gl_img.GetWidth();
    short const frame_h = gl_img.GetHeight();

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((frame_w+dimBlock.x-1)/dimBlock.x, (frame_h+dimBlock.y-1)/dimBlock.y);
    CopyNormDisp2OpenGLTextureKernel<<<dimGrid, dimBlock>>>(cv_data.GetParams());

    checkCudaErrors(cudaUnbindTexture(primal_tex));

    gl_img.UnmapCUDARes();

}

/*
 *  Back-project point to 3D from the disparity map
 *
 */
__global__ void Disp2PtsCloudKernel(float3 *const pts,
                                    uchar4 *const pts_colour,
                                    float const baseline,
                                    CostVolume::Params const cv_params)
{
    short const x = blockDim.x * blockIdx.x + threadIdx.x;
    short const y = blockDim.y * blockIdx.y + threadIdx.y;

    short const roi_w = cv_params.roi_w;
    short const roi_h = cv_params.roi_h;

    short const min_disp = cv_params.min_disp;
    short const num_disp = cv_params.num_disp;    

    if (x < roi_w && y < roi_h)
    {

        size_t const fx = x + cv_params.roi[cv_params.ref_img].x_min;
        size_t const fy = y + cv_params.roi[cv_params.ref_img].y_min;

        float fu = const_dev_K[0];
        float fv = const_dev_K[4];
        float u0 = const_dev_K[6];
        float v0 = const_dev_K[7];

        float d = min_disp + tex2D(primal_tex, x, y)*(num_disp-1);
        float z = d == min_disp ? 1e32 : baseline*fu/d;

        float pts_3d[3];
        pts_3d[0] = ((float(fx) - u0)/fu) * z;
        pts_3d[1] = ((float(fy) - v0)/fv) * z;
        pts_3d[2] = z;

        float T[12];
        LieSE3from4x4(T, const_dev_T);
        LieApplySE3vec((float*)&pts[y*roi_w+x], T, pts_3d);

        pts_colour[y*roi_w+x] = tex2D(ref_img_tex, fx, fy);


    }
}

extern "C" void DispMap2PtsCloudCUDA(CUDAImage<OpenGL2D,uchar4>& frame,
                                     CUDAData<OpenGLBufferType,float3>& pts,
                                     CUDAData<OpenGLBufferType,uchar4>& pts_colour,
                                     CostVolume::Data const& cv_data,
                                     HuberROF::Data const& opt_data,
                                     float const baseline,
                                     float const K[9],
                                     float const T[16])
{
    short const roi_w = cv_data.GetParams().roi_w;
    short const roi_h = cv_data.GetParams().roi_h;

    frame.MapCUDARes();
    checkCudaErrors(cudaBindTextureToArray(ref_img_tex, frame.GetArrayPtr()));
    frame.UnmapCUDARes();

    pts.MapCUDARes();
    pts_colour.MapCUDARes();

    checkCudaErrors(cudaBindTexture2D(0, primal_tex, opt_data.primal.ptr,
                                      cudaCreateChannelDesc<float>(),
                                      opt_data.primal.w, opt_data.primal.h, opt_data.primal.pitch));

    checkCudaErrors(cudaMemcpyToSymbol(const_dev_K, K, 9*sizeof(float)));
    checkCudaErrors(cudaMemcpyToSymbol(const_dev_T, T, 16*sizeof(float)));

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((roi_w+dimBlock.x-1)/dimBlock.x, (roi_h+dimBlock.y-1)/dimBlock.y);

    Disp2PtsCloudKernel<<<dimGrid, dimBlock>>>(pts.GetPtr(),
                                               pts_colour.GetPtr(),
                                               baseline,
                                               cv_data.GetParams());

    pts.UnmapCUDARes();
    pts_colour.UnmapCUDARes();

    checkCudaErrors(cudaUnbindTexture(ref_img_tex));
    checkCudaErrors(cudaUnbindTexture(primal_tex));


}





//__global__ void LeftAndRightConsistencyCheckKernel(cudaPitchedPtr const d_const,
//                                                   uint16_t const width,
//                                                   uint16_t const height,
//                                                   uint16_t const min_disp,
//                                                   uint16_t const max_disp)
//{

//    uint32_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {

//        char* d_const_ptr = (char*)d_const.ptr;
//        uint32_t d_const_pitch = d_const.pitch;
//        char* d_const_slice = d_const_ptr;
//        float* d_const_row = (float*)(d_const_slice + y * d_const_pitch);

//        uint16_t num_disp = max_disp - min_disp + 1;

//        /* Left to right check */
//        d_const_row[x] = 0.0;
//        if(abs(tex2D(d_tex, x, y) - tex2D(a_tex, x-round(tex2D(d_tex, x, y)*num_disp+min_disp), y)) < 0.15)
//            d_const_row[x] = tex2D(d_tex, x, y);

//    }

//}

//__global__ void UpdateDisparityPointCloudKernel(float3 *const pts_cloud,
//                                                uint16_t const width,
//                                                uint16_t const height,
//                                                uint16_t const min_disp,
//                                                uint16_t const max_disp,
//                                                uint8_t const camera_idx)
//{

//    uint32_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {

//        float num_disp = max_disp - min_disp + 1;

//        float n_x = float(x+0.5f)/width;
//        float n_y = float(y+0.5f)/height;

//        if(RGBA2FloatGray(tex2D(ref_rgba_tex, n_x, n_y)) < const_background_threshold ||
//           /*tex2D(d_tex, x, y) < 0.1 || */x < 95 || x > width || y < 15 || y > height-15)
//        {
//            /* 0 disparity */
//            pts_cloud[y*width+x].x = 0.0;
//            pts_cloud[y*width+x].y = 0.0;
//            pts_cloud[y*width+x].z = 0.0;
//        }
//        else
//        {
//            if(camera_idx == 0 || camera_idx == 2)
//            {
//                float disp_pt[4] = {x, y, tex2D(d_tex, x, y)*(num_disp-1)+min_disp, 1.0};

//                float pt[4];
//                MatMul<4, 4, 1, float>(pt, const_left_disp_3d_matrix, disp_pt);

//                pts_cloud[y*width+x].x = pt[0]/pt[3];
//                pts_cloud[y*width+x].y = pt[1]/pt[3];
//                pts_cloud[y*width+x].z = pt[2]/pt[3];
//            }
//            else if(camera_idx == 1)
//            {

//                float disp_pt[4] = {x, y, tex2D(d_tex, x, y)*(num_disp-1)+min_disp, 1.0};

//                float pt[4];
//                MatMul<4, 4, 1, float>(pt, const_right_disp_3d_matrix, disp_pt);

//                float SE3[12], inv_SE3[12], inv_stereo_extrinsic[16];
//                LieSE3from4x4<float>(SE3, const_stereo_extrinsic);
//                LieInverseSE3<float>(inv_SE3, SE3);
//                LiePutSE3in4x4<float>(inv_stereo_extrinsic, inv_SE3);

//                float pt_t[4];
//                MatMul<4, 4, 1, float>(pt_t, inv_stereo_extrinsic, pt);

//                pts_cloud[y*width+x].x = pt_t[0]/pt_t[3];
//                pts_cloud[y*width+x].y = pt_t[1]/pt_t[3];
//                pts_cloud[y*width+x].z = pt_t[2]/pt_t[3];

//            }
//        }
//    }

//}

//__global__ void UpdateGroundTruthDisparityPointCloudKernel(float3 *const pts_cloud,
//                                                           uint16_t const width,
//                                                           uint16_t const height)
//{

//    uint32_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {

//        float n_x = float(x+0.5f)/width;
//        float n_y = float(y+0.5f)/height;

//        if(RGBA2FloatGray(tex2D(ref_rgba_tex, n_x, n_y)) < const_background_threshold ||
//           /*tex2D(d_tex, x, y) < 0.1 || */x < 15 || x > width-15 || y < 15 || y > height-15)
//        {
//            /* 0 disparity */
//            pts_cloud[y*width+x].x = 0.0;
//            pts_cloud[y*width+x].y = 0.0;
//            pts_cloud[y*width+x].z = 0.0;
//        }
//        else
//        {
//            float disp_pt[4] = {x, y, tex2D(gt_disp_tex, x, y), 1.0};

//            float pt[4];
//            MatMul<4, 4, 1, float>(pt, const_left_disp_3d_matrix, disp_pt);

//            pts_cloud[y*width+x].x = pt[0]/pt[3];
//            pts_cloud[y*width+x].y = pt[1]/pt[3];
//            pts_cloud[y*width+x].z = pt[2]/pt[3];

//        }
//    }

//}

//__global__ void DepthAbsoluteDifferenceErrorKernel(float3 const *const pts_cloud,
//                                                   float3 const *const gt_pts_cloud,
//                                                   float *const error,
//                                                   bool *const valid,
//                                                   uint32_t const width,
//                                                   uint32_t const height)
//{

//    uint32_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {

//        if(gt_pts_cloud[x+y*width].z > 0 && pts_cloud[x+y*width].z > 0)
//        {
//            error[x+y*width] = fabsf(pts_cloud[x+y*width].z - gt_pts_cloud[x+y*width].z);
//            valid[x+y*width] = true;
//        }
//        else
//        {
//            error[x+y*width] = 0.0f;
//            valid[x+y*width] = false;
//        }

//    }

//}

//__global__ void ExtractDepthImage(cudaPitchedPtr const volume_ptr,
//                                  cudaPitchedPtr const inv_d,
//                                  uint32_t const width,
//                                  uint32_t const height,
//                                  uint32_t const depth)
//{

//    uint32_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {

//        char* depthPtr = (char*)inv_d.ptr;
//        char* volumePtr = (char*)volume_ptr.ptr;

//        uint32_t depth_pitch = inv_d.pitch;
//        uint32_t volume_pitch = volume_ptr.pitch;

//        char* depth_slice = depthPtr;

//        uint32_t volumeSlicePitch = volume_pitch * height;
//        char* volume_slice = volumePtr + depth * volumeSlicePitch;

//        float* depth_row = (float*)(depth_slice + y * depth_pitch);
//        float* volume_row = (float*)(volume_slice + y * volume_pitch);

//        depth_row[x] = volume_row[x];

//    }

//}

//__global__ void CopyDisparity2GLTextureKernel(uint16_t const width,
//                                              uint16_t const height,
//                                              uint16_t const min_disp,
//                                              uint16_t const max_disp)
//{
//    uint16_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint16_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {
//        float disparity = tex2D(d_tex, x, y);
//        uint8_t depth_gray;

//        float n_x = float(x+0.5f)/width;
//        float n_y = float(y+0.5f)/height;

//        if(x < 15 || x > width-15 || y < 15 || y > height-15)
//            depth_gray = 0;
//        else if(RGBA2FloatGray(tex2D(ref_rgba_tex, n_x, n_y)) < const_background_threshold)
//            depth_gray = 0;
//        else
////            depth_gray = (disparity*(max_disp-min_disp+1)+min_disp)*(255.0/max_disp);
//            depth_gray = disparity*255;

//        uchar4 data = make_uchar4(depth_gray, depth_gray, depth_gray, 255);
//        surf2Dwrite(data, write_surface, x*4, y);

//    }
//}

//__global__ void CopyGroundTruthDisparity2GLTextureKernel(uint16_t const width,
//                                                         uint16_t const height,
//                                                         float const gt_min_disp,
//                                                         float const gt_max_disp)
//{
//    uint16_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint16_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {

//        //        printf("%f\n", gt_max_disp);

//        uint8_t depth_gray;

//        float n_x = float(x+0.5f)/width;
//        float n_y = float(y+0.5f)/height;

//        if(x < 15 || x > width-15 || y < 15 || y > height-15)
//            depth_gray = 0;
//        else if(RGBA2FloatGray(tex2D(ref_rgba_tex, n_x, n_y)) < const_background_threshold)
//            depth_gray = 0;
//        else
//            depth_gray = ((tex2D(gt_disp_tex, x, y)-gt_min_disp)/(gt_max_disp-gt_min_disp))*255;

//        uchar4 data = make_uchar4(depth_gray, depth_gray, depth_gray, 255);
//        surf2Dwrite(data, write_surface, x*4, y);

//    }
//}

//__global__ void CopyEdgeImage2GLTextureKernel(uint16_t const width,
//                                              uint16_t const height,
//                                              uint8_t const camera_idx)
//{
//    uint32_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {

//        uint8_t gray;
//        surf2Dread(&gray, camera_idx == 1 ? right_edge_gray8_surface : left_edge_gray8_surface, x, y);

//        uchar4 data = make_uchar4(gray, gray, gray, 255);
//        surf2Dwrite(data, write_surface, x*4, y);

//    }
//}


/////////////////////////////////////////////////
//  Codes for special projection epipolar geometry
/////////////////////////////////////////////////

//__global__ void InitSpaceCostVolumeKernel(cudaPitchedPtr const volume_pitched_ptr,
//                                          uint32_t const width,
//                                          uint32_t const height,
//                                          uint32_t const depth_resolution)
//{
//    uint32_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t y = blockDim.y * blockIdx.y + threadIdx.y;
//    uint32_t z = blockDim.z * blockIdx.z + threadIdx.z;

//    if (x < width && y < height && z < depth_resolution)
//    {

//        char* volume_ptr = (char*)volume_pitched_ptr.ptr;
//        uint32_t volume_pitch = volume_pitched_ptr.pitch;
//        uint32_t volume_slicePitch = volume_pitch * height;
//        char* volume_slice = volume_ptr + z * volume_slicePitch;
//        float* volume_row = (float*)(volume_slice + y * volume_pitch);

//        // Fill up every cell with 1.0
//        volume_row[x] = FLOAT_MAX;
//    }
//}

//__global__ void PhotometricDirectProjectKernel(cudaPitchedPtr const volume_pitched_ptr,
//                                               float const alpha,
//                                               float const tau1,
//                                               float const tau2,
//                                               IMAGE_TYPE const img_type,
//                                               uint32_t const width,
//                                               uint32_t const height,
//                                               uint32_t const depth_resolution)
//{

//    uint32_t const x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t const y = blockDim.y * blockIdx.y + threadIdx.y;
//    uint32_t const z = blockDim.z * blockIdx.z + threadIdx.z;

//    if(x < width && y < height && z < depth_resolution)
//    {

//        float ref_fu = const_ref_cam_params[0];
//        float ref_fv = const_ref_cam_params[1];
//        float ref_u0 = const_ref_cam_params[2];
//        float ref_v0 = const_ref_cam_params[3];

//        float fu = const_cam_params[0];
//        float fv = const_cam_params[1];
//        float u0 = const_cam_params[2];
//        float v0 = const_cam_params[3];

//        float pt[3];
//        pt[0] = ((float(x)-ref_u0)/ref_fu) / const_inv_depth_LUT[z];
//        pt[1] = ((float(y)-ref_v0)/ref_fv) / const_inv_depth_LUT[z];
//        pt[2] = 1.0/const_inv_depth_LUT[z];

//        float proj_pt[3];

//        float SE3[12];
//        LieSE3from4x4<float>(SE3, const_camera_pose);
//        LieApplySE3vec<float>(proj_pt, SE3, pt);

//        proj_pt[0] /= proj_pt[2];
//        proj_pt[1] /= proj_pt[2];

//        proj_pt[0] = proj_pt[0]*fu + u0;
//        proj_pt[1] = proj_pt[1]*fv + v0;


////        if(x == 5 && y == 5 && z == 0)
////            printf("begin: %f %f\n", proj_pt[0], proj_pt[1]);

////        if(x == 5 && y == 5 && z == depth_resolution-1)
////            printf("end: %f %f\n", proj_pt[0], proj_pt[1]);

//        char* devPtr = (char*)volume_pitched_ptr.ptr;
//        uint32_t pitch = volume_pitched_ptr.pitch;
//        uint32_t slicePitch = pitch * height;

//        char* slice = devPtr + z * slicePitch;
//        float* volume_row = (float*)(slice + y * pitch);

//        volume_row[x] = 1.0;

////        if(x == 5 && y == 5)
////            printf("z: %d, %f\n", z, volume_row[x]);

//        if(proj_pt[0] < 0 || proj_pt[0] >= width || proj_pt[1] < 0 || proj_pt[1] >= height)
//            return;
//        else
//        {


//            float cost;
////            float d_cost;

//            if(img_type == RGBA_IMAGE)
//            {
//                cost = abs(RGBColorDiff(tex2D(ref_rgba_tex, x+0.5, y+0.5), tex2D(live_rgba_tex, proj_pt[0], proj_pt[1])));

////                float dx_ref_img = (RGBColorDiff(tex2D(ref_rgba_tex, x+1.5, y+0.5), tex2D(live_rgba_tex, x-0.5, y+0.5)))*0.5;
////                float dy_ref_img = (RGBColorDiff(tex2D(ref_rgba_tex, x+0.5, y+1.5), tex2D(live_rgba_tex, x+0.5, y-0.5)))*0.5;
////                float d_ref_img = sqrt(dx_ref_img*dx_ref_img + dy_ref_img*dy_ref_img);

////                float dx_proj_img = (RGBColorDiff(tex2D(ref_rgba_tex, proj_pt[0]+1.0f, proj_pt[1]), tex2D(live_rgba_tex, proj_pt[0]-1.0f, proj_pt[1])))*0.5;
////                float dy_proj_img = (RGBColorDiff(tex2D(ref_rgba_tex, proj_pt[0], proj_pt[1]+1.0f), tex2D(live_rgba_tex, proj_pt[0], proj_pt[1]-1.0f)))*0.5;
////                float d_proj_img = sqrt(dx_proj_img*dx_proj_img + dy_proj_img*dy_proj_img);

////                d_cost = abs(d_ref_img - d_proj_img);

//            }
//            else
//            {
//                cost = abs(tex2D(ref_gray_tex, x+0.5, y+0.5) - tex2D(live_gray_tex, proj_pt[0], proj_pt[1]));

////                float dx_ref_img = (tex2D(ref_gray_tex, x+1.5, y+0.5) - tex2D(live_gray_tex, x-0.5, y+0.5))*0.5;
////                float dy_ref_img = (tex2D(ref_gray_tex, x+0.5, y+1.5) - tex2D(live_gray_tex, x+0.5, y-0.5))*0.5;
////                float d_ref_img = sqrt(dx_ref_img*dx_ref_img + dy_ref_img*dy_ref_img);

////                float dx_proj_img = (tex2D(ref_gray_tex, proj_pt[0]+1.0f, proj_pt[1]) - tex2D(live_gray_tex, proj_pt[0]-1.0f, proj_pt[1]))*0.5;
////                float dy_proj_img = (tex2D(ref_gray_tex, proj_pt[0], proj_pt[1]+1.0f) - tex2D(live_gray_tex, proj_pt[0], proj_pt[1]-1.0f))*0.5;

////                float d_proj_img = sqrt(dx_proj_img*dx_proj_img + dy_proj_img*dy_proj_img);

////                d_cost = abs(d_ref_img - d_proj_img);
//            }

////            float illum_cost = (1.0f-alpha)*min(cost, tau1) + alpha*min(d_cost, tau2);
//            volume_row[x] = cost;

////            if(x == 274 && y == 135)
////                printf("%f %f\n", 1.0f/const_inv_depth_LUT[z], volume_row[x]);

////            volume_row[x] = tex2D(img_tex, proj_pt[0], proj_pt[1]);

//        }
//    }
//}


//__global__ void FilterBadPixel(cudaPitchedPtr const d,
//                               IMAGE_TYPE const img_type,
//                               uint32_t const width,
//                               uint32_t const height,
//                               uint32_t const max_depth)
//{

//    uint32_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {
//        char* d_ptr = (char*)d.ptr;
//        uint32_t d_pitch = d.pitch;
//        char* d_slice = d_ptr;
//        float* d_row = (float*)(d_slice + y * d_pitch);


//        if(img_type == RGBA_IMAGE)
//        {
//            float4 colour = tex2D(ref_rgba_tex, x, y);
//            float intensity = sqrt(colour.x*colour.x+colour.y*colour.y*+colour.z*colour.z);

//            if(intensity < 0.1)
//                d_row[x] =const_inv_depth_LUT[max_depth-1];

//        }
//        else if(img_type == GRAY_IMAGE)
//        {
//            float gray = tex2D(ref_gray_tex, x, y);

//            if(gray < 0.05)
//                d_row[x] = const_inv_depth_LUT[max_depth-1];
//        }

//    }
//}

/////////////////////////////////////////////////
//  TV-L1 optimisation scheme
/////////////////////////////////////////////////
//__global__ void UpdateDualTVL1Kernel(cudaPitchedPtr const q_dx,
//                                     cudaPitchedPtr const q_dy,
//                                     float const beta,
//                                     float const sigma,
//                                     uint32_t const width,
//                                     uint32_t const height)
//{

//    uint32_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {

//        char* q_dx_ptr = (char*)q_dx.ptr;
//        uint32_t q_dx_pitch = q_dx.pitch;
//        char* q_dx_slice = q_dx_ptr;
//        float* q_dx_row = (float*)(q_dx_slice + y * q_dx_pitch);

//        char* q_dy_ptr = (char*)q_dy.ptr;
//        uint32_t q_dy_pitch = q_dy.pitch;
//        char* q_dy_slice = q_dy_ptr;
//        float* q_dy_row = (float*)(q_dy_slice + y * q_dy_pitch);

//        uint8_t edge_gray;
//        surf2Dread(&edge_gray, edge_gray8_surface, x, y);
//        float edge_weight = exp(-beta*float(edge_gray)/255.0f);

//        float inv_d_dx = 0.0f;
//        float inv_d_dy = 0.0f;

//        if(x == 0 || x == width-1)
//        {
//            if(y != 0 && y != height-1)
//                inv_d_dy = (tex2D(head_d_tex, x, y+1) - tex2D(head_d_tex, x, y-1))*0.5f;
//        }
//        else if(y == 0 || y == height-1)
//        {
//            if(x != 0 && x != width-1)
//                inv_d_dx = (tex2D(head_d_tex, x+1, y) - tex2D(head_d_tex, x-1, y))*0.5f;
//        }
//        else
//        {
//            inv_d_dx = (tex2D(head_d_tex, x+1, y) - tex2D(head_d_tex, x-1, y))*0.5f;
//            inv_d_dy = (tex2D(head_d_tex, x, y+1) - tex2D(head_d_tex, x, y-1))*0.5f;
//        }

//        q_dx_row[x] = (q_dx_row[x] + inv_d_dx*edge_weight*sigma);
//        q_dy_row[x] = (q_dy_row[x] + inv_d_dy*edge_weight*sigma);

//        // constrain ||q|| <= 1.0
//        float q_norm = sqrt(q_dx_row[x]*q_dx_row[x] + q_dy_row[x]*q_dy_row[x]);
//        if(q_norm > 1.0)
//        {
//            q_dx_row[x] /= q_norm;
//            q_dy_row[x] /= q_norm;
//        }

//    }

//}

//__global__ void UpdatePrimalTVL1Kernel(cudaPitchedPtr const d,
//                                       float const beta,
//                                       float const tau,
//                                       float const lambda,
//                                       uint32_t const width,
//                                       uint32_t const height)
//{

//    uint32_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {

//        char* d_ptr = (char*)d.ptr;
//        uint32_t d_pitch = d.pitch;
//        char* d_slice = d_ptr;
//        float* d_row = (float*)(d_slice + y * d_pitch);

//        uint8_t edge_gray;
//        surf2Dread(&edge_gray, edge_gray8_surface, x, y);
//        float edge_weight = exp(-beta*float(edge_gray)/255.0f);

//        float div_q = 0.0f;

//        if(x == 0 || x == width-1)
//        {
//            if(y != 0 && y != height-1)
//                div_q = (tex2D(q_dy_tex, x, y+1) - tex2D(q_dy_tex, x, y-1))*0.5;
//        }
//        else if(y == 0 || y == height-1)
//        {
//            if(x != 0 && x != width-1)
//                div_q = (tex2D(q_dx_tex, x+1, y) - tex2D(q_dx_tex, x-1, y))*0.5;
//        }
//        else
//            div_q = (tex2D(q_dx_tex, x+1, y) - tex2D(q_dx_tex, x-1, y))*0.5 +
//                    (tex2D(q_dy_tex, x, y+1) - tex2D(q_dy_tex, x, y-1))*0.5;

//        float tmp_d = tex2D(old_d_tex, x, y) + tau*div_q*edge_weight;

//        float ori_d = tex2D(ori_d_tex, x, y);
//        float cond_d = tmp_d - ori_d;

//        if(cond_d > tau*lambda)
//            d_row[x] = tmp_d - tau*lambda;
//        else if(cond_d < tau*lambda)
//            d_row[x] = tmp_d + tau*lambda;
//        else
//            d_row[x] = ori_d;

//    }
//}

//__global__ void UpdateHeadDTVL1Kernel(cudaPitchedPtr const head_d,
//                                      float const theta,
//                                      uint32_t const width,
//                                      uint32_t const height)
//{

//    uint32_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {

//        char* head_d_ptr = (char*)head_d.ptr;
//        uint32_t head_d_pitch = head_d.pitch;
//        char* head_d_slice = head_d_ptr;
//        float* head_d_row = (float*)(head_d_slice + y * head_d_pitch);

//        head_d_row[x] = tex2D(d_tex, x, y) + theta*(tex2D(d_tex, x, y) - tex2D(old_d_tex, x, y));

//    }

//}

//__global__ void UpdateSpacePointCloudKernel(float3 *const pts_cloud,
//                                            uint16_t const width,
//                                            uint16_t const height)
//{

//    uint32_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {

//        float ref_fu = const_left_cam_intrinsic[0];
//        float ref_fv = const_left_cam_intrinsic[1];
//        float ref_u0 = const_left_cam_intrinsic[2];
//        float ref_v0 = const_left_cam_intrinsic[3];

//        float inv_d = tex2D(d_tex, x, y);

//        float pt[3];
//        pt[0] = ((float(x)-ref_u0)/ref_fu) / inv_d;
//        pt[1] = ((float(y)-ref_v0)/ref_fv) / inv_d;
//        pt[2] = 1.0/inv_d;

//        pts_cloud[y*width+x].x = pt[0];
//        pts_cloud[y*width+x].y = pt[1];
//        pts_cloud[y*width+x].z = pt[2];
//    }

//}

//__global__ void CopyInvDepth2GLTextureKernel(uint16_t width,
//                                          uint16_t height,
//                                          uint16_t depth_resolution)
//{
//    uint16_t x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint16_t y = blockDim.y * blockIdx.y + threadIdx.y;

//    if (x < width && y < height)
//    {
//        float depth = tex2D(d_tex, x, y);
//        float base = const_inv_depth_LUT[0];
//        float scale = const_inv_depth_LUT[depth_resolution-1] - const_inv_depth_LUT[0];

//        uint8_t depth_gray = 255-(depth - base)/scale*255.0f;
////                uint8_t depth_gray = tex2D(d_tex, x, y)*255;

//        uchar4 data = make_uchar4(depth_gray, depth_gray, depth_gray, 255);
//        surf2Dwrite(data, gl_rgba8_surface, x*4, y);

//    }
//}

} // namespace falcon {

#ifndef FALCON_CUDA_IMG_H
#define FALCON_CUDA_IMG_H

#include <assert.h>
#include <typeinfo>
#include <cstring>
#include <string>

#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

#include "cuda_const.h"

namespace falcon {

using namespace std;

//! @brief RGBA (uchar4) to gray converter (unsigned char)
__device__ __host__
static unsigned char RGBA2Gray(uchar4 const rgba)
{
    return (unsigned char)(rgba.x*0.2989f+rgba.y*0.5867f+rgba.z*0.1144f);
}

//! @brief RGBA (uchar4) to gray converter (float [0,1])
__device__ __host__
static float RGBA2FloatGray(uchar4 const rgba)
{
    return (rgba.x*0.2989f+rgba.y*0.5867f+rgba.z*0.1144f)/255.0;
}

//! @brief RGBA (float4) to gray converter (float [0,1])
__device__ __host__
static float FloatRGBA2FloatGray(float4 const rgba)
{
    return rgba.x*0.2989f + rgba.y*0.5867f + rgba.z*0.1144f;
}

struct ROI
{
    /* Must have an empty constructor for CUDA device constant memory */
    ROI() {}

    ROI(size_t const x_min, size_t const x_max, size_t const y_min, size_t const y_max)
        :x_min(x_min), x_max(x_max), y_min(y_min), y_max(y_max)
    {        
    }

    bool operator!=(ROI const& rhs) const
    {
        return (x_min != rhs.x_min) ||
               (x_max != rhs.x_max) ||
               (y_min != rhs.y_min) ||
               (y_max != rhs.y_max);
    }

    short x_min, x_max, y_min, y_max;
};

struct OpenGL2D{};
struct Pitched2D{};
struct Array2D{};

/* Base class template */
template<typename ImageType, typename DataType>
struct CUDAImage
{
};

template<typename DataType>
struct CUDAImage<Pitched2D, DataType>
{

    typedef Pitched2D img_type;
    typedef DataType data_type;

    DataType* ptr;
    size_t pitch, w ,h;

    /* CUDA texture objects */
    cudaTextureObject_t cudaTexes;
    cudaResourceDesc cudaResDesc;
    cudaTextureDesc cudaTexDesc;

    CUDAImage() : ptr(NULL), pitch(0), w(0), h(0)
    {
    }

    CUDAImage(size_t const w, size_t const h)
        :w(w), h(h)
    {
        checkCudaErrors(cudaMallocPitch((void **)&ptr, &pitch, w*sizeof(DataType), h));
    }

    CUDAImage(size_t const w, size_t const h, DataType const value)
        :w(w), h(h)
    {
        checkCudaErrors(cudaMallocPitch((void **)&ptr, &pitch, w*sizeof(DataType), h));
        checkCudaErrors(cudaMemset2D(ptr, pitch, value, w*sizeof(float), h));
    }

    ~CUDAImage()
    {
        checkCudaErrors(cudaFree(ptr));
    }

    void Reinitialise(size_t const w, size_t const h)
    {
        if(ptr)
            checkCudaErrors(cudaFree(ptr));

        this->w = w;
        this->h = h;
        checkCudaErrors(cudaMallocPitch((void **)&ptr, &pitch, w*sizeof(DataType), h));
    }

    void Reinitialise(size_t const w, size_t const h, DataType const value)
    {
        if(ptr)
            checkCudaErrors(cudaFree(ptr));

        this->w = w;
        this->h = h;
        checkCudaErrors(cudaMallocPitch((void **)&ptr, &pitch, w*sizeof(DataType), h));
        checkCudaErrors(cudaMemset2D(ptr, pitch, value, w*sizeof(float), h));
    }

    cudaPitchedPtr GetCUDAPitchedPtr() const { return make_cudaPitchedPtr(ptr, pitch, w, h); }

    void SetValue(int const value)
    {
        checkCudaErrors(cudaMemset2D(ptr, pitch, value, w*sizeof(DataType), h));
    }

    template<typename T>
    void CopyFrom(CUDAImage<Pitched2D, T> const& rhs)
    {
        assert(w == rhs.w && h == rhs.h && GetBytes() == rhs.GetBytes());
        checkCudaErrors(cudaMemcpy2D(ptr, pitch, rhs.ptr, rhs.pitch, w*sizeof(DataType), h, cudaMemcpyDeviceToDevice));
    }

    template<typename T>
    void CopyTo(CUDAImage<Pitched2D, T>& rhs) const
    {
        assert(w == rhs.w && h == rhs.h && GetBytes() == rhs.GetBytes());
        checkCudaErrors(cudaMemcpy2D(rhs.ptr, rhs.pitch, ptr, pitch, w*sizeof(DataType), h, cudaMemcpyDeviceToDevice));
    }

    size_t GetWidth() const { return w; }
    size_t GetHeight() const { return h; }

    size_t GetBytes() const { return w*h*sizeof(DataType); }

    string GetImageType() const { return typeid(img_type).name(); }
    string GetDataFormat() const { return typeid(data_type).name(); }

    void CreateCUDATextureObject(cudaTextureAddressMode const address_mode = cudaAddressModeClamp,
                                 cudaTextureFilterMode const filter_mode = cudaFilterModePoint,
                                 cudaTextureReadMode const read_mode = cudaReadModeElementType,
                                 bool const normalised_coord = false)
    {
        memset(&cudaTexDesc, 0, sizeof(cudaTextureDesc));
        cudaTexDesc.addressMode[0] = address_mode;
        cudaTexDesc.addressMode[1] = address_mode;
        cudaTexDesc.filterMode = filter_mode;
        cudaTexDesc.readMode = read_mode;
        cudaTexDesc.normalizedCoords = normalised_coord;

        memset(&cudaResDesc, 0, sizeof(cudaResourceDesc));
        cudaResDesc.resType = cudaResourceTypePitch2D;
        cudaResDesc.res.pitch2D.desc = cudaCreateChannelDesc<DataType>();
        cudaResDesc.res.pitch2D.devPtr = ptr;
        cudaResDesc.res.pitch2D.pitchInBytes = pitch;
        cudaResDesc.res.pitch2D.width = w;
        cudaResDesc.res.pitch2D.height = h;

        cudaCreateTextureObject(&cudaTexes, &cudaResDesc, &cudaTexDesc, NULL);

    }

    void DestroyCUDATextureObject()
    {
        cudaDestroyTextureObject(cudaTexes);
    }

private:
    CUDAImage(CUDAImage const& rhs);
    CUDAImage& operator=(CUDAImage rhs);

};

/* 2D OpenGL texture only at the moment */
template<>
struct CUDAImage<OpenGL2D, uchar4>
{

    typedef OpenGL2D img_type;
    typedef uchar4 data_type;

    cudaArray_t array;
    cudaGraphicsResource_t res;

    GLint internal_format;
    GLuint tid;

    size_t w, h;
    bool is_mapped;

    /* CUDA texture objects */
    cudaTextureObject_t cudaTexes;
    cudaResourceDesc cudaResDesc;
    cudaTextureDesc cudaTexDesc;

    /* NULL image */
    CUDAImage() : w(0), h(0), tid(0), internal_format(0), array(NULL), res(NULL) {}

    /* Bind with OpenGL texture immediately */
    CUDAImage(size_t const w, size_t const h, GLint const internal_format = GL_RGBA8, bool const sampling_linear = true)
        : tid(0), is_mapped(false)
    {
        Reinitialise(w, h, internal_format, sampling_linear);
    }

    ~CUDAImage()
    {
        glDeleteTextures(1,&tid);
        if(res)
            checkCudaErrors(cudaGraphicsUnregisterResource(res));
    }

    void Reinitialise(size_t const w, size_t const h, GLint const internal_format = GL_RGBA8, bool const sampling_linear = true)
    {

        if(tid)
        {
            glDeleteTextures(1,&tid);
            checkCudaErrors(cudaGraphicsUnregisterResource(res));
        }

        this->w = w;
        this->h = h;
        this->internal_format = internal_format;
        this->is_mapped = false;

        glGenTextures(1, &tid);
        Bind();

        // GL_LUMINANCE and GL_FLOAT don't seem to actually affect buffer, but some values are required
        // for call to succeed.
        glTexImage2D(GL_TEXTURE_2D, 0, internal_format, w, h, 0, GL_LUMINANCE,GL_FLOAT, 0);

        if(sampling_linear) {
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        }else{
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        checkCudaErrors(cudaGraphicsGLRegisterImage(&res, tid, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsSurfaceLoadStore));

        Unbind();

    }

    void SetLinear()
    {
      Bind();
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      Unbind();
    }

    void SetNearestNeighbour()
    {
      Bind();
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      Unbind();
    }

    void Upload(void* image, GLenum data_layout = GL_RGBA, GLenum data_type = GL_UNSIGNED_BYTE)
    {
        Bind();
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, data_layout, data_type, image);
        Unbind();
    }

    void Bind() const
    {
        glBindTexture(GL_TEXTURE_2D, tid);
    }

    void Unbind() const
    {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    /* Map CUDA and OpenGL depth textures */
    void MapCUDARes()
    {
        checkCudaErrors(cudaGraphicsMapResources(1, &res));
        checkCudaErrors(cudaGraphicsSubResourceGetMappedArray(&array, res, 0, 0));
        is_mapped = true;
    }

    /* Unmap CUDA and OpenGL depth textures */
    void UnmapCUDARes()
    {
        cudaGraphicsUnmapResources(1, &res);
        is_mapped = false;
    }

    cudaArray_t GetArrayPtr()
    {
        assert(is_mapped);
        return array;
    }

    cudaArray_const_t GetArrayPtr() const
    {
        assert(is_mapped);
        return array;
    }

    size_t GetOpenGLBindTextureID() const
    {
        return tid;
    }

    void CopyFrom(CUDAImage<OpenGL2D, uchar4> *const src)
    {
        src->MapCUDARes();
        MapCUDARes();
        checkCudaErrors(cudaMemcpy2DArrayToArray(array, 0, 0, src->GetArrayPtr(), 0, 0, w*sizeof(uchar4), h, cudaMemcpyDeviceToDevice));
        UnmapCUDARes();
        src->UnmapCUDARes();
    }

    void CopyFrom(CUDAImage<OpenGL2D, uchar4> *const src, ROI const roi)
    {
        src->MapCUDARes();
        MapCUDARes();

        size_t const roi_w = roi.x_max - roi.x_min + 1;
        size_t const roi_h = roi.y_max - roi.y_min + 1;
        checkCudaErrors(cudaMemcpy2DArrayToArray(array, 0, 0, src->GetArrayPtr(), roi.x_min*sizeof(uchar4), roi.y_min, roi_w*sizeof(uchar4), roi_h, cudaMemcpyDeviceToDevice));

        UnmapCUDARes();
        src->UnmapCUDARes();
    }

    size_t GetWidth() const { return w; }
    size_t GetHeight() const { return h; }

    size_t GetBytes() const { return w*h*sizeof(data_type); }

    string GetImageType() const { return typeid(img_type).name(); }
    string GetDataFormat() const { return typeid(data_type).name(); }

    /* Rendering functions */
    void RenderToViewport() const
    {
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      Bind();
      glEnable(GL_TEXTURE_2D);
      glBegin(GL_QUADS);
      glTexCoord2f(0, 0); glVertex2d(-1,-1);
      glTexCoord2f(1, 0); glVertex2d(1,-1);
      glTexCoord2f(1, 1); glVertex2d(1,1);
      glTexCoord2f(0, 1); glVertex2d(-1,1);
      glEnd();
      Unbind();
      glDisable(GL_TEXTURE_2D);
    }

    void RenderToViewportFlipY() const
    {
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      Bind();
      glEnable(GL_TEXTURE_2D);
      glBegin(GL_QUADS);
      glTexCoord2f(0, 0); glVertex2d(-1,1);
      glTexCoord2f(1, 0); glVertex2d(1,1);
      glTexCoord2f(1, 1); glVertex2d(1,-1);
      glTexCoord2f(0, 1); glVertex2d(-1,-1);
      glEnd();
      Unbind();
      glDisable(GL_TEXTURE_2D);
    }

    void CreateCUDATextureObject(cudaTextureAddressMode const address_mode = cudaAddressModeClamp,
                                 cudaTextureFilterMode const filter_mode = cudaFilterModePoint,
                                 cudaTextureReadMode const read_mode = cudaReadModeElementType,
                                 bool const normalised_coord = false)
    {
        memset(&cudaTexDesc, 0, sizeof(cudaTextureDesc));
        cudaTexDesc.addressMode[0] = address_mode;
        cudaTexDesc.addressMode[1] = address_mode;
        cudaTexDesc.filterMode = filter_mode;
        cudaTexDesc.readMode = read_mode;
        cudaTexDesc.normalizedCoords = normalised_coord;

        memset(&cudaResDesc, 0, sizeof(cudaResourceDesc));
        cudaResDesc.resType = cudaResourceTypeArray;

        MapCUDARes();
        cudaResDesc.res.array.array = GetArrayPtr();
        cudaCreateTextureObject(&cudaTexes, &cudaResDesc, &cudaTexDesc, NULL);
        UnmapCUDARes();

    }

    void DestroyCUDATextureObject()
    {
        cudaDestroyTextureObject(cudaTexes);
    }

private:
    CUDAImage(CUDAImage const& rhs);
    void operator=(CUDAImage rhs);

};

} // namespace falcon {

#endif // FALCON_CUDA_IMG_H

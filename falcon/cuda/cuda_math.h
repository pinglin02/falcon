#ifndef FALCON_MATH_H
#define FALCON_MATH_H

#include <cuda_runtime.h>

namespace falcon {

/* For J_Iref, in which a full res. Image will take 3*360*288/1024^2 = 1.2MB */
struct Matrixf1x3
{
    float m[3];
};

/* For J_w, in which a full res. Image will take 3*16*360*288/1024^2 = 19MB */
struct Matrixf3x16
{
    float m[48];
};

struct Matrixf2x3
{
    float m[6];
};

template<int R, int C, typename P>
__device__ __host__ void MatPrint(P const m[R*C])
{

    for(int r=0; r < R; ++r)
    {
        for( int c=0; c < C; ++c )
        {
            if(c == 0 && r == 0)
                m[R*c+r] >= 0 ? printf("[ %e,", m[R*c+r]) : printf("[%e,", m[R*c+r]);
            else if(c == C-1 && r == R-1)
                m[R*c+r] >= 0 ? printf("  %e", m[R*c+r]) : printf(" %e", m[R*c+r]);
            else
                m[R*c+r] >= 0 ? printf("  %e,", m[R*c+r]) : printf(" %e,", m[R*c+r]);
        }

        if(r < R-1)
            printf("\n");
    }

    printf(" ]\n\n");
}


// Convert row major to column major matrix
template<int M, int N, typename P>
__device__ __host__ void Col2RowMajor(P rm[M*N], P const cm[M*N])
{

    for(int i=0; i<N; ++i)
        for(int j=0; j<M; ++j)
            rm[i*M+j] = cm[j*N+i];
}

// Convert column major to row major matrix
template<int M, int N, typename P>
__device__ __host__ void Row2ColMajor(P cm[M*N], P const rm[M*N])
{

    for(int i=0; i<N; ++i)
        for(int j=0; j<M; ++j)
            cm[j*N+i] = rm[i*M+j];
}

/* Remarks: The functions below are all operating with row-major matrix! */

// m = identity(N)
template<int N, typename P>
__device__ __host__ void SetIdentity(P m[N*N])
{
    memset(m, 0, N*N*sizeof(P));
    for( int i=0; i< N; ++i )
        m[N*i+i] = 1;
}

// m = zero(N)
template<int N, typename P>
__device__ __host__ void SetZero(P m[N*N])
{
    memset(m, 0, N*N*sizeof(P));
}

// m = zero(R, C)
template<int R, int C, typename P>
__device__ __host__ void SetZero(P m[R*C])
{
    memset(m, 0, R*C*sizeof(P));
}

template<typename P>
__device__ __host__ void LieSE3from4x4(P out[3*4], P const in[4*4] )
{
    memcpy(out, in, 3*sizeof(P));
    memcpy(out+3, in+4, 3*sizeof(P));
    memcpy(out+6, in+8, 3*sizeof(P));
    memcpy(out+9, in+12, 3*sizeof(P));
}

template<typename P>
__device__ __host__ void LiePutSE3in4x4(P out[4*4], P const in[3*4] )
{

  SetIdentity<4>(out);
  memcpy(out, in, 3*sizeof(P));
  memcpy(out+4, in+3, 3*sizeof(P));
  memcpy(out+8, in+6, 3*sizeof(P));
  memcpy(out+12, in+9, 3*sizeof(P));

}

/// mo = m1 * m2
template<int R, int M, int C, typename P>
__device__ __host__ void MatMul(P mo[R*C], P const m1[R*M], P const m2[M*C] )
{
    for( int r=0; r < R; ++r)
        for( int c=0; c < C; ++c )
        {
            mo[R*c+r] = 0;
            for( int m=0; m < M; ++ m) mo[R*c+r] += m1[R*m+r] * m2[M*c+m];
        }
}

/// mo = m1 * m2 * s
template<int R, int M, int C, typename P>
__device__ __host__ void MatMul(P mo[R*C], P const m1[R*M], P const m2[M*C], P s )
{
  for( int r=0; r < R; ++r)
    for( int c=0; c < C; ++c )
    {
      mo[R*c+r] = 0;
      for( int m=0; m < M; ++ m) mo[R*c+r] += m1[R*m+r] * m2[M*c+m] * s;
    }
}

/// m = m1 * scalar
template<int R, int C, typename P>
__device__ __host__ void MatMul(P m[R*C], P const m1[R*C], P scalar)
{
    for( int i=0; i< R*C; ++i )
        m[i] = m1[i] * scalar;
}

template<int R, int C, typename P>
__device__ __host__ void MatAdd(P m[R*C], P const m1[R*C], P const m2[R*C])
{
    for( int i=0; i< R*C; ++i )
        m[i] = m1[i] + m2[i];
}

template<int R, int C, typename P>
__device__ __host__ void MatSub(P m[R*C], P const m1[R*C], P const m2[R*C])
{
    for( int i=0; i< R*C; ++i )
        m[i] = m1[i] - m2[i];
}

template<typename P>
__device__ __host__ void LieApplySO3(P out[3], P const R_ba[3*3], P const in[3])
{
    MatMul<3, 3, 1, P>(out, R_ba, in);
}

template<typename P>
__device__ __host__ void LieApplySE3vec(P x_b[3], P const T_ba[3*4], P const x_a[3])
{
    P rot[3];
    MatMul<3,3,1,P>(rot,T_ba,x_a);
    MatAdd<3,1,P>(x_b,rot,T_ba+(3*3));
}

template<typename P>
__device__ __host__ void LieMulSO3(P R_ca[3*3], P const R_cb[3*3], P const R_ba[3*3])
{
    MatMul<3,3,3>(R_ca,R_cb,R_ba);
}

template<typename P>
__device__ __host__ void LieMulSE3(P T_ca[3*4], P const T_cb[3*4], P const T_ba[3*4])
{
    LieMulSO3<>(T_ca,T_cb,T_ba);
    P R_cb_times_a_b[3];
    LieApplySO3<>(R_cb_times_a_b,T_cb,T_ba+(3*3));
    MatAdd<3,1>(T_ca+(3*3),R_cb_times_a_b,T_cb+(3*3));
}

template<int N, typename P>
__device__ __host__ void MatTranspose(P out[N*N], P const in[N*N] )
{
    for( int c=0; c<N; ++c )
        for( int r=0; r<N; ++r )
            out[N*c+r] = in[N*r+c];
}

template<typename P>
__device__ __host__ void LieTransposeSO3( P R_ab[3*3], P const R_ba[3*3] )
{
    MatTranspose<3,P>(R_ab,R_ba);
}

template<typename P>
__device__ __host__ void LieInverseSE3( P T_ab[3*4], P const T_ba[3*4] )
{
    LieTransposeSO3<P>(T_ab,T_ba);
    P minus_b_a[3];
    LieApplySO3(minus_b_a, T_ab, T_ba+(3*3));
    MatMul<3, 1, P>(T_ab+(3*3), minus_b_a, -1);
}

template<typename P>
__device__ __host__ void LieGetRotation(P R_ba[3*3], P const T_ba[3*4])
{
    memcpy(R_ba, T_ba, 9*sizeof(P));
}

template<typename P>
__device__ __host__ void LieGetTranslation(P t_ba[3], P const T_ba[3*4])
{
    memcpy(t_ba, T_ba+9, 3*sizeof(P));
}

template<typename P>
__device__ __host__ void LieSetIdentity(P T_ba[3*4] )
{
    SetIdentity<3>(T_ba);
    memset(T_ba+9, 0, 3*sizeof(P));
}

} // namespace falcon {

#endif // FALCON_MATH_H

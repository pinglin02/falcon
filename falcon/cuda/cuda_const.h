#ifndef FALCON_CUDA_CONST_H
#define FALCON_CUDA_CONST_H

#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>

namespace falcon {

size_t const PYR_LEVEL_NUM = 3;

/* Some graphic cards seem to be no support for exact 1024 threads */
size_t const THREAD_NUM_1D_BLOCK = 256;
size_t const THREAD_NUM_2D_BLOCK = 16;
size_t const THREAD_NUM_3D_BLOCK = 8;

/* Must be divisible with no remainder for 256 */
size_t const HISTOGRAM_BIN_NUM = 64;

/* For the reduction of the Jacobian calculation */
size_t const SHARE_THREAD_NUM = 4;

///**
// *  Constant macros
// */

#define checkCudaErrors(err)  __checkCudaErrors (err, __FILE__, __LINE__)
#define getLastCudaError(msg) __getLastCudaError (msg, __FILE__, __LINE__)

////////////////////////////////////////////////////////////////////////////////
// These are CUDA Helper functions
// This will output the proper CUDA error strings in the event that a CUDA host call returns an error
inline void __checkCudaErrors(cudaError err, const char *file, const int line )
{
    if(cudaSuccess != err)
    {
        fprintf(stderr, "%s(%i) : CUDA Runtime API error %d: %s.\n",
                file, line, (int)err, cudaGetErrorString(err) );
        return ;
    }
}

// This will output the proper error string when calling cudaGetLastError
inline void __getLastCudaError(const char *errorMessage, const char *file, const int line )
{
    cudaError_t err = cudaGetLastError();
    if (cudaSuccess != err)
    {
        fprintf(stderr, "%s(%i) : getLastCudaError() CUDA error : %s : (%d) %s.\n",
                file, line, errorMessage, (int)err, cudaGetErrorString(err) );
        return ;
    }
}

class CUDATimer
{
public:
    CUDATimer()
    {
    }

    ~CUDATimer()
    {
        checkCudaErrors(cudaEventDestroy(start));
        checkCudaErrors(cudaEventDestroy(stop));
    }

    void Start()
    {
        checkCudaErrors(cudaEventCreate(&start));
        checkCudaErrors(cudaEventCreate(&stop));
        checkCudaErrors(cudaEventRecord(start, 0));
    }

    double Stop()
    {
        checkCudaErrors(cudaEventRecord(stop, 0));
        checkCudaErrors(cudaEventSynchronize(stop));
        checkCudaErrors(cudaEventElapsedTime(&gpuTime, start, stop));
        return gpuTime;
    }

private:
    cudaEvent_t start, stop;
    float gpuTime;

};

} // namespace falcon {

#endif // FALCON_CUDA_CONST_H

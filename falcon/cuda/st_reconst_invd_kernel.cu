#include "cuda_const.h"
#include "cuda_math.h"
#include "../st_reconst.h"
#include "../img_proc.h"

namespace falcon {

/* CUDA texutres */
texture<uchar4, cudaTextureType2D, cudaReadModeElementType> ref_img_tex;
texture<uchar4, cudaTextureType2D, cudaReadModeElementType> target_img_tex;

texture<float, cudaTextureType2D, cudaReadModeElementType> invd_tex;

texture<float, cudaTextureType2D, cudaReadModeElementType> win_invd_tex;

texture<float, cudaTextureType2D, cudaReadModeElementType> min_disp_cost_tex;
texture<float, cudaTextureType2D, cudaReadModeElementType> max_disp_cost_tex;

texture<float, cudaTextureType2D, cudaReadModeElementType> aux_tex;

texture<float, cudaTextureType2D, cudaReadModeElementType> head_primal_tex;
texture<float, cudaTextureType2D, cudaReadModeElementType> old_primal_tex;

texture<float, cudaTextureType2D, cudaReadModeElementType> primal_tex;
texture<float, cudaTextureType2D, cudaReadModeElementType> primal_step_tex;

texture<float2, cudaTextureType2D, cudaReadModeElementType> dual_tex;
texture<float2, cudaTextureType2D, cudaReadModeElementType> dual_step_tex;

texture<float4, cudaTextureType2D, cudaReadModeElementType> diffuse_tensor_tex;

/* Write OpenGL texture by using CUDA surface */
surface<void, cudaSurfaceType3D> cost_volume_surface;
surface<void, cudaSurfaceType2D> invd_surface;

/* Constant variables */
float const cost_max = 2.0;

/* Constant structures */
__constant__ InverseDepth::CostVolume::Params const_dev_cv_params;
__constant__ InverseDepth::HuberL1TGV1::Params const_dev_hbl1_tgv1_params;

/* Inverse depth look-up-table */
__constant__ float* const_invd_LUT;

/* Camera intrinsic matrix */
__constant__ float const_dev_Kl[9];
__constant__ float const_dev_Kr[9];

/* Extrinsic matrix for the stereo camera (the right w.r.t the left camera in the world) */
__constant__ float const_dev_Text[16];

/* Pose for the point cloud */
__constant__ float const_dev_T[16];

namespace InverseDepth {

namespace CostVolume {

/*
 *  Initialise cuda constnat cost-volume paramenters
 *
 *  Remark: Must be set before the kernels
 */
void SetConstCostVolumeParams(CostVolume::Params const disp_cv_params)
{

    /* Copy CUDA constant */
    checkCudaErrors(cudaMemcpyToSymbol(const_dev_cv_params, &disp_cv_params, sizeof(CostVolume::Params)));

}

void SetConstInverseDepthLUT(float const min_depth, float const max_depth, uint8_t const num_depth)
{
    assert(min_depth <= max_depth);

    float invd_LUT[num_depth];

    float max_invd = 1.0/min_depth;
    float min_invd = 1.0/max_depth;

    float invd_step = (max_invd - min_invd) / float(num_depth-1);

    for(uint32_t i = 0; i < num_depth; i++)
        invd_LUT[i] = min_invd + i*invd_step;

    checkCudaErrors(cudaMemcpyToSymbol(const_invd_LUT, &invd_LUT, sizeof(float)*num_depth));


}

void SetConstStereoCameraMatrices(float const Kl[9], float const Kr[9], float const Text[16])
{
    checkCudaErrors(cudaMemcpyToSymbol(const_dev_Kl, Kl, 9*sizeof(float)));
    checkCudaErrors(cudaMemcpyToSymbol(const_dev_Kr, Kr, 9*sizeof(float)));
    checkCudaErrors(cudaMemcpyToSymbol(const_dev_Text, Text, 16*sizeof(float)));
}

/*
 *  Bind image textures
 *
 *  Remark: Reference image can be left or right camera image, so the target image
 */
void BindImageTextures(cudaArray const *const left_img_array,
                       cudaArray const *const right_img_array,
                       RefImage const ref_img)
{

    /* Bind to read-only textures */
    if(ref_img == LeftRefImage)
    {
        checkCudaErrors(cudaBindTextureToArray(ref_img_tex, left_img_array));
        checkCudaErrors(cudaBindTextureToArray(target_img_tex, right_img_array));
    }
    else if(ref_img == RightRefImage)
    {
        checkCudaErrors(cudaBindTextureToArray(ref_img_tex, right_img_array));
        checkCudaErrors(cudaBindTextureToArray(target_img_tex, left_img_array));
    }

}

/*
 *  Unbind image textures
 *
 *  Remark: Reference image can be left or right camera image, so the target image
 */
void UnbindImageTextures()
{

    checkCudaErrors(cudaUnbindTexture(ref_img_tex));
    checkCudaErrors(cudaUnbindTexture(target_img_tex));
}

/*
 *  Cost-Volume Construction
 *
 *  Remark: The cost value is normalised to [0,1]
 */

/* Absolute Differences (AD) */
void __global__ ADKernel(uint32_t const width,
                         uint32_t const height)
{
    uint32_t const x = blockDim.x * blockIdx.x + threadIdx.x;
    uint32_t const y = blockDim.y * blockIdx.y + threadIdx.y;
    uint32_t const z = blockDim.z * blockIdx.z + threadIdx.z;

    uint32_t const num_depth = const_dev_cv_params.num_depth;
    RefImage const ref_img  = const_dev_cv_params.ref_img;

    uint8_t const min_bg_thr = const_dev_cv_params.min_bg_thr;
    uint8_t const max_bg_thr = const_dev_cv_params.max_bg_thr;

    if(x < width && y < height && z < num_depth)
    {

        surf3Dwrite(cost_max, cost_volume_surface, x*sizeof(float), y, z);

        uint8_t const ref_gray = CUDAImgProc::RGBA2Gray(tex2D(ref_img_tex, x, y));
        if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
        {

            switch (ref_img)
            {
            case LeftRefImage:
            {

                float fu, fv, u0, v0;

                fu = const_dev_Kl[0];
                fv = const_dev_Kl[4];
                u0 = const_dev_Kl[6];
                v0 = const_dev_Kl[7];

                float d = 1.0/const_invd_LUT[z];

                float pts[3];
                pts[0] = ((float(x) - u0)/fu) * d;
                pts[1] = ((float(y) - v0)/fv) * d;
                pts[2] = d;

                float pts_right[3], T[12];
                LieSE3from4x4(T, const_dev_Text);
                LieInverseSE3(T, T);
                LieApplySE3vec(pts_right, T, pts);

                fu = const_dev_Kr[0];
                fv = const_dev_Kr[4];
                u0 = const_dev_Kr[6];
                v0 = const_dev_Kr[7];

                size_t u = (pts_right[0]/pts_right[2])*fu+u0;
                size_t v = (pts_right[1]/pts_right[2])*fv+v0;

                float cost = abs(CUDAImgProc::RGBA2FloatGray(tex2D(ref_img_tex, x, y)) - CUDAImgProc::RGBA2FloatGray(tex2D(target_img_tex, u, v)));
                surf3Dwrite(cost, cost_volume_surface, x*sizeof(float), y, z);

            }
                break;
            case RightRefImage:
            {
                float fu, fv, u0, v0;

                fu = const_dev_Kr[0];
                fv = const_dev_Kr[4];
                u0 = const_dev_Kr[6];
                v0 = const_dev_Kr[7];

                float d = 1.0/const_invd_LUT[z];

                float pts[3];
                pts[0] = ((float(x) - u0)/fu) * d;
                pts[1] = ((float(y) - v0)/fv) * d;
                pts[2] = d;

                float pts_left[3], T[12];
                LieSE3from4x4(T, const_dev_Text);
                LieApplySE3vec(pts_left, T, pts);

                fu = const_dev_Kl[0];
                fv = const_dev_Kl[4];
                u0 = const_dev_Kl[6];
                v0 = const_dev_Kl[7];

                size_t u = (pts_left[0]/pts_left[2])*fu+u0;
                size_t v = (pts_left[1]/pts_left[2])*fv+v0;

                float cost = abs(CUDAImgProc::RGBA2FloatGray(tex2D(ref_img_tex, x, y)) - CUDAImgProc::RGBA2FloatGray(tex2D(target_img_tex, u, v)));
                surf3Dwrite(cost, cost_volume_surface, x*sizeof(float), y, z);
            }
                break;
            default:
                break;
            }
        }
    }
}

///* Zero norm_grad_imgalised Cross-Correlation (ZNCC) */
//void __global__ ZNCCKernel(uint32_t const width,
//                           uint32_t const height)
//{
//    uint32_t const x = blockDim.x * blockIdx.x + threadIdx.x;
//    uint32_t const y = blockDim.y * blockIdx.y + threadIdx.y;
//    uint32_t const z = blockDim.z * blockIdx.z + threadIdx.z;

//    uint32_t const min_disp = const_dev_cv_params.min_disp;
//    uint32_t const max_disp = const_dev_cv_params.max_disp;
//    uint32_t const num_disp = const_dev_cv_params.num_disp;
//    uint32_t const win_r    = const_dev_cv_params.win_r;
//    RefImage const ref_img  = const_dev_cv_params.ref_img;

//    uint8_t const min_bg_thr = const_dev_cv_params.min_bg_thr;
//    uint8_t const max_bg_thr = const_dev_cv_params.max_bg_thr;

//    uint8_t const ref_gray = CUDAImgProc::RGBA2Gray(tex2D(ref_img_tex, x, y));

//    if (x < width && y < height && z < num_disp)
//    {
//        surf3Dwrite(cost_max, cost_volume_surface, x*sizeof(float), y, z);

//        if(ref_gray <= max_bg_thr && ref_gray >= min_bg_thr)
//        {
//            switch (ref_img)
//            {
//            case LeftRefImage:
//                if(x >= win_r+max_disp && x < width-win_r && y >= win_r && y < height-win_r)
//                {
//                    uint32_t disp = z+min_disp;
//                    double sum_ref = 0, sum_target = 0, sum_ref_target = 0, sum_sq_ref = 0, sum_sq_target = 0;

//                    for(int win_x = x-win_r; win_x <= x+win_r; win_x++)
//                        for(int win_y = y-win_r; win_y <= y+win_r; win_y++)
//                        {
//                            double ref_gray = CUDAImgProc::RGBA2FloatGray(tex2D(ref_img_tex, win_x, win_y));
//                            double target_gray = CUDAImgProc::RGBA2FloatGray(tex2D(target_img_tex, win_x-disp, win_y));

//                            sum_ref += ref_gray;
//                            sum_target += target_gray;

//                            sum_ref_target += (ref_gray*target_gray);

//                            sum_sq_ref += (ref_gray*ref_gray);
//                            sum_sq_target += (target_gray*target_gray);
//                        }

//                    double N = (2*win_r+1)*(2*win_r+1);

//                    double numerator = N*sum_ref_target - sum_ref*sum_target;
//                    double denominator = (N*sum_sq_target - sum_target*sum_target)*(N*sum_sq_ref - sum_ref*sum_ref);

//                    if(denominator > 0)
//                    {
//                        float cost = 1.0 - (numerator*rsqrtf(abs(denominator))+1.0)/2.0;
//                        surf3Dwrite(cost, cost_volume_surface, x*sizeof(float), y, z);
//                    }

//                }
//                break;
//            case RightRefImage:
//                if(x >= win_r && x < width-win_r-max_disp && y >= win_r && y < height-win_r)
//                {
//                    uint32_t disp = z+min_disp;
//                    double sum_ref = 0, sum_target = 0, sum_ref_target = 0, sum_sq_ref = 0, sum_sq_target = 0;

//                    for(int win_x = x-win_r; win_x <= x+win_r; win_x++)
//                        for(int win_y = y-win_r; win_y <= y+win_r; win_y++)
//                        {
//                            double ref_gray = CUDAImgProc::RGBA2FloatGray(tex2D(ref_img_tex, win_x, win_y));
//                            double target_gray = CUDAImgProc::RGBA2FloatGray(tex2D(target_img_tex, win_x+disp, win_y));

//                            sum_ref += ref_gray;
//                            sum_target += target_gray;

//                            sum_ref_target += ref_gray*target_gray;

//                            sum_sq_ref += ref_gray*ref_gray;
//                            sum_sq_target += target_gray*target_gray;
//                        }

//                    double N = (2*win_r+1)*(2*win_r+1);

//                    double numerator = N*sum_ref_target - sum_ref*sum_target;
//                    double denominator = (N*sum_sq_target - sum_target*sum_target)*(N*sum_sq_ref - sum_ref*sum_ref);

//                    if(denominator > 0)
//                    {
//                        float cost = 1.0 - (numerator*rsqrtf(abs(denominator))+1.0)/2.0;
//                        surf3Dwrite(cost, cost_volume_surface, x*sizeof(float), y, z);
//                    }

//                }
//                break;
//            default:
//                break;
//            }

//        }
//    }

//}

void BuildCostVolume(cudaArray *const cost_volume_array,
                     DataTermMethod const method,
                     uint32_t const num_disp,
                     uint32_t const width,
                     uint32_t const height)
{

    checkCudaErrors(cudaBindSurfaceToArray(cost_volume_surface, cost_volume_array));

    dim3 dimBlock(THREAD_NUM_3D_BLOCK, THREAD_NUM_3D_BLOCK, THREAD_NUM_3D_BLOCK);
    dim3 dimGrid((width+dimBlock.x-1)/dimBlock.x,
                 (height+dimBlock.y-1)/dimBlock.y,
                 (num_disp+dimBlock.z-1)/dimBlock.z);

    /* AD */
    if(method == AD)
        ADKernel<<<dimGrid, dimBlock>>>(width,
                                        height);

//    /* ZNCC */
//    if(method == ZNCC)
//        ZNCCKernel<<<dimGrid, dimBlock>>>(width,
//                                          height);

}

/*
 *  Winner-take-all kernel
 *
 *  Remark: The cost value is normalised to [0,1]
 */

void __global__ WinnerTakeAllKernel(cudaPitchedPtr const win_disp,
                                    cudaPitchedPtr const min_disp_cost,
                                    cudaPitchedPtr const max_disp_cost,
                                    size_t const width,
                                    size_t const height)
{
    uint32_t const x = blockDim.x * blockIdx.x + threadIdx.x;
    uint32_t const y = blockDim.y * blockIdx.y + threadIdx.y;

    uint32_t const num_disp = const_dev_cv_params.num_depth;

    if(x < width && y < height)
    {

        float min_cost = cost_max;
        uint32_t min_cost_idx = 0;
        for(uint32_t z = 0; z < num_disp; z++)
        {

            float cost = surf3Dread<float>(cost_volume_surface, x*sizeof(float), y, z);
            if(cost < min_cost)
            {
                min_cost = cost;
                min_cost_idx = z;
            }

        }

        float* win_disp_row = (float*)((char*)win_disp.ptr + y*win_disp.pitch);
        win_disp_row[x] = float(min_cost_idx)/float(num_disp-1);

        float* min_disp_cost_row = (float*)((char*)min_disp_cost.ptr + y*min_disp_cost.pitch);
        min_disp_cost_row[x] = min_cost;

        /* Search for max cost value */
        float max_cost = -1;
        for(uint32_t z = 0; z < num_disp; z++)
        {
            float cost = surf3Dread<float>(cost_volume_surface, x*sizeof(float), y, z);
            if(cost > max_cost)
                max_cost = cost;
        }

        float* max_disp_cost_row = (float*)((char*)max_disp_cost.ptr + y*max_disp_cost.pitch);
        max_disp_cost_row[x] = max_cost;
    }

}

void WinnerTakeAll(cudaPitchedPtr const win_disp,
                   cudaPitchedPtr const min_disp_cost,
                   cudaPitchedPtr const max_disp_cost,
                   uint32_t const width,
                   uint32_t const height)
{

    dim3 dimBlock = dim3(THREAD_NUM_2D_BLOCK, THREAD_NUM_2D_BLOCK);
    dim3 dimGrid = dim3((width+dimBlock.x-1)/dimBlock.x, (height+dimBlock.y-1)/dimBlock.y);
    WinnerTakeAllKernel<<<dimGrid, dimBlock>>>(win_disp,
                                               min_disp_cost,
                                               max_disp_cost,
                                               width,
                                               height);
}

} // namespace CostVolume {

} // namespace InverseDepth {

} // namespace falcon {

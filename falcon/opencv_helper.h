#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "cuda/cuda_img.h"

namespace falcon {

static inline void ShowOpenCVImage(CUDAImage<Pitched2D,float> const& cuda_img, string const file_name)
{

    size_t const w = cuda_img.GetWidth();
    size_t const h = cuda_img.GetHeight();


    cv::Mat cv_img(cv::Size(w, h), CV_32F);
    checkCudaErrors(cudaMemcpy2D(cv_img.data, cv_img.step,
                                 cuda_img.GetCUDAPitchedPtr().ptr,
                                 cuda_img.GetCUDAPitchedPtr().pitch,
                                 w*sizeof(float), h,
                                 cudaMemcpyDeviceToHost));

    double minVal, maxVal;
    minMaxLoc(cv_img, &minVal, &maxVal);
    cv_img = (cv_img - minVal)*(1.0/(maxVal-minVal));

//    cout << "minVal: " << minVal << ", " << "maxVal: " << maxVal << endl;

    cv_img.convertTo(cv_img, CV_8U, 255);
    imwrite(file_name+".png", cv_img);

}

static inline void ShowOpenCVImage(CUDAImage<Pitched2D,uchar1> const& cuda_img, string const file_name)
{

    size_t const w = cuda_img.GetWidth();
    size_t const h = cuda_img.GetHeight();


    cv::Mat cv_img(cv::Size(w, h), CV_8U);
    checkCudaErrors(cudaMemcpy2D(cv_img.data, cv_img.step,
                                 cuda_img.GetCUDAPitchedPtr().ptr,
                                 cuda_img.GetCUDAPitchedPtr().pitch,
                                 w*sizeof(uchar1), h,
                                 cudaMemcpyDeviceToHost));

    imwrite(file_name+".png", cv_img);

}

static inline void ShowOpenCVImage(CUDAImage<OpenGL2D,uchar4>& cuda_img, string const file_name)
{

    size_t const w = cuda_img.GetWidth();
    size_t const h = cuda_img.GetHeight();


    cv::Mat cv_img(cv::Size(w, h), CV_8UC4);
    cuda_img.MapCUDARes();
    checkCudaErrors(cudaMemcpyFromArray(cv_img.data,
                                        cuda_img.GetArrayPtr(), 0, 0,
                                        w*h*sizeof(uchar4),
                                        cudaMemcpyDeviceToHost));
    cuda_img.UnmapCUDARes();

    cvtColor(cv_img, cv_img, CV_BGR2RGB);
    imwrite(file_name+".png", cv_img);

}

} // namespace falcon {

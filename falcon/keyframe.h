#ifndef FALCON_KEYFRAME_H
#define FALCON_KEYFRAME_H

#include <sophus/se3.hpp>

#include "cuda/cuda_const.h"
#include "cuda/cuda_data.h"
#include "cuda/cuda_img.h"

namespace falcon {

using namespace Sophus;

struct StereoKeyframe
{

    StereoKeyframe(size_t const frame_w, size_t const frame_h)
    {        

        size_t w = frame_w;
        size_t h = frame_h;
        for(int level = 0; level < PYR_LEVEL_NUM; ++level)
        {
            for(int cam = 0; cam < 2; ++cam)
                imgs[level][cam].Reinitialise(w, h);

            w = (w+1)/2;
            h = (h+1)/2;
        }

        for(int i = 0; i < 2; ++i)
            disps[i].Reinitialise(frame_w, frame_h);
    }

    ~StereoKeyframe()
    {        
    }

    CUDAImage<OpenGL2D,uchar4> imgs[PYR_LEVEL_NUM][2];
    CUDAImage<Pitched2D,float> disps[2];    

    /* Pose */
    SE3f T;

private:
    StereoKeyframe();
    StereoKeyframe(StereoKeyframe const&);
    StereoKeyframe& operator=(StereoKeyframe const&);

};

} // namespace falcon {

#endif // FALCON_KEYFRAME_H

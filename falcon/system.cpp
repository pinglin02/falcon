#include "opencv_helper.h"

#include "system.h"

namespace falcon {

SLAMSystem::SLAMSystem(CUDAImage<OpenGL2D, uchar4> st_frames[PYR_LEVEL_NUM][2],
                       RectifiedStereoCamera const st_cam,
                       CostVolume::Params const& cv_params,
                       HuberROF::Params const& hbl1_tgv1_params)
    : reconstor(cv_params, hbl1_tgv1_params),
      params(st_frames[0][0].GetWidth(), st_frames[0][0].GetHeight(), st_cam),
      tracker(st_frames[0][0].GetWidth(), st_frames[0][0].GetHeight(), st_cam.K, st_cam.T_ext)
{

    for(int level = 0; level < PYR_LEVEL_NUM; ++level)
        for(int cam = 0; cam < 2; ++cam)
            st_cur_frames[level][cam] = &st_frames[level][cam];

}

SLAMSystem::~SLAMSystem()
{

}

void SLAMSystem::Init()
{

    ResetKeyframes();
    AddKeyframe();

}

void SLAMSystem::ResetKeyframes()
{

    /* Clean all keyframes */
    if(keyframes.size() > 0)
        for(vector<StereoKeyframe*>::iterator itr = keyframes.begin(); itr != keyframes.end(); ++itr)
            delete *itr;

    keyframes.clear();

}

bool SLAMSystem::AddKeyframe()
{

    keyframes.push_back(new StereoKeyframe(reconstor.GetCostVolumeParams().frame_w,
                                           reconstor.GetCostVolumeParams().frame_h));

    for(int level = 0; level < PYR_LEVEL_NUM; ++level) for(int cam = 0; cam < 2; ++cam)
        keyframes.back()->imgs[level][cam].CopyFrom(st_cur_frames[level][cam]);

    for(int cam = 0; cam < 2; ++cam)
    {
        reconstor.Reconstruct(*st_cur_frames[0], cam == 0 ? LeftRefImage : RightRefImage);
        reconstor.GetRealDisparity(keyframes.back()->disps[cam]);
    }

    keyframes.back()->T = tracker.GetWorldInCamera();

    tracker.SetRefFrames(keyframes.back());

    return true;

}

//bool SLAMSystem::Tracking(CUDAImage<OpenGL2D, uchar4> st_cur_frames[PYR_LEVEL_NUM][2])
//{
//    try {
//        if(keyframes.size() == 0)
//            throw FalconException("Keyframe hasn't been initialised!");
//    } catch (FalconException e) {
//        cerr << e.what() << endl;
//        return false;
//    }


//}

} // namespace falcon {

#ifndef FALCON_FALCON_H
#define FALCON_FALCON_H

#include <stdexcept>
#include <vector>

namespace falcon {

using namespace std;

class FalconException : public std::runtime_error {
public:
  FalconException (const std::string& str)
    : runtime_error("Falcon exception: " + str) {
  }
};

} // namespace falcon {

#endif // FALCON_FALCON_H

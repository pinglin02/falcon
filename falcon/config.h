#ifndef CONFIG_H_CMAKE
#define CONFIG_H_CMAKE

/*
 * Configuration Header for Falcon
 */

// Version
#define _VERSION_MAJOR 0
#define _VERSION_MINOR 1

/// Platform
/* #undef _UNIX_ */
/* #undef _WIN_ */
/* #undef _OSX_ */
/* #undef _LINUX_ */

/// Compiler
/* #undef _GCC_ */
/* #undef _MSVC_ */

#ifdef _MSVC_

#ifdef falcon_EXPORTS
#define LIBRARY_API __declspec(dllexport)
#else
#define LIBRARY_API __declspec(dllimport)
#endif

#else 
#define LIBRARY_API 

#endif // _MSVC_


#endif //CONFIG_H_CMAKE


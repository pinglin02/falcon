#ifndef FALCON_TRACKER_RECONST_H
#define FALCON_TRACKER_RECONST_H

#include <vector>

#include <sophus/se3.hpp>

#include "cuda/cuda_math.h"
#include "cuda/cuda_img.h"
#include "keyframe.h"

namespace falcon {

using namespace std;
using namespace Sophus;

struct ReferenceFrame
{

    ReferenceFrame()
    {
        for(int level = 0; level < PYR_LEVEL_NUM; ++level)
            for(int cam = 0; cam < 2; ++cam)
                this->imgs[level][cam] = NULL;

        for(int cam = 0; cam < 2; ++cam)
            this->disps[cam] = NULL;
    }

    void SetRefFrames(CUDAImage<OpenGL2D,uchar4> st_ref_imgs[PYR_LEVEL_NUM][2],
                     CUDAImage<Pitched2D,float> st_ref_disps[2])
    {        

        for(int level = 0; level < PYR_LEVEL_NUM; ++level)
            for(int cam = 0; cam < 2; ++cam)
                imgs[level][cam] = &st_ref_imgs[level][cam];

        for(int cam = 0; cam < 2; ++cam)
            this->disps[cam] = &st_ref_disps[cam];

        for(int level = 0; level < PYR_LEVEL_NUM; ++level) for(int cam = 0; cam < 2; ++cam)
        {
            J_Iref[level][cam].Reinitialise(st_ref_imgs[level][cam].GetWidth(), st_ref_imgs[level][cam].GetHeight());
            J_w[level][cam].Reinitialise(st_ref_imgs[level][cam].GetWidth(), st_ref_imgs[level][cam].GetHeight());
        }

    }

    SE3f T_ref;

    /* Store only pointers for reference image and disparity */
    CUDAImage<OpenGL2D,uchar4>* imgs[PYR_LEVEL_NUM][2];
    CUDAImage<Pitched2D,float>* disps[2];    

    /* Constant Jacobian of reference image */    
    CUDAImage<Pitched2D,Matrixf1x3> J_Iref[PYR_LEVEL_NUM][2];
    CUDAImage<Pitched2D,Matrixf3x16> J_w[PYR_LEVEL_NUM][2];

private:
    ReferenceFrame(ReferenceFrame const&);
    ReferenceFrame& operator=(ReferenceFrame const&);

};

class Tracker
{

public:
    Tracker(size_t const frame_w, size_t const frame_h,            
            Matrix3f const& K, SE3f const& T_ext);
    ~Tracker();

    /* Use keyframe as reference frame during tracking */
    void SetRefFrames(StereoKeyframe *const st_ref_frames);

    SE3f GetCameraInWorld() const { return T_cur.inverse(); }

    SE3f GetWorldInCamera() const { return T_cur; }

    bool Tracking(CUDAImage<OpenGL2D,uchar4> st_cur_frames[PYR_LEVEL_NUM][2]);

    void GetErrorImage(CUDAImage<OpenGL2D, uchar4> gl_img[PYR_LEVEL_NUM][2], size_t const level = 0, size_t const cam = 0);

    void GetWarpedImage(CUDAImage<OpenGL2D, uchar4> gl_img[PYR_LEVEL_NUM][2], size_t const level = 0, size_t const cam = 0);

    void GetWeightImage(CUDAImage<OpenGL2D, uchar4> gl_img[PYR_LEVEL_NUM][2], size_t const level = 0, size_t const cam = 0);

    ReferenceFrame* GetRefFrame() { return &ref_frames; }

private:

    void CalTukeyMEstimator(size_t const level, size_t const cam);

    size_t frame_w, frame_h;

    ReferenceFrame ref_frames;    

    CUDAImage<Pitched2D,Matrixf2x3> J_cur[PYR_LEVEL_NUM][2];
    CUDAImage<Pitched2D,float> I_warp[PYR_LEVEL_NUM][2];
    CUDAImage<Pitched2D,float> I_errors[PYR_LEVEL_NUM][2];
    CUDAImage<Pitched2D,float> I_weights[PYR_LEVEL_NUM][2];

    /* For M-estimator on CPU */
    float* I_error_host[PYR_LEVEL_NUM][2];
    float* median_array_host[PYR_LEVEL_NUM][2];
    float* weight_array_host[PYR_LEVEL_NUM][2];

    /* Gauss-Newton data */
    /* Host memory */
    size_t num_blocks[PYR_LEVEL_NUM];
    float* block_se3_gradients[PYR_LEVEL_NUM];
    float* block_se3_hessians[PYR_LEVEL_NUM];
    float* block_errors[PYR_LEVEL_NUM];

    /* CUDA memory */
    float* dev_block_se3_gradients[PYR_LEVEL_NUM];
    float* dev_block_se3_hessians[PYR_LEVEL_NUM];
    float* dev_block_errors[PYR_LEVEL_NUM];

    /* Stereo intrinsic matrix (assusm left and right are the same) */
    Matrix3f K;

    /* Tracking poses */
    SE3f T_cur, T_head;

    /* Stereo rig poses */
    SE3f T_rl, T_lr;
    SE3f T_cl, T_cr;

    /* Constant Jacobians*/
    Matrix<float,6,6> J_ladj, J_radj;
    Matrix<float,16,6> J_SE3;


};

extern "C" void SetConstTheadCUDA(float const T_head[16]);

extern "C" void SetConstMatrixCUDA(float const K[9],
                                   float const T_rl[16],
                                   float const T_lr[16],
                                   float const T_cl[16],
                                   float const T_cr[16],
                                   float const J_ladj[36],
                                   float const J_radj[36],
                                   float const J_SE3[96]);

extern "C" void CalJIrefCUDA(CUDAImage<Pitched2D,Matrixf1x3> J_Iref[PYR_LEVEL_NUM][2],
                             CUDAImage<OpenGL2D,uchar4> *const imgs[PYR_LEVEL_NUM][2],
                             CUDAImage<Pitched2D,float> *const disps[2]);

extern "C" void CalJwCUDA(CUDAImage<Pitched2D,Matrixf3x16> J_w[PYR_LEVEL_NUM][2],
                          CUDAImage<Pitched2D,float> *const ref_disps[2]);

extern "C" void WarpCurImageCUDA(CUDAImage<Pitched2D,Matrixf2x3> J_cur[PYR_LEVEL_NUM][2],
                                 CUDAImage<Pitched2D,float> I_warp[PYR_LEVEL_NUM][2],
                                 CUDAImage<Pitched2D,float> I_errors[PYR_LEVEL_NUM][2],
                                 CUDAImage<OpenGL2D,uchar4> st_cur_frames[PYR_LEVEL_NUM][2],
                                 CUDAImage<OpenGL2D,uchar4>* ref_imgs[PYR_LEVEL_NUM][2],
                                 CUDAImage<Pitched2D,float>* ref_disps[2],
                                 size_t const level,
                                 bool const cam);

extern "C" void GaussNewtonCUDA(float* dev_block_se3_gradients[PYR_LEVEL_NUM],
                                float* dev_block_se3_hessians[PYR_LEVEL_NUM],
                                float* dev_block_errors[PYR_LEVEL_NUM],
                                float* block_se3_gradients[PYR_LEVEL_NUM],
                                float* block_se3_hessians[PYR_LEVEL_NUM],
                                float* block_errors[PYR_LEVEL_NUM],
                                size_t const num_blocks[PYR_LEVEL_NUM],
                                CUDAImage<Pitched2D,Matrixf1x3> J_Iref[PYR_LEVEL_NUM][2],
                                CUDAImage<Pitched2D,Matrixf2x3> J_cur[PYR_LEVEL_NUM][2],
                                CUDAImage<Pitched2D,Matrixf3x16> J_w[PYR_LEVEL_NUM][2],
                                CUDAImage<Pitched2D,float> I_warp[PYR_LEVEL_NUM][2],
                                CUDAImage<Pitched2D,float> I_errors[PYR_LEVEL_NUM][2],
                                CUDAImage<Pitched2D,float> I_weights[PYR_LEVEL_NUM][2],
                                size_t const level,
                                bool const cam);

extern "C" void GetErrorImageCUDA(CUDAImage<OpenGL2D,uchar4> gl_img[PYR_LEVEL_NUM][2],
                                  CUDAImage<Pitched2D,float> I_errors[PYR_LEVEL_NUM][2],
                                  size_t const level,
                                  bool const cam);

extern "C" void GetWarpedImageCUDA(CUDAImage<OpenGL2D,uchar4> gl_img[PYR_LEVEL_NUM][2],
                                   CUDAImage<Pitched2D,float> I_warp[PYR_LEVEL_NUM][2],
                                   size_t const level,
                                   bool const cam);

extern "C" void GetWeightImageCUDA(CUDAImage<OpenGL2D,uchar4> gl_img[PYR_LEVEL_NUM][2],
                                   CUDAImage<Pitched2D,float> I_weights[PYR_LEVEL_NUM][2],
                                   size_t const level,
                                   bool const cam);

} // namespace falcon {

#endif // FALCON_TRACKER_RECONST_H

#ifndef FALCON_IMG_PROC_H
#define FALCON_IMG_PROC_H

#include <cuda_runtime.h>

#include "cuda/cuda_img.h"
#include "cuda/cuda_data.h"

namespace falcon {

namespace CUDAImgProc{

////////////////////////////////////////////////////////////////////////////
//  Device functions
////////////////////////////////////////////////////////////////////////////
extern "C" void DrawTextPtsOn3DPtsOccluding(CUDAData<OpenGLBufferType,float3>& pts,
                                            CUDAData<OpenGLBufferType,float3>& text_pts,
                                            CUDAData<OpenGLBufferType,uchar4>& text_pts_colour,
                                            CUDAImage<Pitched2D,uchar1>& mask,
                                            CUDAImage<OpenGL2D,uchar4>& weight_img,
                                            uchar4 const text_colour,
                                            ROI const roi,
                                            short const w,
                                            short const h,
                                            short const frame_w,
                                            short const frame_h);


extern "C" void DrawTextPtsOn3DPts(CUDAData<OpenGLBufferType,float3>& pts,
                                   CUDAData<OpenGLBufferType,float3>& text_pts,
                                   CUDAData<OpenGLBufferType,uchar4>& text_pts_colour,
                                   CUDAImage<Pitched2D,uchar1>& mask,
                                   uchar4 const text_colour,
                                   short const w,
                                   short const h);

}

} // namespace falcon {

#endif // FALCON_IMG_PROC_H

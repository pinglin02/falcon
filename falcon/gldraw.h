#ifndef FALCON_GLDRAW_H
#define FALCON_GLDRAW_H

#include <sophus/se3.hpp>

#include "cuda/cuda_img.h"
#include "cuda/cuda_data.h"
#include "cuda/cuda_const.h"

namespace falcon {

using namespace std;
using namespace Sophus;

struct OpenGLCUDAInteropSrc
{
    /* Reconstruction */
    CUDAImage<OpenGL2D,uchar4> disp_imgs;
    CUDAData<OpenGLBufferType,float3> pts;
    CUDAData<OpenGLBufferType,uchar4> pts_colour;

    /* Tracking */
    CUDAImage<OpenGL2D,uchar4> weight_imgs[PYR_LEVEL_NUM][2];
    CUDAImage<OpenGL2D,uchar4> warp_imgs[PYR_LEVEL_NUM][2];
    CUDAImage<OpenGL2D,uchar4> error_imgs[PYR_LEVEL_NUM][2];

    /* AR */
    CUDAImage<Pitched2D,uchar1> mask;
    CUDAData<OpenGLBufferType,float3> text_pts;
    CUDAData<OpenGLBufferType,uchar4> text_pts_colour;

};

struct GLDrawer
{

    static float near;
    static float far;
    static float c_s;
    static float w_s;

    static inline void DrawCamera(Matrix3f const K,
                                  SE3f const T,
                                  CUDAImage<OpenGL2D,uchar4> const& frames,
                                  uint32_t const cam_w,
                                  uint32_t const cam_h);

    static inline void DrawDispFrustum(Matrix3f const K,
                                       SE3f const T,
                                       float const B,
                                       float const min_disp,
                                       float const max_disp,
                                       uint32_t const cam_w,
                                       uint32_t const cam_h);

    static inline void DrawWorldCoord();

    static inline void DrawWorldGrid(float const step = 10.0);

    static inline void RenderPointCloud(CUDAData<OpenGLBufferType,float3> const& pts,
                                        CUDAData<OpenGLBufferType,uchar4> const& pts_colour);

};


void GLDrawer::DrawCamera(Matrix3f const K,
                          SE3f const cam_in_world,
                          CUDAImage<OpenGL2D,uchar4> const& frames,
                          uint32_t const cam_w,
                          uint32_t const cam_h)
{

    Vector3f v[4];
    Vector3f frustums[4];
    float fu, fv, u0, v0;

    fu = K(0, 0);
    fv = K(1, 1);
    u0 = K(0, 2);
    v0 = K(1, 2);

    float re_x = (float)(cam_w/2.0f)*(near/fu);
    float re_y = (float)(cam_h/2.0f)*(near/fv);
    float c_x = (u0-cam_w/2.0f)*(near/fu);
    float c_y = (v0-cam_h/2.0f)*(near/fv);

    float l = (-re_x-c_x);
    float t = (-re_y-c_y);
    float r = (re_x-c_x);
    float b = (re_y-c_y);

    float Z = (far*near)/(far-near);

    // near top left
    frustums[0].x() = l;
    frustums[0].y() = t;
    frustums[0].z() = Z;

    // near bottom left
    frustums[1].x() = l;
    frustums[1].y() = b;
    frustums[1].z() = Z;

    // near bottom right
    frustums[2].x() = r;
    frustums[2].y() = b;
    frustums[2].z() = Z;

    // near top right
    frustums[3].x() = r;
    frustums[3].y() = t;
    frustums[3].z() = Z;

    v[0] = w_s*(cam_in_world.matrix().block<3, 3>(0, 0)*(frustums[0]*c_s) + cam_in_world.matrix().block<3, 1>(0, 3));
    v[1] = w_s*(cam_in_world.matrix().block<3, 3>(0, 0)*(frustums[1]*c_s) + cam_in_world.matrix().block<3, 1>(0, 3));
    v[2] = w_s*(cam_in_world.matrix().block<3, 3>(0, 0)*(frustums[2]*c_s) + cam_in_world.matrix().block<3, 1>(0, 3));
    v[3] = w_s*(cam_in_world.matrix().block<3, 3>(0, 0)*(frustums[3]*c_s) + cam_in_world.matrix().block<3, 1>(0, 3));

    Vector3f O = w_s*cam_in_world.matrix().block<3, 1>(0, 3);;

    float colorLine[4] = { 0.8f, 0.8f, 0.8f, 1.0f };
    glColor4fv(colorLine);

    glBegin(GL_LINES);
    glVertex3fv(O.data());
    glVertex3fv(v[0].data());
    glVertex3fv(O.data());
    glVertex3fv(v[1].data());
    glVertex3fv(O.data());
    glVertex3fv(v[2].data());
    glVertex3fv(O.data());
    glVertex3fv(v[3].data());
    glEnd();

    glBegin(GL_LINE_LOOP);
    glVertex3fv(v[0].data());
    glVertex3fv(v[1].data());
    glVertex3fv(v[2].data());
    glVertex3fv(v[3].data());
    glEnd();

    float colorFace[4] = { 1.0f, 1.0f, 1.0f, 1.0f };
    glColor4fv(colorFace);

    frames.Bind();
    glEnable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0, 0.0);
    glVertex3fv(v[0].data());
    glTexCoord2f(1.0, 0.0);
    glVertex3fv(v[3].data());
    glTexCoord2f(1.0, 1.0);
    glVertex3fv(v[2].data());
    glTexCoord2f(0.0, 1.0);
    glVertex3fv(v[1].data());
    glEnd();
    glDisable(GL_TEXTURE_2D);
    frames.Unbind();


}

void GLDrawer::DrawDispFrustum(Matrix3f const K,
                               SE3f const cam_in_world,
                               float const B,
                               float const min_disp,
                               float const max_disp,
                               uint32_t const cam_w,
                               uint32_t const cam_h)
{    

    float const fu = K(0, 0);
    float const fv = K(1, 1);
    float const u0 = K(0, 2);
    float const v0 = K(1, 2);

    // Back-project the front- and back-end of the 2D keyframe to 3D.
    float const z_max = min_disp == 0 ? 1e32 : B*fu/min_disp;
    float const z_min = B*fu/max_disp;

    Vector3f v[4];

    v[0](0, 0) = (0 - u0)/fu;
    v[0](1, 0) = (0 - v0)/fv;
    v[0](2, 0) = 1.0;

    v[1](0, 0) = (0 - u0)/fu;
    v[1](1, 0) = (cam_h-1 - v0)/fv;
    v[1](2, 0) = 1.0;

    v[2](0, 0) = (cam_w-1 - u0)/fu;
    v[2](1, 0) = (cam_h-1 - v0)/fv;
    v[2](2, 0) = 1.0;

    v[3](0, 0) = (cam_w-1 - u0)/fu;
    v[3](1, 0) = (0 - v0)/fv;
    v[3](2, 0) = 1.0;

    Vector3f frustum[8];
    for(int i = 0; i < 4; i++)
    {
        frustum[i] = w_s*(cam_in_world.matrix().block<3,3>(0,0)*v[i]*z_min + cam_in_world.matrix().block<3,1>(0,3));
        frustum[i+4] = w_s*(cam_in_world.matrix().block<3,3>(0,0)*v[i]*z_max + cam_in_world.matrix().block<3,1>(0,3));
    }

    float colorLine[4] = { 0.5f, 0.7f, 0.8f, 1.0f };
    glColor4fv(colorLine);

    glBegin(GL_LINE_LOOP);
    glVertex3fv(frustum[0].data());
    glVertex3fv(frustum[1].data());
    glVertex3fv(frustum[2].data());
    glVertex3fv(frustum[3].data());
    glEnd();

    glBegin(GL_LINE_LOOP);
    glVertex3fv(frustum[4].data());
    glVertex3fv(frustum[5].data());
    glVertex3fv(frustum[6].data());
    glVertex3fv(frustum[7].data());
    glEnd();

    glBegin(GL_LINES);
    glVertex3fv(frustum[0].data());
    glVertex3fv(frustum[4].data());
    glVertex3fv(frustum[1].data());
    glVertex3fv(frustum[5].data());
    glVertex3fv(frustum[2].data());
    glVertex3fv(frustum[6].data());
    glVertex3fv(frustum[3].data());
    glVertex3fv(frustum[7].data());
    glEnd();

    /* Inverse depth sample visualization */
//    float inv_depth_max = 1.0/min_disp;
//    float inv_depth_min = 1.0/max_disp;
//    float inv_depth_stride = (inv_depth_max - inv_depth_min) / float(depth_resolution);
//    for(uint32_t i = 1; i < depth_resolution; i++)
//    {
//        float depth = 1.0/(inv_depth_max - float(i*inv_depth_stride));
//        CVFrustum[0] = project(cameraInWorld*unproject(unproject(cam.unmap(Vector2d(0, 0)))*=depth));
//        CVFrustum[1] = project(cameraInWorld*unproject(unproject(cam.unmap(Vector2d(cam.width()-1, 0)))*=depth));
//        CVFrustum[2] = project(cameraInWorld*unproject(unproject(cam.unmap(Vector2d(cam.width()-1, cam.height()-1)))*=depth));
//        CVFrustum[3] = project(cameraInWorld*unproject(unproject(cam.unmap(Vector2d(0, cam.height()-1)))*=depth));

//        glBegin(GL_LINE_LOOP);
//        glVertex3dv(CVFrustum[0].data());
//        glVertex3dv(CVFrustum[1].data());
//        glVertex3dv(CVFrustum[2].data());
//        glVertex3dv(CVFrustum[3].data());
//        glEnd();
//    }

    glColor4f(1.0, 1.0, 1.0, 1.0);

}

void GLDrawer::DrawWorldCoord()
{

    glLineWidth(1.5);
    glBegin(GL_LINES);
    // x-axis
    glColor3f(0.5f, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(w_s*5, 0, 0);

    // y-axis
    glColor3f(0, 0.5f, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, w_s*5, 0);

    // z-axis
    glColor3f(0, 0, 0.5f);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, w_s*5);
    glEnd();

    glColor3f(1, 1, 1);
}


void GLDrawer::DrawWorldGrid(float const step)
{

    float size = step*1000;

    glLineWidth(1.0);
    glBegin(GL_LINES);

    glColor3f(0.3f, 0.3f, 0.3f);
    for(float i=0; i < size; i+= step)
    {
        glVertex3f(-size, 0,  i);   // lines parallel to X-axis
        glVertex3f( size, 0,  i);
        glVertex3f(-size, 0, -i);   // lines parallel to X-axis
        glVertex3f( size, 0, -i);

        glVertex3f( i, 0, -size);   // lines parallel to Z-axis
        glVertex3f( i, 0,  size);
        glVertex3f(-i, 0, -size);   // lines parallel to Z-axis
        glVertex3f(-i, 0,  size);
    }
    glEnd();

    glColor3f(1, 1, 1);
}

void GLDrawer::RenderPointCloud(CUDAData<OpenGLBufferType,float3> const& pts,
                                CUDAData<OpenGLBufferType,uchar4> const& pts_colour)
{

    glEnable(GL_COLOR_MATERIAL);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);

    pts.BindOpenGLBuffer();
    glVertexPointer(3, GL_FLOAT, 0, 0);
    pts.UnbindOpenGLBuffer();

    pts_colour.BindOpenGLBuffer();
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, 0);
    pts_colour.UnbindOpenGLBuffer();

    glDrawArrays(GL_POINTS, 0, pts.GetNumElemenets());

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);

    glDisable(GL_COLOR_MATERIAL);

}

} // namespace falcon {


#endif // FALCON_GLDRAW_H

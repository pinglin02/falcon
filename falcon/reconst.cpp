#include "img_proc.h"
#include "reconst.h"

namespace falcon {

////////////////////////////////////////////////////////////////////////////
//  Stereo reconstruction using disparity (rectification required)
////////////////////////////////////////////////////////////////////////////

namespace CostVolume {

Params::Params(uint32_t const min_disp,
               uint32_t const max_disp,
               uint32_t const win_r,
               RefImage const ref_img,
               DataTermMethod const method,
               uint8_t min_bg_thr,
               uint8_t max_bg_thr,
               uint32_t const frame_w,
               uint32_t const frame_h)
    : min_disp(min_disp), max_disp(max_disp), num_disp(max_disp-min_disp+1),
      win_r(win_r), ref_img(ref_img), method(method),
      min_bg_thr(min_bg_thr), max_bg_thr(max_bg_thr),
      frame_w(frame_w), frame_h(frame_h)
{

    roi[0] = ROI(win_r + max_disp, frame_w - win_r - 1, win_r, frame_h - win_r - 1);
    roi[1] = ROI(win_r, frame_w -  max_disp - win_r - 1, win_r, frame_h - win_r - 1);

    /* Left and right ROI shall have the same width and height */
    roi_w = roi[0].x_max - roi[0].x_min + 1;
    roi_h = roi[0].y_max - roi[0].y_min + 1;

}

bool Params::operator!=(Params const& rhs) const
{
    return (min_disp != rhs.min_disp) ||
           (max_disp != rhs.max_disp) ||
           (num_disp != rhs.num_disp) ||
           (win_r != rhs.win_r) ||
           (ref_img != rhs.ref_img) ||
           (method != rhs.method) ||
           (min_bg_thr != rhs.min_bg_thr) ||
           (max_bg_thr != rhs.max_bg_thr);
}

Data::Data(Params const params)
    : params(params)
{

    cudaChannelFormatDesc channelDesc_float = cudaCreateChannelDesc<float>();
    checkCudaErrors(cudaMalloc3DArray(&cost_volume_array, &channelDesc_float, make_cudaExtent(params.roi_w, params.roi_h, params.num_disp), cudaArraySurfaceLoadStore));

    win_disp.Reinitialise(params.roi_w, params.roi_h, 0.0);
    min_disp_cost.Reinitialise(params.roi_w, params.roi_h, 0.0);
    max_disp_cost.Reinitialise(params.roi_w, params.roi_h, 0.0);

}

Data::~Data()
{
    checkCudaErrors(cudaFreeArray(cost_volume_array));
}

void Data::UpdateParams(Params const params)
{

    if(this->params.num_disp != params.num_disp ||
       this->params.roi_w != params.roi_w ||
       this->params.roi_h != params.roi_h)
    {
        checkCudaErrors(cudaFreeArray(cost_volume_array));

        cudaChannelFormatDesc channelDesc_float = cudaCreateChannelDesc<float>();
        checkCudaErrors(cudaMalloc3DArray(&cost_volume_array, &channelDesc_float, make_cudaExtent(params.roi_w, params.roi_h, params.num_disp), cudaArraySurfaceLoadStore));

        win_disp.Reinitialise(params.roi_w, params.roi_h, 0.0);
        min_disp_cost.Reinitialise(params.roi_w, params.roi_h, 0.0);
        max_disp_cost.Reinitialise(params.roi_w, params.roi_h, 0.0);
    }

    this->params = params;

}

void Data::SetRefImage(RefImage const ref_img)
{
    params.ref_img = ref_img;
}


} //namespace CostVolume {

namespace HuberROF {

Params::Params(int const num_itr,
               float const alpha,
               float const beta,
               float const epsilon,
               float const lambda,
               float const theta)
    : num_itr(num_itr), alpha(alpha), beta(beta), epsilon(epsilon),
      lambda(lambda), theta(theta)
{    

}

bool Params::operator!=(Params const& rhs) const
{

    return (num_itr != rhs.num_itr) ||
           (alpha != rhs.alpha) ||
           (beta != rhs.beta) ||
           (epsilon != rhs.epsilon) ||
           (lambda != rhs.lambda) ||
           (theta != rhs.theta);
}

Data::Data(Params const params, size_t const w, size_t const h)
    : params(params), w(w), h(h)
{    

    /* Diffusion Tensor */
    diffuse_tensor.Reinitialise(w, h);

    /* Primal variables */
    primal.Reinitialise(w, h);
    primal_step.Reinitialise(w, h);

    old_primal.Reinitialise(w, h);
    head_primal.Reinitialise(w, h);

    /* Dual variables */
    dual.Reinitialise(w, h);
    dual_step.Reinitialise(w, h);

    /* Auxiliary variable */
    aux.Reinitialise(w, h);

}

Data::~Data()
{   
}

void Data::UpdateParams(Params const params)
{

    this->params = params;

}

void Data::Resize(size_t const w, size_t const h)
{

    if(this->w != w || this->h != h)
    {
        this->w = w;
        this->h = h;

        diffuse_tensor.Reinitialise(w, h);
        primal.Reinitialise(w, h);
        primal_step.Reinitialise(w, h);
        old_primal.Reinitialise(w, h);
        head_primal.Reinitialise(w, h);
        dual.Reinitialise(w, h);
        dual_step.Reinitialise(w, h);
        aux.Reinitialise(w, h);
    }

}

} // namespace HuberL1TGV1 {

////////////////////////////////////////////////////////////////////////////
//  StereoReconstructor
////////////////////////////////////////////////////////////////////////////
StereoReconstructor::StereoReconstructor(CostVolume::Params const& cv_params,
                                         HuberROF::Params const& hbl1_tgv1_params)
    : cv_data(cv_params), opt_data(hbl1_tgv1_params, cv_params.roi_w, cv_params.roi_h)
{    
}

StereoReconstructor::~StereoReconstructor()
{

}

void StereoReconstructor::Reconstruct(CUDAImage<OpenGL2D,uchar4> st_frames[2],
                                      RefImage const ref_img)
{

    cv_data.SetRefImage(ref_img);
    BindTexturesCUDA(st_frames, ref_img);

    CostVolumeFunctions();
    HuberL1TGV1Functions();

}

bool StereoReconstructor::Reconstruct(CUDAImage<OpenGL2D,uchar4> st_frames[2],
                                      CostVolume::Params const& cv_params,
                                      HuberROF::Params const& opt_params)
{

    /* Update cost-volume */
    if(cv_data.GetParams() != cv_params)
    {

        if(cv_data.GetParams().roi_w != cv_params.roi_w || cv_data.GetParams().roi_h != cv_params.roi_h)
            opt_data.Resize(cv_params.roi_w, cv_params.roi_h);

        BindTexturesCUDA(st_frames, cv_params.ref_img);

        cv_data.UpdateParams(cv_params);

        CostVolumeFunctions();
        HuberL1TGV1Functions();

        return true;
    }

    if(opt_data.GetParams() != opt_params)
    {
        opt_data.UpdateParams(opt_params);

        BindTexturesCUDA(st_frames, cv_params.ref_img);

        HuberL1TGV1Functions();

        return true;
    }

    return false;

}

void StereoReconstructor::CostVolumeFunctions()
{

    CostVolume::BuildCostVolume(cv_data.cost_volume_array,
                                cv_data.GetParams());

    CostVolume::WinnerTakeAll(cv_data.win_disp,
                              cv_data.min_disp_cost,
                              cv_data.max_disp_cost,
                              cv_data.GetParams());

}

void StereoReconstructor::HuberL1TGV1Functions()
{

    HuberROF::CalculateDiffuseTensor(opt_data.diffuse_tensor,
                                        cv_data.GetParams(),
                                        opt_data.GetParams());

    HuberROF::Preconditioning(opt_data.diffuse_tensor,
                                 opt_data.primal_step,
                                 opt_data.dual_step,
                                 cv_data.GetParams(),
                                 opt_data.GetParams());

    HuberROF::ItrHuberROF(cv_data.win_disp,
                          cv_data.min_disp_cost,
                          cv_data.max_disp_cost,
                          opt_data.head_primal,
                          opt_data.primal,
                          opt_data.old_primal,
                          opt_data.dual,
                          opt_data.aux,
                          opt_data.diffuse_tensor,
                          opt_data.primal_step,
                          opt_data.dual_step,
                          cv_data.GetParams(),
                          opt_data.GetParams());

}

void StereoReconstructor::GetRealDisparity(CUDAImage<Pitched2D,float>& real_disp)
{
    if(real_disp.GetWidth() != cv_data.GetParams().frame_w || real_disp.GetHeight() != cv_data.GetParams().frame_h)
        real_disp.Reinitialise(cv_data.GetParams().frame_w, cv_data.GetParams().frame_h);

    CopyNormDisp2RealDispCUDA(real_disp, cv_data, opt_data);

}

void StereoReconstructor::GetDisparityImage(CUDAImage<OpenGL2D,uchar4>& uchar4_disp)
{

    if(uchar4_disp.GetWidth() != cv_data.GetParams().frame_w || uchar4_disp.GetHeight() != cv_data.GetParams().frame_h)    
        uchar4_disp.Reinitialise(cv_data.GetParams().frame_w, cv_data.GetParams().frame_h);    

    CopyNormDisp2OpenGLTextureCUDA(uchar4_disp, cv_data, opt_data);

}

void StereoReconstructor::GetPointCloud(CUDAImage<OpenGL2D,uchar4>& frame,
                                        CUDAData<OpenGLBufferType,float3>& pts,
                                        CUDAData<OpenGLBufferType,uchar4>& pts_colour,
                                        float const baseline,
                                        float const K[9],
                                        float const T[16])
{

    if(pts.GetNumElemenets() != cv_data.GetParams().roi_w*cv_data.GetParams().roi_h)
    {
        pts.Reinitialise(OpenGLArrayBuffer, cv_data.GetParams().roi_w*cv_data.GetParams().roi_h, sizeof(float3));
        pts_colour.Reinitialise(OpenGLArrayBuffer, cv_data.GetParams().roi_w*cv_data.GetParams().roi_h, sizeof(uchar4));
    }

    DispMap2PtsCloudCUDA(frame,
                         pts,
                         pts_colour,
                         cv_data, opt_data,
                         baseline, K, T);

}

} // namespace falcon {

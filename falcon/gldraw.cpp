#include <Eigen/Core>

#include <typeinfo>
#include "gldraw.h"

namespace falcon {

float GLDrawer::near;
float GLDrawer::far;
float GLDrawer::c_s;
float GLDrawer::w_s;

} // namespace falcon {

#include "opencv_helper.h"

#include <iostream>
#include <algorithm>

#include "tracker.h"

namespace falcon {

Tracker::Tracker(size_t const frame_w, size_t const frame_h,                 
                 Matrix3f const& K, SE3f const& T_ext)
    : frame_w(frame_w), frame_h(frame_h), K(K), T_rl(T_ext)
{

    size_t w = frame_w;
    size_t h = frame_h;
    for(int level = 0; level < PYR_LEVEL_NUM; ++level)
    {
        for(int cam = 0; cam < 2; ++cam)
        {
            J_cur[level][cam].Reinitialise(w, h);
            I_warp[level][cam].Reinitialise(w, h);
            I_errors[level][cam].Reinitialise(w, h);
            I_weights[level][cam].Reinitialise(w, h);

            I_error_host[level][cam] = new float[I_errors[level][cam].GetWidth()*I_errors[level][cam].GetHeight()];
            median_array_host[level][cam] = new float[I_errors[level][cam].GetWidth()*I_errors[level][cam].GetHeight()];
            weight_array_host[level][cam] = new float[I_errors[level][cam].GetWidth()*I_errors[level][cam].GetHeight()];
        }

        w = (w+1)/2;
        h = (h+1)/2;
    }

    /* Gauss-Newton data */
    w = frame_w;
    h = frame_h;
    for(int level = 0; level < PYR_LEVEL_NUM; ++level)
    {
        num_blocks[level] = ((w+SHARE_THREAD_NUM-1)/SHARE_THREAD_NUM)*((h+SHARE_THREAD_NUM-1)/SHARE_THREAD_NUM);

        checkCudaErrors(cudaMalloc((void**)&dev_block_se3_gradients[level], sizeof(float)*num_blocks[level]*6));
        block_se3_gradients[level] = new float[num_blocks[level]*6];

        checkCudaErrors(cudaMalloc((void**)&dev_block_se3_hessians[level], sizeof(float)*num_blocks[level]*36));
        block_se3_hessians[level] = new float[num_blocks[level]*36];

        checkCudaErrors(cudaMalloc((void**)&dev_block_errors[level], sizeof(float)*num_blocks[level]));
        block_errors[level] = new float[num_blocks[level]];

        w = (w+1)/2;
        h = (h+1)/2;
    }

    /* Setup stereo rig poses */
    T_rl = T_ext;
    T_lr = T_rl.inverse();

    T_cl = SE3f::exp(T_rl.log()/2.0);
    T_cr = SE3f::exp(T_lr.log()/2.0);

    /* Setup constant Jacobian */
    J_ladj = Matrix<float,6,6>::Zero();
    J_ladj.block<3,3>(0, 0) = T_cr.rotationMatrix();
    J_ladj.block<3,3>(3, 3) = T_cr.rotationMatrix();
    J_ladj.block<3,3>(0, 3) = SO3f::hat(T_cr.translation())*T_cr.rotationMatrix();

    J_radj = Matrix<float,6,6>::Zero();
    J_radj.block<3,3>(0, 0) = T_cr.rotationMatrix();
    J_radj.block<3,3>(3, 3) = T_cr.rotationMatrix();
    J_radj.block<3,3>(0, 3) = SO3f::hat(T_cr.translation())*T_cr.rotationMatrix();

    for(int i = 0; i < 6; ++i)
        J_SE3.block<16,1>(0, i) = Map<Matrix<float,16,1> >((float*)SE3f::generator(i).data());

    SetConstMatrixCUDA(K.data(),
                       T_rl.matrix().data(),
                       T_lr.matrix().data(),
                       T_cl.matrix().data(),
                       T_cr.matrix().data(),
                       J_ladj.data(),
                       J_radj.data(),
                       J_SE3.data());

}

Tracker::~Tracker()
{

    for(int level = 0; level < PYR_LEVEL_NUM; ++level) for(int i = 0; i < 2; ++i)
    {
        delete [] I_error_host[level][i];
        delete [] median_array_host[level][i];
        delete [] weight_array_host[level][i];
    }

    for(int level = 0; level < PYR_LEVEL_NUM; ++level)
    {
        delete [] block_se3_gradients[level];
        delete [] block_se3_hessians[level];
        delete [] block_errors[level];

        checkCudaErrors(cudaFree(dev_block_se3_gradients[level]));
        checkCudaErrors(cudaFree(dev_block_se3_hessians[level]));
        checkCudaErrors(cudaFree(dev_block_errors[level]));
    }
}

void Tracker::SetRefFrames(StereoKeyframe *const st_keyframe)
{

    ref_frames.T_ref = st_keyframe->T;
    ref_frames.SetRefFrames(st_keyframe->imgs,
                            st_keyframe->disps);

    T_head = SE3f();

    /* Pre-compute constant Jacobian */
    CalJIrefCUDA(ref_frames.J_Iref,
                 ref_frames.imgs,
                 ref_frames.disps);
    CalJwCUDA(ref_frames.J_w,
              ref_frames.disps);

}

bool Tracker::Tracking(CUDAImage<OpenGL2D,uchar4> st_cur_frames[PYR_LEVEL_NUM][2])
{

    /* Inversion requires higher precision so using double */
    Matrix<double,1,6> grad;
    Matrix<double,6,6> Hessian;
    double error;

    int itr_num[PYR_LEVEL_NUM] = {1, 5, 5/*, 10, 3*/};
    float update_norm[PYR_LEVEL_NUM] = {1e-2, 1e-3, 1e-3/*, 1e-3, 1e-3*/};

    SE3f update;
    for(int level = PYR_LEVEL_NUM-1; level >= 0; --level)
    {


        for(int itr = 0; itr < itr_num[level] ; ++itr)
        {
//        int itr = 0;
//        while(true)
//        {

            SetConstTheadCUDA(T_head.matrix().data());

            grad = Matrix<double,1,6>::Zero();
            Hessian = Matrix<double,6,6>::Zero();
            error = 0;

            for(int cam = 0; cam < 2; ++cam)
            {                

                /* For automatic Tukey M-estimator weighting */
                WarpCurImageCUDA(J_cur,
                                 I_warp,
                                 I_errors,
                                 st_cur_frames,
                                 ref_frames.imgs,
                                 ref_frames.disps,
                                 level,
                                 cam);

                CalTukeyMEstimator(level, cam);

                GaussNewtonCUDA(dev_block_se3_gradients, dev_block_se3_hessians, dev_block_errors,
                                block_se3_gradients, block_se3_hessians, block_errors, num_blocks,
                                ref_frames.J_Iref,
                                J_cur,
                                ref_frames.J_w,
                                I_warp,
                                I_errors,
                                I_weights,
                                level,
                                cam);

                for(int i = 0; i < num_blocks[level]; ++i)
                {
                    for(int j = 0; j < 6; ++j)
                        grad(0, j) += block_se3_gradients[level][i*6+j];

                    for(int j = 0; j < 36; ++j)
                        Hessian(j%6, j/6) += block_se3_hessians[level][i*36+j];

                    error += block_errors[level][i];
                }

            }

            update = SE3d::exp(Hessian.llt().solve(grad.transpose())).cast<float>();

            T_head = update*T_head;

//            cout << "Level: " << level << endl;
//            cout << "Itr: " << itr << endl;
//            cout << "se3 update norm: " << update.log().norm() << endl;
//            cv::waitKey(100);

//            if(update.log().norm() < update_norm[level] || itr > 50)
//                break;
//            else
//                itr++;

        }
    }

    float rmse = sqrt(error/(frame_w*frame_h));
    printf("RMSE: %f\n", rmse);

//    Vector6f T_cur_se3 = T_cur.log();
//    printf("T_cur pose: [%f, %f, %f, %f, %f, %f]\n\n", T_cur_se3(0), T_cur_se3(1), T_cur_se3(2), T_cur_se3(3), T_cur_se3(4), T_cur_se3(5));
    fflush(stdout);

    T_cur = T_head*ref_frames.T_ref;

//    if(rmse > 0.01)
    if(rmse > 0.04)
        return false;
    else
    {
        return  true;
    }

}

void Tracker::CalTukeyMEstimator(size_t const level, size_t const cam)
{

    size_t const num_data = I_errors[level][cam].GetWidth()*I_errors[level][cam].GetHeight();
    checkCudaErrors(cudaMemcpy2D(I_error_host[level][cam], I_errors[level][cam].GetWidth()*sizeof(float),
                                 I_errors[level][cam].ptr, I_errors[level][cam].pitch,
                                 I_errors[level][cam].GetWidth()*sizeof(float), I_errors[level][cam].GetHeight(),
                                 cudaMemcpyDeviceToHost));

    copy(I_error_host[level][cam], I_error_host[level][cam]+num_data, median_array_host[level][cam]);

    nth_element(median_array_host[level][cam], median_array_host[level][cam]+num_data/2, median_array_host[level][cam]+num_data);
    float const median1 = median_array_host[level][cam][num_data/2];

    for(int i = 0; i < num_data; ++i)
        median_array_host[level][cam][i] = abs(I_error_host[level][cam][i] - median1);

    nth_element(median_array_host[level][cam], median_array_host[level][cam]+num_data/2, median_array_host[level][cam]+num_data);
    float const median2 = median_array_host[level][cam][num_data/2];

    fill(weight_array_host[level][cam], weight_array_host[level][cam]+num_data, 0);

    float const c = 1.4826;
    float const b = 4.6851;
    float const s = c*median2;

    for(int i = 0; i < num_data; ++i)
    {
        float const u = abs(I_error_host[level][cam][i] - median1)/s;
        if(abs(u) < b)
            weight_array_host[level][cam][i] = pow(pow(1.0f - u/b, 2.0f), 2.0f);
    }

    checkCudaErrors(cudaMemcpy2D(I_weights[level][cam].ptr, I_weights[level][cam].pitch,
                                 weight_array_host[level][cam], I_weights[level][cam].GetWidth()*sizeof(float),
                                 I_weights[level][cam].GetWidth()*sizeof(float), I_weights[level][cam].GetHeight(),
                                 cudaMemcpyHostToDevice));

}

void Tracker::GetErrorImage(CUDAImage<OpenGL2D,uchar4> gl_img[PYR_LEVEL_NUM][2], size_t const level, size_t const cam)
{

    if(gl_img[level][cam].GetWidth() != (frame_w+1)/pow(2.0f,level) || gl_img[level][cam].GetHeight() != (frame_h+1)/pow(2.0f,level))
        gl_img[level][cam].Reinitialise((frame_w+1)/pow(2.0f,level), (frame_h+1)/pow(2.0f,level));

    GetErrorImageCUDA(gl_img, I_errors, level, cam);

}

void Tracker::GetWarpedImage(CUDAImage<OpenGL2D,uchar4> gl_img[PYR_LEVEL_NUM][2], size_t const level, size_t const cam)
{

    if(gl_img[level][cam].GetWidth() != (frame_w+1)/pow(2.0f,level) || gl_img[level][cam].GetHeight() != (frame_h+1)/pow(2.0f,level))
        gl_img[level][cam].Reinitialise((frame_w+1)/pow(2.0f,level), (frame_h+1)/pow(2.0f,level));

    GetWarpedImageCUDA(gl_img, I_warp, level, cam);

}

void Tracker::GetWeightImage(CUDAImage<OpenGL2D,uchar4> gl_img[PYR_LEVEL_NUM][2], size_t const level, size_t const cam)
{

    if(gl_img[level][cam].GetWidth() != (frame_w+1)/pow(2.0f,level) || gl_img[level][cam].GetHeight() != (frame_h+1)/pow(2.0f,level))
        gl_img[level][cam].Reinitialise((frame_w+1)/pow(2.0f,level), (frame_h+1)/pow(2.0f,level));

    GetWeightImageCUDA(gl_img, I_weights, level, cam);

}

//    CUDAImage<Pitched2D,float> test_img(ref_frames.disps[0]->GetWidth(), ref_frames.disps[0]->GetHeight());
//    TextureTest(test_img, ref_frames.disps[0]->cudaTexes);
//    ShowOpenCVImage(test_img, "test");
//    count++;
//    char buf[100];
//    sprintf(buf, "texture-%5d", count);
//    ShowOpenCVImage(test_img, string(buf));

} // namespace falcon {
